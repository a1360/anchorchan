package com.anchor.chan.common.connection.usecase

import com.anchor.chan.common.configuration.ConfigurationState
import com.anchor.chan.common.configuration.ConfigurationStateStore
import com.anchor.chan.common.connection.ConnectionState
import com.anchor.chan.common.connection.I2pConnectionStateStore
import com.anchor.chan.common.connection.isConnectedToLocalI2p
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import net.i2p.router.Router
import net.i2p.util.Log
import org.slf4j.LoggerFactory
import java.util.Properties
import java.util.concurrent.TimeUnit

private val logger = LoggerFactory.getLogger(SetupI2pRouterUseCase::class.java)

class SetupI2pRouterUseCase(
    private val configurationStateStore: ConfigurationStateStore,
    private val connectionStateStore: I2pConnectionStateStore,
    private val estimateBandwidthUseCase: EstimateBandwidthUseCase,
    private val routerFactory: (Properties) -> Router = { Router(it) },
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO,
) {

    private var router: Router? = null

    suspend fun setupConnection() {
        withContext(ioDispatcher) {
            setupRouter()
        }
    }

    suspend fun shutdownConnection() {
        performShutdown()
    }

    private suspend fun setupRouter() {
        logger.info("Checking i2p connection state")
        val connect = checkNeedsToUpdateConnection()
        if (connect == true) {
            logger.info("Starting to connect to i2p")
            estimateBandwidthUseCase.estimateBandwidth()
            startEmbeddedRouter()
        } else if (connect == false) {
            logger.info("Shutting down the i2p connection")
            disableEmbeddedRouter()
        }
    }

    private suspend fun checkNeedsToUpdateConnection(): Boolean? {
        val config = configurationStateStore.state.value
        val wantToConnect = config.isRouterEmbedded == true

        var connect: Boolean? = null
        connectionStateStore.updateConnectionState { oldConnectionState ->
            if (wantToConnect && !oldConnectionState.isConnectedToLocalI2p()) {
                connect = true
                ConnectionState.CONNECTING
            } else if (!wantToConnect && oldConnectionState != ConnectionState.MANUAL) {
                connect = false
                ConnectionState.SHUTTING_DOWN
            } else {
                connect = null
                oldConnectionState
            }
        }

        return connect
    }

    private suspend fun startEmbeddedRouter() {
        val router = createOrFetchRouter()
        val routerProperties = router.context.properties
        val hasValidProperties =
            createCurrentUserProperties()
                .map { routerProperties[it.key] == it.value }
                .takeIf { it.isNotEmpty() }
                ?.reduce { acc, b -> acc && b }
                ?: true
        if (hasValidProperties) {
            startAndWaitForRouterToFinishConnecting(router)
        } else {
            restartRouter()
        }
    }

    private fun createOrFetchRouter(): Router =
        router ?: createRouter().also { router = it }

    private fun createCurrentUserProperties(): Properties =
        configurationStateStore.state.value.toConfigMap()

    private fun createRouter() =
        routerFactory(createCurrentUserProperties())
            .apply {
                killVMOnEnd = false
                context.logManager().defaultLimit = Log.STR_WARN
            }

    private suspend fun startAndWaitForRouterToFinishConnecting(router: Router) {
        connectionStateStore.updateConnectionState(ConnectionState.CONNECTING)
        if (!router.isAlive) {
            router.runRouter()
        }
        while (!router.isRunning) {
            delay(100)
        }
        delay(TimeUnit.MINUTES.toMillis(2)) // wait for services to launch and connections to build
        logger.info("Connected to I2P")
        connectionStateStore.updateConnectionState(ConnectionState.CONNECTED_AND_BUILDING_NETWORK)
    }

    private suspend fun disableEmbeddedRouter() {
        connectionStateStore.updateConnectionState(ConnectionState.SHUTTING_DOWN)
        performShutdown()
        connectionStateStore.updateConnectionState(ConnectionState.MANUAL)
    }

    private suspend fun restartRouter() {
        connectionStateStore.updateConnectionState(ConnectionState.CONNECTING)
        performShutdown()
        startEmbeddedRouter()
    }

    private suspend fun performShutdown() {
        val router = router
        this.router = null
        router?.shutdown(0)
    }

    private fun ConfigurationState.toConfigMap(): Properties {
        val install = "${System.getProperty("user.home")}/.i2p"
        return Properties().apply {
            put("i2p.dir.config", install)
            put("i2p.dir.base", install)
            put("i2np.bandwidth.inboundKBytesPerSecond", bandwidthSettings.inBoundKBytesPerSecond.toString())
            put("i2np.bandwidth.outboundKBytesPerSecond", bandwidthSettings.outBoundKBytesPerSecond.toString())
            put("router.sharePercentage", bandwidthSettings.bandwidthSharePercentage.toString())
            put("router.disableTunnelTesting", false)
            put("i2np.ntcp.port", "27009")
            put("i2np.udp.port", "27009")
            put("i2np.udp.internalPort", "27009")
        }
    }
}
