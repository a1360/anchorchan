package com.anchor.chan.desktop.screen.account

import com.anchor.chan.account.AccountState
import com.anchor.chan.architecture.ui.ScreenBuilder
import com.anchor.chan.common.account.AccountComponent
import com.anchor.chan.desktop.screen.UiConstants
import com.anchor.chan.desktop.screen.navigation.NavigateToThreadBrowser
import com.anchor.chan.desktop.screen.viewmodel.AccountSettingsViewModel
import com.anchor.chan.desktop.screen.viewmodel.AccountSettingsViewModel.AccountViewModel
import com.anchor.chan.desktop.screen.viewmodel.AccountSettingsViewModel.BoardPermissionViewModel
import com.anchor.chan.model.imageboard.BoardModel
import com.anchor.publickey.crypto.AssymetricPublicKey
import com.anchor.publickey.crypto.toSha1Hex
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import java.time.Instant
import kotlin.coroutines.coroutineContext

class AccountSettingsScreenBuilder(
    private val accountComponent: AccountComponent,
    private val navigateToThreadBrowser: NavigateToThreadBrowser,
) : ScreenBuilder<AccountSettingsViewModel> {

    override suspend fun build(): Flow<AccountSettingsViewModel> =
        AccountSettingsScreen(CoroutineScope(coroutineContext), accountComponent, navigateToThreadBrowser).build()

    private class AccountSettingsScreen(
        private val scope: CoroutineScope,
        private val accountComponent: AccountComponent,
        private val navigateToThreadBrowser: NavigateToThreadBrowser,
    ) {

        private val onCloseClicked: () -> Unit = {
            navigateToThreadBrowser.navigateToThreadBrowser()
        }
        private val onDeleteClicked: (publicKey: String) -> Unit = {
            scope.launch {
                accountComponent.accountStateStore.delete(it)
            }
        }

        fun build(): Flow<AccountSettingsViewModel> =
            combine(
                accountComponent.accountStateStore.state,
                accountComponent.loadBoardsUseCase.loadBoards().onStart { emit(emptyList()) }
            ) { accountState, boardList ->
                accountState.toViewModel(boardList)
            }

        private fun AccountState.toViewModel(boards: List<BoardModel>): AccountSettingsViewModel {
            val accounts = accounts.map { it.toViewModel() }
            val longestChain = accounts.maxByOrNull { it.chainLength }?.chainLength ?: 0
            return AccountSettingsViewModel(
                accounts = accounts,
                boards = boards.map { it.toViewModel(longestChain) },
                targetLength = targetAccountLength ?: 0,
                onDeleteClicked = onDeleteClicked,
                onCloseClicked = onCloseClicked,
            )
        }

        private fun Map.Entry<AssymetricPublicKey, AccountState.AccountData>.toViewModel(): AccountViewModel =
            AccountViewModel(
                publicKey = key.key,
                publicKeyHash = key.key.toSha1Hex(),
                chainLength = value.chain.size,
                expiryDate = UiConstants.Time.defaultFormat(Instant.ofEpochMilli(value.accountEndDate)),
                threadUsageCount = value.associatedThreads.size,
            )

        private fun BoardModel.toViewModel(longestChain: Int): BoardPermissionViewModel =
            BoardPermissionViewModel(
                name = "$shortName - $name",
                canPost = permissions.postLength <= longestChain,
                canCreateThread = permissions.createLength <= longestChain,

            )
    }
}
