package com.anchor.chan.server.imageboard.board.datasource

import com.google.inject.Inject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.jdbi.v3.core.Jdbi
import org.slf4j.LoggerFactory

private val logger = LoggerFactory.getLogger(BoardRepository::class.java)

class BoardRepository @Inject constructor(
    private val jdbi: Jdbi,
) {

    init {
        jdbi.registerRowMapper(BoardEntityMapper())
        jdbi.registerRowMapper(BoardEntityGeneratedFieldsMapper())
        jdbi.registerRowMapper(BoardPermissionEntityMapper())
    }

    suspend fun requestBoardPermissions(): List<BoardPermissionEntity> =
        withContext(Dispatchers.IO) {
            jdbi.withHandle<List<BoardPermissionEntity>, Exception> {
                it
                    .createQuery("SELECT * FROM BoardPermission")
                    .mapTo(BoardPermissionEntity::class.java)
                    .toList()
            }
        }

    suspend fun requestBoards(): List<BoardEntity> =
        withContext(Dispatchers.IO) {
            jdbi.withHandle<List<BoardEntity>, Exception> {
                it
                    .createQuery("SELECT * FROM Board")
                    .mapTo(BoardEntity::class.java)
                    .list()
            }
        }
}
