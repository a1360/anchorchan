plugins {
    kotlin("jvm")
}

kotlin {
    jvmToolchain {
        (this as JavaToolchainSpec).languageVersion.set(JavaLanguageVersion.of("8"))
    }
}

dependencies {
    implementation(Deps.kotlinCoroutines)
    implementation(project(":common:library:publickey-crypto"))
    implementation(project(":client:component:client-models"))
    implementation("com.squareup.retrofit2:retrofit:2.9.0")
    implementation("com.squareup.retrofit2:converter-jackson:2.9.0")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.13.1")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.12.3")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.13.1")
    implementation("org.slf4j:slf4j-api:1.7.32")
    api(project(":api"))
}
