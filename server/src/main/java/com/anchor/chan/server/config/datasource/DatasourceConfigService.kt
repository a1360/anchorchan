package com.anchor.chan.server.config.datasource

import com.zaxxer.hikari.HikariDataSource
import org.flywaydb.core.Flyway
import ratpack.service.Service
import ratpack.service.StartEvent
import ratpack.service.StopEvent
import javax.inject.Inject

internal class DatasourceConfigService @Inject constructor(
    private val hikariDataSource: HikariDataSource
) : Service {

    override fun onStart(event: StartEvent?) {
        super.onStart(event)
        performMigrations()
    }

    private fun performMigrations() {
        Flyway.configure()
            .dataSource(hikariDataSource)
            .locations("classpath:db/migration")
            .load()
            .migrate()
    }

    override fun onStop(event: StopEvent?) {
        super.onStop(event)
        hikariDataSource.close()
    }
}
