package com.anchor.chan.desktop.screen.threadbrowser

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.anchor.chan.architecture.ui.Renderer
import com.anchor.chan.desktop.LexemeConstants
import com.anchor.chan.desktop.screen.UiConstants
import com.anchor.chan.desktop.screen.roundedClickable
import com.anchor.chan.desktop.screen.threadbrowser.content.boards.SelectBoardRenderer
import com.anchor.chan.desktop.screen.threadbrowser.content.boards.SelectThreadRenderer
import com.anchor.chan.desktop.screen.threadbrowser.content.thread.ThreadRenderer
import com.anchor.chan.desktop.screen.threadbrowser.watcher.renderThreadWatcher
import com.anchor.chan.desktop.screen.viewmodel.ThreadBrowserViewModel
import com.anchor.chan.desktop.screen.viewmodel.ThreadBrowserViewModel.ContentViewModel.Empty
import com.anchor.chan.desktop.screen.viewmodel.ThreadBrowserViewModel.ContentViewModel.SelectBoardViewModel
import com.anchor.chan.desktop.screen.viewmodel.ThreadBrowserViewModel.ContentViewModel.SelectThreadViewModel
import com.anchor.chan.desktop.screen.viewmodel.ThreadBrowserViewModel.ContentViewModel.ThreadViewModel
import com.anchor.chan.desktop.screen.viewmodel.ThreadBrowserViewModel.MenuBarViewModel
import com.anchor.chan.desktop.ui.CrossfadeModel
import compose.icons.FeatherIcons
import compose.icons.feathericons.Settings
import compose.icons.feathericons.User

@ExperimentalFoundationApi
@ExperimentalComposeUiApi
object ThreadBrowserRenderer : Renderer<ThreadBrowserViewModel> {

    @Composable
    override fun render(model: ThreadBrowserViewModel) {
        Row(
            modifier = Modifier
                .fillMaxSize()
                .background(MaterialTheme.colors.background)
        ) {
            createSideBar(model)
            renderThreadWatcher(model.threadWatcherViewModel)
            Column(
                modifier = Modifier
                    .fillMaxHeight()
                    .weight(0.7f)
            ) {
                createConnectionNotification(model.connectionStatusViewModel)
                createContentScreen(model.contentViewModel)
            }
        }
    }

    @Composable
    private fun createSideBar(model: ThreadBrowserViewModel) {
        Column(
            modifier = Modifier
                .width(50.dp)
                .fillMaxHeight()
                .background(MaterialTheme.colors.primary)
                .padding(top = UiConstants.CONTENT_PADDING / 2),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            settingsButtons(model.menuBarViewModel)
        }
    }

    @Composable
    private fun settingsButtons(menuBarModel: MenuBarViewModel) {
        Image(
            painter = rememberVectorPainter(FeatherIcons.Settings),
            contentDescription = LexemeConstants.SideBar.SETTINGS,
            modifier = Modifier
                .size(UiConstants.BUTTON_SIZE)
                .roundedClickable(onClick = menuBarModel.settingsClicked)
                .padding(UiConstants.SMALLER_BUTTON_PADDING),
            colorFilter = ColorFilter.tint(MaterialTheme.colors.onPrimary)
        )
        Image(
            painter = rememberVectorPainter(FeatherIcons.User),
            contentDescription = LexemeConstants.SideBar.ACCOUNT_SETTINGS,
            modifier = Modifier
                .size(UiConstants.BUTTON_SIZE)
                .roundedClickable(onClick = menuBarModel.accountSettingsClicked)
                .padding(UiConstants.SMALLER_BUTTON_PADDING),
            colorFilter = ColorFilter.tint(MaterialTheme.colors.onPrimary)
        )
    }

    @Composable
    private fun createConnectionNotification(
        connectionStatusViewModel: ThreadBrowserViewModel.ConnectionStatusViewModel
    ) {
        AnimatedVisibility(
            visible = connectionStatusViewModel.displayStatus,
            enter = slideInVertically(),
            exit = slideOutVertically()
        ) {
            Text(
                text = connectionStatusViewModel.loadingMessage ?: "",
                modifier = Modifier
                    .fillMaxWidth()
                    .background(connectionStatusViewModel.color)
                    .padding(
                        start = UiConstants.CONTENT_PADDING,
                        end = UiConstants.CONTENT_PADDING,
                        top = UiConstants.CONTENT_PADDING / 2,
                        bottom = UiConstants.CONTENT_PADDING / 2
                    ),
                color = Color.White,
                textAlign = TextAlign.Center,
                maxLines = 2
            )
        }
    }

    @ExperimentalFoundationApi
    @ExperimentalComposeUiApi
    @Composable
    private fun createContentScreen(model: ThreadBrowserViewModel.ContentViewModel) {
        CrossfadeModel(model) {
            when (it) {
                is SelectBoardViewModel -> SelectBoardRenderer.render(it)
                is SelectThreadViewModel -> SelectThreadRenderer.render(it)
                is ThreadViewModel -> ThreadRenderer.render(it)
                is Empty -> Unit
            }.let { }
        }
    }
}
