package com.anchor.chan.server.account.datasource

import com.anchor.publickey.crypto.PublicKeyHash
import org.jdbi.v3.core.mapper.RowMapper
import org.jdbi.v3.core.statement.StatementContext
import java.sql.ResultSet

data class AccountEntity(
    val accountId: String,
    val publicKeyHash: PublicKeyHash,
    val publicKey: String,
    val accountChain: List<String>,
) {

    companion object {

        val generatedFields: Array<String> = arrayOf("accountId")
    }
}

internal class AccountEntityMapper : RowMapper<AccountEntity> {

    override fun map(rs: ResultSet, ctx: StatementContext): AccountEntity =
        AccountEntity(
            accountId = rs.getString("accountId"),
            publicKeyHash = PublicKeyHash(rs.getString("publicKeyHash")),
            publicKey = rs.getString("publicKey"),
            accountChain = rs.getString("string_agg").split(","),
        )
}

data class AccountEntityGeneratedFields(
    val accountId: String
)

internal class AccountEntityGeneratedFieldsMapper : RowMapper<AccountEntityGeneratedFields> {

    override fun map(rs: ResultSet, ctx: StatementContext): AccountEntityGeneratedFields =
        AccountEntityGeneratedFields(
            accountId = rs.getString("accountId"),
        )
}
