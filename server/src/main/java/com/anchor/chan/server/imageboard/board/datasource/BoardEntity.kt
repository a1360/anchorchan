package com.anchor.chan.server.imageboard.board.datasource

import org.jdbi.v3.core.mapper.RowMapper
import org.jdbi.v3.core.statement.StatementContext
import java.sql.ResultSet

data class BoardEntity(
    val boardId: String,
    val boardName: String,
    val shortName: String
) {

    companion object {

        val generatedFields: Array<String> = arrayOf("boardId")
    }
}

internal class BoardEntityMapper : RowMapper<BoardEntity> {

    override fun map(rs: ResultSet, ctx: StatementContext): BoardEntity =
        BoardEntity(
            boardId = rs.getString("boardId"),
            boardName = rs.getString("boardName"),
            shortName = rs.getString("shortName")
        )
}

data class BoardEntityGeneratedFields(
    val boardId: Long
)

internal class BoardEntityGeneratedFieldsMapper : RowMapper<BoardEntityGeneratedFields> {

    override fun map(rs: ResultSet, ctx: StatementContext): BoardEntityGeneratedFields =
        BoardEntityGeneratedFields(
            boardId = rs.getLong("boardId"),
        )
}
