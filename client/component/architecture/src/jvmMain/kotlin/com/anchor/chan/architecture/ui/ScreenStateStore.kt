package com.anchor.chan.architecture.ui

import com.anchor.chan.architecture.StateStore
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class ScreenStateStore<T>(
    initial: T,
    private val dispatcher: CoroutineDispatcher = Dispatchers.Default
) : StateStore<T>(initial) {

    private var currentScreenScope: CoroutineScope? = null

    fun updateScreen(screenBuilder: ScreenBuilder<out T>) {
        val scope = CoroutineScope(dispatcher + SupervisorJob())
        currentScreenScope?.cancel()
        currentScreenScope = scope
        scope.launch {
            screenBuilder.build().collect { newModel ->
                update { newModel }
            }
        }
    }
}
