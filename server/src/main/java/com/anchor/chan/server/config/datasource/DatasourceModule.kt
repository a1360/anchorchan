package com.anchor.chan.server.config.datasource

import com.google.inject.Provides
import com.google.inject.Scopes
import com.google.inject.Singleton
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import com.zaxxer.hikari.pool.HikariPool
import org.jdbi.v3.core.Jdbi
import ratpack.guice.ConfigurableModule

class DatasourceModule : ConfigurableModule<DatasourceConfig>() {

    override fun configure() {
        bind(DatasourceConfigService::class.java).`in`(Scopes.SINGLETON)
    }

    @Provides
    @Singleton
    private fun provideJdbi(hikariDataSource: HikariDataSource): Jdbi =
        Jdbi.create(hikariDataSource)

    @Provides
    @Singleton
    private fun provideHikariConfig(config: DatasourceConfig): HikariConfig =
        HikariConfig().apply {
            jdbcUrl = config.jdbcUrl
            username = config.username
            password = config.password
        }

    @Provides
    @Singleton
    fun hikariDataSource(config: HikariConfig): HikariDataSource =
        HikariDataSource(config)

    @Provides
    @Singleton
    fun hikariPool(hikariDataSource: HikariDataSource): HikariPool =
        hikariDataSource.hikariPoolMXBean as HikariPool
}

class DatasourceConfig(
    val jdbcUrl: String,
    val username: String,
    val password: String
)
