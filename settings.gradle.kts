pluginManagement {
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
        maven { url = uri("https://maven.pkg.jetbrains.space/public/p/compose/dev") }
    }
}

rootProject.name = "anchorchan"

include(":client:android")
include(":client:desktop")
include(":client:common-domain")
include(":client:component:architecture")
include(":client:component:network-client")
include(":client:component:client-models")
include(":client:component:logger")
include(":client:component:account-management")
include(":client:library:bandwidthestimate")
include(":client:library:i2p")

include(":api")

include(":common:library:hashcash")
include(":common:library:publickey-crypto")

include(":server")
include(":server:server-acceptance-test")
