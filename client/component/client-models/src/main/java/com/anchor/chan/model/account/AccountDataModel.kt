package com.anchor.chan.model.account

import com.anchor.publickey.crypto.AssymetricPublicKey
import com.anchor.publickey.crypto.PublicKeyHash

data class AccountDataModel(
    val publicKeyHash: PublicKeyHash,
    val publicKey: AssymetricPublicKey,
    var accountChain: List<String>,
) {

    val chainLength: Int
        get() = accountChain.size
}
