package com.anchor.chan.server.account

import com.anchor.chan.api.account.ProofOfWorkDTO
import com.anchor.chan.server.util.await
import com.anchor.chan.server.util.blockingAccept
import com.anchor.chan.server.util.blockingRenderJson
import com.anchor.publickey.crypto.PublicKeyHash
import org.slf4j.LoggerFactory
import ratpack.kotlin.handling.KChainAction
import javax.inject.Inject

private val logger = LoggerFactory.getLogger(AccountRouter::class.java)

class AccountRouter @Inject constructor(
    private val accountService: AccountService
) : KChainAction() {

    override fun execute() {
        get(":accountHash") {
            val key = it.pathTokens["accountHash"] ?: throw IllegalArgumentException("Missing public key hash")
            blockingRenderJson {
                accountService.getAccount(PublicKeyHash(key))
            }
        }

        get("chain/settings") {
            blockingRenderJson {
                accountService.getProofOfWorkSettings()
            }
        }
        post("chain/:data") {
            val key = it.pathTokens["data"] ?: throw IllegalArgumentException("Missing public key")
            blockingAccept {
                val token = it.parse(ProofOfWorkDTO::class.java).await()?.token
                    ?: throw IllegalArgumentException("Missing proof of work token")
                accountService.uploadProofOfWork(key, token)
            }
        }
    }
}
