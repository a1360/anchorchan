plugins {
    kotlin("jvm")
    id("idea")
}

kotlin {
    jvmToolchain {
        (this as JavaToolchainSpec).languageVersion.set(JavaLanguageVersion.of("8"))
    }
}

dependencies {
    implementation(project(":common:library:publickey-crypto"))
    implementation(Deps.kotlinCoroutines)
    implementation(Deps.slf4j)
    implementation(Deps.logback)
    implementation(Deps.logbackClassic)

    testImplementation(Deps.kotest)
    testImplementation(Deps.kotestAssertions)
}
