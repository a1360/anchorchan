package com.anchor.chan.common.connection

import com.anchor.chan.architecture.StateStore

class I2pConnectionStateStore : StateStore<I2pConnectionState>(I2pConnectionState()) {

    fun updateConnectionState(connectionState: ConnectionState) {
        updateAsync {
            it.copy(connectionState = connectionState)
        }
    }

    suspend fun updateConnectionState(calculateConnectionState: (old: ConnectionState) -> ConnectionState) {
        update { it.copy(connectionState = calculateConnectionState(it.connectionState)) }
    }

    fun onSuccessfulRequestMade() {
        val state = state.value
        if (state.connectionState == ConnectionState.CONNECTED_AND_BUILDING_NETWORK) {
            updateAsync { it.copy(connectionState = ConnectionState.CONNECTED) }
        }
    }
}
