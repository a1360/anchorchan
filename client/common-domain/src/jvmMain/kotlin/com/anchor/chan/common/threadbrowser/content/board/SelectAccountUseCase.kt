package com.anchor.chan.common.threadbrowser.content.board

import com.anchor.chan.account.AccountState.AccountData
import com.anchor.chan.account.AccountStateStore

class SelectAccountUseCase(
    private val stateStore: AccountStateStore,
) {

    suspend fun fetchBestAccount(): AccountData? =
        stateStore
            .state
            .value
            .accounts
            .values
            .filterByMaxChainLength()
            .randomOrNull()

    private fun Collection<AccountData>.filterByMaxChainLength(): Collection<AccountData> {
        val list = sortedByDescending { it.chain.size }
        val maxValue = list.firstOrNull()?.chain?.size
        return list
            .takeIf { maxValue != null }
            ?.filter { it.chain.size == maxValue }
            ?: emptyList()
    }

    suspend fun fetchAccountForThread(threadId: String): AccountData? {
        val accountExistsForThread = stateStore.state.value
            .accounts
            .values
            .find { it.associatedThreads.contains(threadId) }

        return if (accountExistsForThread == null) {
            val newAccount = stateStore.state.value
                .accounts.values
                .filterByMaxChainLength()
                .minByOrNull { it.associatedThreads.size }
            newAccount?.keys?.publicKey
                ?.let { stateStore.associateAccountWithThread(threadId, it) }
            newAccount
        } else {
            accountExistsForThread
        }
    }
}
