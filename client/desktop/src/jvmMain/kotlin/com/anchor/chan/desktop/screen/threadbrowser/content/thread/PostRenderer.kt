package com.anchor.chan.desktop.screen.threadbrowser.content.thread

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.text.ClickableText
import androidx.compose.foundation.text.selection.DisableSelection
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import com.anchor.chan.desktop.LexemeConstants
import com.anchor.chan.desktop.screen.UiConstants
import com.anchor.chan.desktop.screen.processSingleLinePostStyle
import com.anchor.chan.desktop.screen.viewmodel.ThreadBrowserViewModel.ContentViewModel.ThreadViewModel.PostViewModel

@Composable
fun renderPostModel(
    postsMap: Map<String, PostViewModel>,
    postIdsFromCurrentUser: Set<String>,
    postViewModel: PostViewModel,
    onIdClicked: (String) -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .background(MaterialTheme.colors.surface)
            .padding(UiConstants.POST_PADDING)
    ) {
        postTitleBar(postViewModel, onIdClicked)
        Spacer(modifier = Modifier.height(16.dp))
        message(postsMap, postIdsFromCurrentUser, postViewModel, onIdClicked)
    }
}

@Composable
private fun postTitleBar(postViewModel: PostViewModel, onIdClicked: (String) -> Unit) {
    Row(
        modifier = Modifier
            .fillMaxWidth(),
        horizontalArrangement = Arrangement.Start
    ) {
        SelectionContainer {
            Text(
                text = postViewModel.title ?: "",
                style = MaterialTheme.typography.body2,
                color = MaterialTheme.colors.onSurface,
                fontWeight = FontWeight.Bold,
            )
        }
        Spacer(modifier = Modifier.weight(1f))
        if (postViewModel.accountId != null) {
            Text(
                modifier = Modifier
                    .clickable { onIdClicked(postViewModel.id) },
                text = LexemeConstants.ThreadBrowser.Posts.USER_ID + (postViewModel.accountId),
                style = MaterialTheme.typography.body2,
                color = MaterialTheme.colors.onSurface,
            )
            Spacer(modifier = Modifier.width(16.dp))
        }
        SelectionContainer {
            Text(
                text = postViewModel.timestamp,
                style = MaterialTheme.typography.body2,
                color = MaterialTheme.colors.onSurface,
            )
        }
        Spacer(modifier = Modifier.width(16.dp))
        Text(
            modifier = Modifier
                .clickable { onIdClicked(postViewModel.id) },
            text = LexemeConstants.ThreadBrowser.Posts.ID + postViewModel.id,
            style = MaterialTheme.typography.body2,
            color = MaterialTheme.colors.onSurface,
            textDecoration = TextDecoration.Underline
        )
    }
}

@Composable
private fun message(
    postsMap: Map<String, PostViewModel>,
    postIdsFromCurrentUser: Set<String>,
    postViewModel: PostViewModel,
    onIdClicked: (String) -> Unit
) {
    val greenTextSpanStyle = UiConstants.Post.greenTextSpanStyle()
    val openedLinks: MutableState<Set<Int>> = remember { mutableStateOf(emptySet()) }
    SelectionContainer {
        Column {
            postViewModel.message.lines().forEachIndexed { index, it ->
                val text = it.processSingleLinePostStyle(greenTextSpanStyle, postIdsFromCurrentUser)
                val linkedPost = text
                    .getStringAnnotations(UiConstants.Post.LINK_POST_TAG, 0, text.length)
                    .firstOrNull()
                    ?.item
                    ?.let { postsMap.get(it) }
                if (linkedPost != null) {
                    clickableMessageText(index, text, openedLinks)
                    linkedPost(
                        index,
                        linkedPost,
                        postsMap,
                        postIdsFromCurrentUser,
                        openedLinks,
                        onIdClicked
                    )
                } else {
                    normalMessageText(text)
                }
            }
        }
    }
}

@Composable
private fun linkedPost(
    index: Int,
    linkedPost: PostViewModel?,
    postsMap: Map<String, PostViewModel>,
    postIdsFromCurrentUser: Set<String>,
    openedLinks: MutableState<Set<Int>>,
    onIdClicked: (String) -> Unit,
) {
    if (openedLinks.value.contains(index) && linkedPost != null) {
        DisableSelection {
            Box(
                modifier = Modifier
                    .padding(UiConstants.POST_SPACING)
                    .background(MaterialTheme.colors.onSurface)
                    .padding(UiConstants.POST_SPACING),
            ) {
                renderPostModel(postsMap, postIdsFromCurrentUser, linkedPost, onIdClicked)
            }
        }
    }
}

@Composable
private fun clickableMessageText(
    index: Int,
    text: AnnotatedString,
    openedLinks: MutableState<Set<Int>>
) {
    DisableSelection {
        ClickableText(
            text = text,
            modifier = Modifier,
            style = MaterialTheme.typography.body1.copy(color = MaterialTheme.colors.onSurface),
            onClick = { hit ->
                if (openedLinks.value.contains(index)) {
                    openedLinks.value = openedLinks.value.minus(index)
                } else {
                    openedLinks.value = openedLinks.value.plus(index)
                }
            }
        )
    }
}

@Composable
private fun normalMessageText(text: AnnotatedString) {
    Text(
        text = text,
        modifier = Modifier,
        style = MaterialTheme.typography.body1,
        color = MaterialTheme.colors.onSurface
    )
}
