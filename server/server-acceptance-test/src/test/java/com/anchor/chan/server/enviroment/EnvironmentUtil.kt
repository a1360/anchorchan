package com.anchor.chan.server.enviroment

import com.anchor.chan.server.TestServer
import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.core.locator.ClasspathSqlLocator
import org.slf4j.LoggerFactory
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.containers.output.Slf4jLogConsumer

fun TestServer.jdbi() =
    server!!.registry.get().get(Jdbi::class.java)

fun TestServer.insertDefaultData() {
    val sqlStatements = ClasspathSqlLocator.create().locate("db/setup_test_data")
    jdbi().useHandle<Exception> {
        it.createUpdate(sqlStatements)
            .execute()
    }
}

fun createTestContainerDatabase(): PostgreSQLContainer<Nothing> =
    PostgreSQLContainer<Nothing>("postgres:11.1").apply {
        withUsername("sa")
        withPassword("")
        withDatabaseName("testContainer")
        withLogConsumer(Slf4jLogConsumer(LoggerFactory.getLogger("TestContainer")))
    }
