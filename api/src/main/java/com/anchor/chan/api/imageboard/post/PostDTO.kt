package com.anchor.chan.api.imageboard.post

import java.time.Instant

data class PostDTO(
    val postId: String?,
    val message: String?,
    val timestamp: Instant?,
    val threadId: String?,
    val title: String?,
    val publicKeyHash: String? = null,
    val signature: String? = null,
)
