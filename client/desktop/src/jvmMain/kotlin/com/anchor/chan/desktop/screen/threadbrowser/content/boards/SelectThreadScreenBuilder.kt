package com.anchor.chan.desktop.screen.threadbrowser.content.boards

import com.anchor.chan.architecture.ui.ScreenBuilder
import com.anchor.chan.common.threadbrowser.ThreadBrowserComponent
import com.anchor.chan.common.threadbrowser.content.board.LoadThreadsUseCase
import com.anchor.chan.common.threadbrowser.content.board.SubmitThreadUseCase
import com.anchor.chan.common.threadbrowser.watcher.ThreadWatcherContent
import com.anchor.chan.common.threadbrowser.watcher.ThreadWatcherStateStore
import com.anchor.chan.desktop.screen.UiConstants
import com.anchor.chan.desktop.screen.viewmodel.ThreadBrowserViewModel
import com.anchor.chan.model.imageboard.BoardModel
import com.anchor.chan.model.imageboard.ThreadModel
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch

class SelectThreadScreenBuilder(
    private val boards: List<BoardModel>,
    private val component: ThreadBrowserComponent
) : ScreenBuilder<ThreadBrowserViewModel.ContentViewModel.SelectThreadViewModel> {

    private val loadThreadUseCase = component.loadThreadsUseCase
    private val submitThreadUseCase = component.submitThreadUseCase
    private val watcherStateStore = component.threadWatcherStateStore

    override suspend fun build(): Flow<ThreadBrowserViewModel.ContentViewModel.SelectThreadViewModel> =
        SelectThreadScreen(boards, loadThreadUseCase, submitThreadUseCase, watcherStateStore).build()

    private class SelectThreadScreen(
        private val boards: List<BoardModel>,
        private val loadThreadUseCase: LoadThreadsUseCase,
        private val submitThreadUseCase: SubmitThreadUseCase,
        private val watcherStateStore: ThreadWatcherStateStore,
    ) {

        private val onThreadSubmittedError = MutableStateFlow<String?>(null)
        private val onThreadSelected: (ThreadModel) -> Unit = {
            watcherStateStore.setSelected(ThreadWatcherContent.Thread(it))
        }

        @DelicateCoroutinesApi
        private val onNewThreadSubmitted: (title: String, message: String) -> Unit = { title, message ->
            GlobalScope.launch {
                submitThreadUseCase.submitThread(title, message, boards.map { it.boardId })
                    .fold(
                        ifLeft = { onThreadSubmittedError.emit(it) },
                        ifRight = { watcherStateStore.setSelected(ThreadWatcherContent.Thread(it)) },
                    )
            }
        }

        fun build(): Flow<ThreadBrowserViewModel.ContentViewModel.SelectThreadViewModel> =
            loadThreadUseCase.loadThreadsForBoards(boards.map { it.boardId })
                .combine(onThreadSubmittedError) { threads, error -> threads.toViewModel(error) }
                .onStart { emit(loading()) }

        private fun loading() =
            ThreadBrowserViewModel.ContentViewModel.SelectThreadViewModel(
                isLoading = true,
                threads = emptyList(),
                onThreadSelected = onThreadSelected,
                onThreadSubmitted = onNewThreadSubmitted,
                allowedToCreatePost = true,
                onThreadSubmittedError = null,
            )

        private fun List<ThreadModel>.toViewModel(submitThreadError: String?) =
            ThreadBrowserViewModel.ContentViewModel.SelectThreadViewModel(
                isLoading = false,
                threads = map { it.toViewModel() },
                onThreadSelected = onThreadSelected,
                onThreadSubmitted = onNewThreadSubmitted,
                allowedToCreatePost = true,
                onThreadSubmittedError = submitThreadError,
            )

        private fun ThreadModel.toViewModel() =
            ThreadBrowserViewModel.ContentViewModel.SelectThreadViewModel.ThreadPreviewViewModel(
                initialPost = ThreadBrowserViewModel.ContentViewModel.ThreadViewModel.PostViewModel(
                    id = initialPost.id,
                    title = initialPost.title,
                    message = initialPost.message,
                    timestamp = UiConstants.Time.defaultFormat(initialPost.timestamp),
                    isFromCurrentUser = false,
                    accountId = null,
                ),
                lastUpdateTimestamp = UiConstants.Time.defaultFormat(lastUpdated),
                model = this,
            )
    }
}
