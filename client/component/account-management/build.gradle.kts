import org.jetbrains.compose.compose

plugins {
    kotlin("multiplatform")
    id("org.jetbrains.compose") version DepVersions.DESKTOP_COMPOSE
    kotlin("plugin.serialization") version "1.5.0"
}

kotlin {
    jvm {
        compilations.all {
            kotlinOptions.jvmTarget = "11"
        }
    }
    sourceSets {
        val jvmMain by getting {
            dependencies {
                implementation(compose.foundation)

                implementation(project(":client:component:network-client"))
                implementation(project(":client:component:client-models"))
                implementation(project(":client:component:architecture"))
                implementation(project(":common:library:hashcash"))
                implementation(project(":common:library:publickey-crypto"))
                implementation(project(":client:component:logger"))

                implementation(Deps.kotlinxSerialization)
                implementation(Deps.resiliance4jRetry)
                implementation(Deps.resiliance4jKotlin)
                implementation(Deps.caffeineCache)
            }
        }
        val jvmTest by getting
    }
}
