package com.anchor.chan.account

import com.anchor.chan.account.AccountState.AccountData
import com.anchor.chan.architecture.StateStore
import com.anchor.publickey.crypto.AssymetricPublicKey

class AccountStateStore : StateStore<AccountState>(AccountState()) {

    suspend fun delete(publicKey: String) =
        update { oldState -> oldState.copy(accounts = oldState.accounts.filter { it.key.key != publicKey }) }

    suspend fun associateAccountWithThread(threadId: String, key: AssymetricPublicKey) {
        update { state ->
            val account = state.accounts[key]
            account
                ?.copy(associatedThreads = account.associatedThreads.plus(threadId))
                ?.let { state.accounts.plus(it.keys.publicKey to it) }
                ?.let { state.copy(accounts = it) }
                ?: state
        }
    }

    internal suspend fun loadedFromCache(cachedState: AccountState?) {
        update {
            val newState = cachedState ?: it
            newState.copy(isLoadedFromCache = true)
        }
    }

    internal suspend fun updateAccounts(
        updateMap: (Map<AssymetricPublicKey, AccountData>) -> Map<AssymetricPublicKey, AccountData>
    ) {
        update {
            val newAccounts = updateMap(it.accounts)
            it.copy(accounts = newAccounts)
        }
    }

    internal suspend fun updateAccount(
        key: AssymetricPublicKey,
        action: (AccountData) -> AccountData
    ) {
        update { state ->
            val account = state.accounts[key]
            account
                ?.let(action)
                ?.let { state.accounts.plus(it.keys.publicKey to it) }
                ?.let { state.copy(accounts = it) }
                ?: state
        }
    }

    internal suspend fun setTargetAccountLength(length: Int) {
        update { it.copy(targetAccountLength = length) }
    }
}
