package com.anchor.chan.common.configuration

import androidx.compose.material.Colors
import androidx.compose.ui.graphics.Color
import kotlinx.serialization.Serializable

@Serializable
enum class ColorConfiguration {
    FOREST, DARK_COFFEE_SHOP, NEON;

    fun toColors(): Colors =
        when (this) {
            // https://www.materialpalette.com/light-green/brown
            FOREST ->
                Colors(
                    primary = Color(139, 195, 74),
                    primaryVariant = Color(104, 159, 56),
                    secondary = Color(121, 85, 72),
                    secondaryVariant = Color(175, 63, 22),
                    background = Color.White,
                    surface = Color(220, 237, 200),
                    error = Color.Red,
                    onPrimary = Color.White,
                    onSecondary = Color.White,
                    onBackground = Color(33, 33, 33),
                    onSurface = Color(33, 33, 33),
                    onError = Color.White,
                    isLight = true
                )
            // https://www.color-hex.com/color-palette/109780
            DARK_COFFEE_SHOP ->
                Colors(
                    primary = Color(165, 135, 109),
                    primaryVariant = Color(175, 63, 22),
                    secondary = Color(117, 91, 55),
                    secondaryVariant = Color(122, 88, 85),
                    background = Color(55, 43, 55),
                    surface = Color(72, 61, 69),
                    error = Color.Red,
                    onPrimary = Color.White,
                    onSecondary = Color.White,
                    onBackground = Color.White,
                    onSurface = Color.White,
                    onError = Color.White,
                    isLight = false
                )
            // https://www.color-hex.com/color-palette/108463
            NEON ->
                Colors(
                    primary = Color(230, 0, 172),
                    primaryVariant = Color(230, 0, 172),
                    secondary = Color(194, 0, 182),
                    secondaryVariant = Color(112, 0, 165),
                    background = Color(2, 0, 75),
                    surface = Color(6, 0, 140),
                    error = Color.Red,
                    onPrimary = Color(112, 0, 165),
                    onSecondary = Color.White,
                    onBackground = Color.White,
                    onSurface = Color.White,
                    onError = Color.White,
                    isLight = false
                )
        }
}
