package com.anchor.chan.network.client

import com.anchor.chan.api.account.AccountDataDTO
import com.anchor.chan.api.account.ProofOfWorkDTO
import com.anchor.chan.api.account.ProofOfWorkSettingsDTO
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

internal interface AccountNetworkService {

    @GET("/account/{publicKeyHash}")
    suspend fun getAccountData(@Path("publicKeyHash") publicKeyHash: String): Response<AccountDataDTO>

    @POST("/account/chain/{publicKey}")
    suspend fun uploadProofOfWorkBlock(@Path("publicKey") publicKey: String, @Body proofOfWork: ProofOfWorkDTO): Response<Unit>

    @GET("/account/chain/settings")
    suspend fun getProofOfWorkSettings(): Response<ProofOfWorkSettingsDTO>
}
