package com.anchor.chan.server.imageboard

import com.anchor.chan.api.account.SignatureBlockDTO
import com.anchor.chan.model.account.AccountDataModel
import com.anchor.chan.model.imageboard.PostModel
import com.anchor.chan.model.imageboard.ThreadModel
import com.anchor.publickey.crypto.AssymetricKeyPair
import com.anchor.publickey.crypto.ecc.EccGenerator
import com.anchor.publickey.crypto.ecc.EccVerifier
import com.anchor.publickey.crypto.toCreateThreadSignature
import com.anchor.publickey.crypto.toSha1Hex
import com.anchor.publickey.crypto.toSubmitPostSignature
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.collections.shouldContainAll
import io.kotest.matchers.shouldBe
import java.time.Instant

class ConsistentThreadIdFunctionalTest : FunSpec({

    val environment = ImageboardEnvironment()
    val keyGen = EccGenerator()
    val verifier = EccVerifier()

    fun createNewFullAccessUser(): AssymetricKeyPair {
        val user = keyGen.generateKey()
        environment.accountUseCase.setAccountChainLength(user.publicKey, Int.MAX_VALUE)
        return user
    }

    suspend fun AssymetricKeyPair.submitPost(threadId: String) {
        environment.postService.submitPost(this, threadId, "Title", "Message")
    }

    suspend fun createThread(numUsers: Int, numPosts: Int): Pair<ThreadModel, List<AssymetricKeyPair>> {
        val users = List(numUsers) { createNewFullAccessUser() }

        val thread = environment.boardService.submitThread(users.first(), "Title", "Message", listOf("0"))
        var userIndex = 1
        repeat(numPosts - 1) {
            if (userIndex > users.lastIndex) {
                userIndex = 0
            }
            users[userIndex].submitPost(thread.threadId)
            userIndex++
        }

        return thread to users
    }

    beforeSpec {
        environment.start()
        environment.insertDefaultData()
    }

    afterSpec {
        environment.clearEnvironmentData()
        environment.close()
    }

    context("Fetching an entire thread") {

        test(
            "GIVEN a thread with 10 unique posters " +
                "WHEN first requesting the thread " +
                "THEN receive all 10 users"
        ) {
            val user = createNewFullAccessUser()
            val (thread, otherPosters) = createThread(10, 10)

            val networkThread = environment.postService.requestUpdate(user, listOf(thread.threadId), Instant.EPOCH)
            val accounts = networkThread.accounts

            accounts.keys shouldContainAll otherPosters.map { it.publicKey.toSha1Hex() }
        }

        test(
            "GIVEN a thread with 1 unique poster and multiple posts " +
                "WHEN first requesting the thread " +
                "THEN receive only 1 user"
        ) {
            val user = createNewFullAccessUser()
            val (thread, otherPosters) = createThread(1, 10)

            val networkThread = environment.postService.requestUpdate(user, listOf(thread.threadId), Instant.EPOCH)
            val accounts = networkThread.accounts

            accounts.size shouldBe 1
            accounts.keys shouldContainAll otherPosters.map { it.publicKey.toSha1Hex() }
        }

        test(
            "GIVEN a thread with a single post " +
                "WHEN requesting the thread " +
                "THEN receive only 1 user"
        ) {
            val user = createNewFullAccessUser()
            val (thread, otherPosters) = createThread(1, 1)

            val networkThread = environment.postService.requestUpdate(user, listOf(thread.threadId), Instant.EPOCH)
            val accounts = networkThread.accounts

            accounts.size shouldBe 1
            accounts.keys shouldContainAll otherPosters.map { it.publicKey.toSha1Hex() }
        }
    }

    context("Fetching thread updates") {

        suspend fun createThreadWithSomeUpdates(
            numUpdates: Int,
            numUsers: Int,
            usersUpdate: Int,
            updatesAreNewUsers: Boolean = false
        ): Triple<ThreadModel, List<AssymetricKeyPair>, Instant> {
            val user = createNewFullAccessUser()
            val (thread, otherPosters) = createThread(numUsers, numUpdates)
            val updatePosters = if (updatesAreNewUsers) {
                List(usersUpdate) { createNewFullAccessUser() }
            } else {
                otherPosters.subList(0, usersUpdate)
            }
            val threadWithFirstUpdates =
                environment.postService.requestUpdate(user, listOf(thread.threadId), Instant.EPOCH)
            val timestamp = threadWithFirstUpdates.latestTimestamp

            var userIndex = 0
            repeat(numUpdates) {
                if (userIndex > updatePosters.lastIndex) {
                    userIndex = 0
                }
                updatePosters[userIndex].submitPost(thread.threadId)
                userIndex++
            }

            return Triple(thread, updatePosters, timestamp)
        }

        test(
            "GIVEN a thread with 10 unique posters " +
                "WHEN 2 existing users have posted " +
                "THEN receive just those 2 users"
        ) {
            val user = createNewFullAccessUser()
            val (thread, updatePosters, timestamp) =
                createThreadWithSomeUpdates(numUpdates = 2, numUsers = 10, usersUpdate = 2)

            val networkThread = environment.postService.requestUpdate(user, listOf(thread.threadId), timestamp)
            val accounts = networkThread.accounts

            accounts.size shouldBe 2
            accounts.keys shouldContainAll updatePosters.map { it.publicKey.toSha1Hex() }
        }

        test(
            "GIVEN a thread with 10 unique posters " +
                "WHEN 2 new users have posted " +
                "THEN receive just those 2 users"
        ) {
            val user = createNewFullAccessUser()
            val (thread, updatePosters, timestamp) =
                createThreadWithSomeUpdates(numUpdates = 2, numUsers = 10, usersUpdate = 2, updatesAreNewUsers = true)

            val networkThread = environment.postService.requestUpdate(user, listOf(thread.threadId), timestamp)
            val accounts = networkThread.accounts

            accounts.size shouldBe 2
            accounts.keys shouldContainAll updatePosters.map { it.publicKey.toSha1Hex() }
        }

        test(
            "GIVEN a thread with no updates " +
                "WHEN requesting updates " +
                "THEN return no users"
        ) {
            val user = createNewFullAccessUser()
            val (thread, _) = createThread(10, 10)

            val networkThread = environment.postService.requestUpdate(user, listOf(thread.threadId), Instant.now())
            val accounts = networkThread.accounts

            accounts.size shouldBe 0
        }
    }

    context("Validating another posters signature") {

        fun PostModel.verify(
            account: AccountDataModel,
            threadId: String,
            boardIds: List<String>,
            isFirst: Boolean
        ): Boolean {
            val signatureBlock = SignatureBlockDTO(account.publicKey.key, timestamp, null)
            val message = if (isFirst) {
                signatureBlock.toCreateThreadSignature(
                    boardIds,
                    message, title!!
                )
            } else {
                signatureBlock.toSubmitPostSignature(threadId, title, message)
            }

            return verifier.verify(account.publicKey, message, signature)
        }

        test("GIVEN a valid thread of ids WHEN verifying THEN all pass") {
            val user = createNewFullAccessUser()
            val (thread, _) = createThread(10, 10)
            val updatedThread =
                environment.postService.requestUpdate(user, listOf(thread.threadId), Instant.EPOCH)

            updatedThread.posts.forEachIndexed { index, post ->
                val isVerified = post.verify(
                    updatedThread.accounts[post.accountKeyHash]!!,
                    thread.threadId, thread.boardIds, index == 0
                )
                isVerified shouldBe true
            }
        }

        data class TamperedPostModel(
            val tamper: (PostModel) -> PostModel,
            val fieldName: String,
            val isValidForFirstPost: Boolean = true,
        )

        listOf(
            TamperedPostModel(
                fieldName = "title",
                tamper = { it.copy(title = "Tampered") },
            ),
            TamperedPostModel(
                fieldName = "message",
                tamper = { it.copy(message = "Tampered") },
            ),
            TamperedPostModel(
                fieldName = "timestamp",
                tamper = { it.copy(timestamp = Instant.now()) },
            ),
            TamperedPostModel(
                fieldName = "threadId",
                tamper = { it.copy(threadId = "Tampered") },
                isValidForFirstPost = false,
            ),
        ).forEach {

            suspend fun testTamperedInput(first: Boolean) {
                val user = createNewFullAccessUser()
                val (thread, _) = createThread(10, 10)
                val updatedThread =
                    environment.postService.requestUpdate(user, listOf(thread.threadId), Instant.EPOCH)
                val tamperedPost = it.tamper(updatedThread.posts[if (first) 0 else 1])

                val isVerified = tamperedPost.verify(
                    updatedThread.accounts[tamperedPost.accountKeyHash]!!, tamperedPost.threadId,
                    thread.boardIds.toList(), first
                )
                isVerified shouldBe false
            }

            test("GIVEN second post where ${it.fieldName} has been tampered WHEN verifying the post THEN reject") {
                testTamperedInput(false)
            }

            if (it.isValidForFirstPost) {
                test("GIVEN first post where ${it.fieldName} has been tampered WHEN verifying the post THEN reject") {
                    testTamperedInput(true)
                }
            }
        }
    }
})
