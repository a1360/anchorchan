# Anonymous Moderated Forum - Anchor Chan

## Proposed Project Design Goals:

Internet forum / imageboard focused on anonymity and effective moderation.
+ [X] Easy to use (smart client)
+ [X] Does not reveal user ip address (i2p)
+ [X] Anonymous posting (Rotating client userId)
+ [X] Non-anonymous posting (consistent thread id + tripfagging)
+ [X] Way of restricting new account creation (decentralised reputation with PoW)
+ [X] Moderate users to prevent spam and illegal content (moderation tools + decentralised reputation with PoW)
+ [ ] Decentralised moderation and hosting

## Implemented features

### Client connects over i2p

+ Hides ip address of both the user and the server.
+ Use a dedicated smart client to make it easy for the user to connect.
+ Manage configuring bandwidth and general i2p settings.
+ Gracefully manage shutdown.

### Generic Imageboard functionality list v1.0

+ Single thread
+ Text posts
+ Thread browser
+ Tag system / boards
+ Thread watcher
+ (You)'s
+ Reply functionality

### Decentralised Reputation Management v1.0

+ Client creates accounts based upon a randomly generated RSA key.
+ Client maintains a Proof of Work chain containing its public key.
+ Client must maintain a chain long and complex enough (decided by server) to post.
+ Configure amount of PoW needed for various degrees of functionality.

### Rotating client userId

+ Goal is to prevent a single user from being tracked or doxxed by what they're saying, by either a server or another user.
+ Client will attempt to generate multiple userIds for the user, creating the necessary PoW to make each id valid.
+ A users anonymity would increase over time as their client builds more accounts.
+ Over time older account ids can be dropped.

## Proposed (unimplemented) feature spec:

### Decentralised Reputation Management v1.1

+ Server must make all users available online alongside their proof of work.
    + This would be to prove the servers honesty.

### Consistent Thread Id

+ For each thread the client would randomly select an account id (as in Rotating Client userId) and keep it consistent across posting in that thread.
+ Use that accounts key to act as its personal id for the thread
+ The client must send proof with every post that they own the key pair (sign timestamp + message)
+ Other clients may then use the public key to identify the posts as coming from the same user

### Direct Messages

+ A user can send direct messages to another user by using the public key of their consistent thread id (without revealing who they wish to send a message to).
+ The smart client must actively scan for messages like this.

+ Issues:
    + [ ] Messages would be lost when the thread would expire. It would be nice to have a semi-persistent messaging 
      system.

### Tripfagging

+ Building upon the consistent thread id, allow the user to create an identity id.
+ The user may choose to select this id when replying to a thread.

+ Issues:
    + [ ] Once an account is associated with a particular trip a degree of anonymity is lost.

### Generic Smart-Client functionality list

+ [ ] Auto-updates

### Generic Imageboard functionality list v1.1

+ [ ] Image posts

### Moderation functionality list

+ [ ] Built into the app for ease of use
+ [ ] Easy adding and removing of moderators
+ [ ] Private board to discuss moderation
+ [ ] Report functionality
+ [ ] Image moderation queue ordered by reports
+ [ ] Text moderation queue ordered by reports
+ [ ] Delete posts
+ [ ] Delete thread
+ [ ] Transparently publish moderation decisions

### Admin tools

+ [ ] Create / delete boards
+ [ ] Pin threads
+ [ ] Recover expired threads
+ [ ] Manage moderator privileges

### Private invite-only boards

+ [ ] Allow restricted access to boards using a token
+ [ ] Track what tokens have been allocated and what privileges they have
+ [ ] Grant access to boards + posting based on token privilege
+ [ ] Grant access to moderation powers based on token privilege

Issues:
+ [ ] By requiring a token to read / write we now have a means of tracking users, violating their privacy. Potentially revive ring-signatures? Or acceptable risk?

### Android support

### Federation

+ Why do I want to federate?
    + Resiliance. The board can continue working even if the original server goes down.
    + Replication of moderation. Even if one moderator is compromised, he can easily be replacted with others.
    + Encourage smaller communities (back to the old days of MMO guilds), while still benefitting from a large community.
+ There must be an incentive for federation.
    + Hosting your own board?
    + Do people like moderating?
    + Include tools for community engagement (planning events, internal chat, local-only boards)?
+ Client will need to have a way of discovering other federated boards.
    + Some form of board registry?
+ What tech spec to achieve federation?
    + ActivityPub?

+ Issues:
    + [ ] Use ActivityPub protocol? At a first look the protocol doesn't look appropriate due to being very user focused. Suffers from attempting to achieve a lot.
    + [ ] Need an incentive for people to federalise
        + Potentially limit the number of new users on the main server. Host a board for users to find boards that they can become a member of.
        + Limit the main server to a small number of boards
    + [ ] How would a user discover the other servers?
    + [ ] How to manage posts not showing up on some servers? What about users replying to a user which isn't visible on another server?
    + [ ] How to manage reputable users replying to a thread made by a user banned on another server?
    + [ ] What does this look like on platforms like pleroma?
    + [ ] Incomplete
  
### Speed up I2P connection time

+ Problem: The application can take 5 to 10 minutes to connect throught the I2P network on a cold launch

+ Questions:
  + Why not use a dedicated seed server to bootstrap clients peers?

## Tech stack

+ Kotlin Mutliplatform smart client.
    + Cross-platform desktop support with Android support.
    + Desktop / Jetpack compose fun new UI library.

+ Ratpack stack with grpc on server

## Discontinued ideas

### Limit account creation with crypto mining

+ User must first mine a certain amount of crypto to earn the right to post.
+ Profits can be used to cover server costs.
+ Abandoned as PoW will work just as well and is easier to decentralise - will likely pick this up again in the future.

### Use ring-signatures to hide who wrote a particular message

+ Users would use one anothers public keys to hide which user wrote a particular post.
+ Abandoned as simpler idea was found: allow a user to create multiple accounts.
