package com.anchor.chan.desktop

object LexemeConstants {

    const val APP_TITLE = "Anchor Chan"

    object Config {
        const val CONFIG_TITLE = "Settings"

        const val ROUTER_CONFIG_TITLE = "I2P Configuration"
        const val ROUTER_EMBED_OPTION_ENABLE = "Automatic (recommended)"
        const val ROUTER_EMBED_OPTION_DISABLE = "Use your own I2P connection (advanced user)"

        const val BANDWIDTH_CONFIG_TITLE = "Bandwidth Configuration"
        const val BANDWIDTH_OPTION_AUTO = "Automatic (recommended)"
        const val BANDWITH_OPTION_MANUAL = "Manual configuration (advanced user)"
        const val BANDWIDTH_OPTION_MANUAL_IN = "Kb/s inbound"
        const val BANDWIDTH_OPTION_MANUAL_OUT = "Kb/s outbound"
        const val BANDWIDTH_ESTIMATE_INVALIDATE = "Invalidate cache"

        const val COLOR_CONFIG_TITLE = "Color Settings"
        const val COLOR_OPTION_DEFAULT = "Forest"
        const val COLOR_OPTION_DEFAULT_DARK = "Dark Coffee Shop"
        const val COLOUR_OPTION_NEON = "Neon"
    }

    object AccountSettings {
        const val ACCOUNT_SETTINGS_TITLE = "Account Settings"
        const val ACCOUNT_SETTINGS_CLOSE = "Close"

        const val ACCOUNTS_SETTINGS_SECTION_BOARDS = "Board Permissions"
        const val BOARD_NAME = "Board Name"
        const val CAN_POST = "Can Post"
        const val CAN_CREATE = "Can Create Threads"

        const val ACCOUNTS_SETTINGS_SECTION_CONFIG = "Account Config"
        const val CONFIG_TARGET_LENGTH = "Target chain length: "

        const val ACCOUNT_SETTINGS_SECTION_ACCOUNTS = "Current Active Accounts"
        const val ACCOUNT_NAME = "Account PublicKey Hash: "
        const val ACCOUNT_LENGTH = "Account Reputation Length: "
        const val ACCOUNT_THREADS = "Number of Threads used in: "
        const val ACCOUNT_DELETE = "Delete Account"
    }

    object Generic {
        const val COMPLETE_BUTTON = "Save"
    }

    object ThreadBrowser {

        object ConnectionStatus {
            const val CONNECTING = "Connecting to the I2P network. This can take 10 - 15 minutes."
            const val BUILDING_NETWORK = "Building connections within the I2P network. This can take 10 - 15 minutes."
            const val CONNECTED = "Connected"
        }

        object Posts {
            const val USER_ID = "id: "
            const val ID = "POST: "
            const val INPUT_HINT = "Enter your comment here. Close input with Esc."
            const val TITLE_HINT = "Enter title here"
            const val ID_REF = ">>"
            const val REPLY_TO_YOU = " (YOU)"
        }

        object ThreadWatcher {
            const val SELECT_BOARDS = "Select Boards"
        }

        object SelectBoard {
            const val BOARD_IS_SELECTED = "Board is selected"
        }
    }

    object SideBar {
        const val SETTINGS = "Settings"
        const val ACCOUNT_SETTINGS = "Account Settings"
    }
}
