package com.anchor.chan.common.connection.usecase

import com.anchor.chan.common.configuration.ConfigurationState
import com.anchor.chan.common.configuration.ConfigurationStateStore
import com.vuze.plugins.mlab.MLabRunner
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.withContext
import net.i2p.I2PAppContext
import java.util.Calendar
import java.util.concurrent.TimeUnit
import kotlin.coroutines.coroutineContext

class EstimateBandwidthUseCase(
    private val configurationStateStore: ConfigurationStateStore,
    private val mLabRunnerProvider: () -> MLabRunner = { MLabRunner.getInstance(I2PAppContext.getGlobalContext()) },
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) {

    suspend fun estimateBandwidth() {
        withContext(ioDispatcher) {
            val bandwidthSettings = configurationStateStore.state.value
            if (bandwidthSettings.needsToCalculateEstimate()) {
                performEstimate()
            }
        }
    }

    private suspend fun performEstimate() {
        val runner = mLabRunnerProvider()
        if (!runner.isRunning) {
            runner.runNDT(EstimateResultListener(configurationStateStore))
        }
        while (runner.isRunning && coroutineContext.isActive) {
            delay(1000)
        }
    }

    private class EstimateResultListener(
        private val configurationStateStore: ConfigurationStateStore
    ) : MLabRunner.ToolListener {

        override fun reportSummary(str: String?) = Unit

        override fun reportDetail(str: String?) = Unit

        override fun complete(results: MutableMap<String, Any>?) {
            if (results != null && results.isValid()) {
                val orateKBps = results.getUpBandwidth()
                    .toKbs(MIN_UP_BW)
                val irateKBps = results.getDownBandwidth()
                    .toKbs(MIN_DOWN_BW)
                val now = Calendar.getInstance().timeInMillis
                configurationStateStore.setBandwidthValues(irateKBps.toInt(), orateKBps.toInt(), now)
            } else {
                configurationStateStore.markBandwidthEstimateAsFailed()
            }
        }

        private fun Long.toKbs(min: Float): Float =
            Math.max(
                min,
                BW_SCALE * this / 1024f
            )

        private fun MutableMap<String, Any>.isValid(): Boolean {
            val isUpValid = getUpBandwidth() > 0
            val isDownValid = getDownBandwidth() > 0
            return isUpValid && isDownValid
        }

        private fun MutableMap<String, Any>.getUpBandwidth(): Long =
            get("up") as Long? ?: 0L

        private fun MutableMap<String, Any>.getDownBandwidth(): Long =
            get("down") as Long? ?: 0L
    }

    companion object {
        private val MILLIS_UNTIL_RE_ESTIMATE = TimeUnit.DAYS.toMillis(180)
        private val MILLIS_DELAY_BETWEEN_RETRY = TimeUnit.DAYS.toMillis(1)

        // scale bw test results by this for limiter settings
        private const val BW_SCALE = 0.75f

        // KBps
        private const val MIN_DOWN_BW = 32.0f
        private const val MIN_UP_BW = 12.0f

        private fun ConfigurationState.needsToCalculateEstimate(): Boolean {
            val isRouterEmbedded = isRouterEmbedded == true
            val now = Calendar.getInstance().timeInMillis
            val isBandwidthEstimateEnabled = bandwidthSettings.isAutoEstimateBandwidth
            val needsReEstimate = now - bandwidthSettings.timeEstimated >= MILLIS_UNTIL_RE_ESTIMATE
            val canAttemptEstimateAgain =
                now - bandwidthSettings.previousEstimateAttempt >= MILLIS_DELAY_BETWEEN_RETRY
            return isRouterEmbedded && isBandwidthEstimateEnabled && needsReEstimate && canAttemptEstimateAgain
        }
    }
}
