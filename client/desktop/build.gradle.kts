import org.jetbrains.compose.compose
import org.jetbrains.compose.desktop.application.dsl.TargetFormat

plugins {
    kotlin("multiplatform")
    id("org.jetbrains.compose") version DepVersions.DESKTOP_COMPOSE
}

group = "com.anchor.chan"
version = "1.0"

kotlin {
    jvm {
        compilations.all {
            kotlinOptions.jvmTarget = "15"
        }
    }
    sourceSets {
        named("jvmMain") {
            dependencies {
                implementation(project(":client:common-domain"))
                implementation(project(":client:component:account-management"))
                implementation(project(":common:library:publickey-crypto"))
                implementation(project(":common:library:hashcash"))
                implementation(project(":client:component:architecture"))
                implementation(project(":client:component:client-models"))
                implementation(project(":client:component:logger"))

                implementation("br.com.devsrsouza.compose.icons.jetbrains:feather:1.0.0")

                implementation(Deps.slf4j)
                implementation(Deps.arrowCore)
                implementation(compose.desktop.currentOs)
                implementation(compose.runtime)
            }
        }
    }
}

compose.desktop {
    application {
        mainClass = "com.anchor.chan.desktop.MainKt"
        nativeDistributions {
            targetFormats(TargetFormat.Dmg, TargetFormat.Msi, TargetFormat.Deb)
            modules("java.naming")
            packageName = "AnchorChan"
            packageVersion = "1.0.0"
            appResourcesRootDir.set(project.layout.projectDirectory.dir("resources"))
        }
    }
}
