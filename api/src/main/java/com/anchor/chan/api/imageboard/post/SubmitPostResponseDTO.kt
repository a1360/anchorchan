package com.anchor.chan.api.imageboard.post

data class SubmitPostResponseDTO(
    val post: PostDTO,
)
