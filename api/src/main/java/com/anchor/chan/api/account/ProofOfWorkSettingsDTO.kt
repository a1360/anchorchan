package com.anchor.chan.api.account

import com.fasterxml.jackson.annotation.JsonProperty

data class ProofOfWorkSettingsDTO(
    @JsonProperty("leadingZeros") var leadingZeros: Int?,
)
