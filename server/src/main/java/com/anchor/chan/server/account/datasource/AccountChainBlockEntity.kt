package com.anchor.chan.server.account.datasource

import org.jdbi.v3.core.mapper.RowMapper
import org.jdbi.v3.core.statement.StatementContext
import java.sql.ResultSet
import java.time.Instant

data class AccountChainBlockEntity(
    val blockId: String,
    val accountId: String,
    val token: String,
    val timestamp: Instant,
)

internal class AccountChainBlockEntityMapper : RowMapper<AccountChainBlockEntity> {

    override fun map(rs: ResultSet, ctx: StatementContext): AccountChainBlockEntity =
        AccountChainBlockEntity(
            blockId = rs.getString("blockId"),
            accountId = rs.getString("accountId"),
            token = rs.getString("token"),
            timestamp = rs.getTimestamp("timestamp").toInstant(),
        )
}
