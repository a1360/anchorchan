package com.anchor.chan.hashcash

import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import java.text.SimpleDateFormat
import java.util.Calendar

private object Tokens {
    val validFirst = "1:10:211031:key::ffffffffffffd671:fffffffff86363c1"
    val validSecond = "1:10:211031:key:prev_hash=003522048674705aa94e4063a6e84:ffffffffffffff98:9c"
    val gibberish = "blah blah blah"
    val oldDate = "1:10:211029:key::ffffffffffffffcb:ffffffffffff9939"
    val futureDate = "1:10:211101:key::ffffffffffffde6d:ffffffff986d4727"
    val invalidSecondWithInvalidPrevToken =
        "1:10:211031:key:prev_hash=13447686a045293efc0b016082c068:ffffffffffff9044:21e"
    val incorrectKey = "1:10:211031:invalid::ffffffff8178104a:283"
    val incorrectLeadingZeros = "1:4:211031:key::ffffffffa1510206:ffffffffffffffcc"
    val invalidHash = "1:10:211031:key::ffffffffffffd671:fffffffff86363c0"
}

private val testTimeProvider: () -> Calendar = {
    Calendar.getInstance().apply {
        time = SimpleDateFormat("yyMMdd").parse("211031")
    }
}
private val leadingZeros = 10
private val verifier = NettygryppaChainVerifier(
    config = NettygryppaChainVerifierConfig(
        coroutineContext = Dispatchers.IO
    ),
    calendarProvider = testTimeProvider
)
private val builder = NettygryppaChainBuilder(
    config = NettygryppaChainBuilderConfig(
        coroutineContext = Dispatchers.IO,
        parallelCount = 2,
    )
)
private fun verify(token: String, prevToken: String? = null): Boolean =
    runBlocking {
        verifier.verifyToken(leadingZeros, token, "key", prevToken)
    }

class NettygryppaChainVerifierTest : FunSpec({

    test("WHEN token is valid THEN accept") {
        val result = verify(Tokens.validFirst)

        result shouldBe true
    }

    test("WHEN a second token is valid THEN accept") {
        builder.encode(leadingZeros, "key", null).also { println(it) }
        val result = verify(Tokens.validSecond, Tokens.validFirst)

        result shouldBe true
    }

    test("WHEN token has an incorrect previous token THEN reject") {
        val result = verify(Tokens.invalidSecondWithInvalidPrevToken, Tokens.validFirst)

        result shouldBe false
    }

    listOf(
        "gibberish data" to Tokens.gibberish,
        "an old date" to Tokens.oldDate,
        "a future date" to Tokens.futureDate,
        "an incorrect key" to Tokens.incorrectKey,
        "an incorrect number of leading zeros" to Tokens.incorrectLeadingZeros,
        "an invalid hash" to Tokens.invalidHash,
    ).forEach {
        test("WHEN token has ${it.first} THEN reject") {
            val result = verify(it.second)

            result shouldBe false
        }
    }
})
