package com.anchor.chan.network.client

import com.anchor.chan.api.account.SignatureBlockDTO
import com.anchor.chan.api.imageboard.post.PostDTO
import com.anchor.chan.api.imageboard.post.PostUpdatesRequestDTO
import com.anchor.chan.api.imageboard.post.SubmitPostRequestDTO
import com.anchor.chan.model.account.AccountDataModel
import com.anchor.chan.model.imageboard.PostModel
import com.anchor.chan.model.toModel
import com.anchor.publickey.crypto.AssymetricKeyPair
import com.anchor.publickey.crypto.AssymetricKeySigner
import com.anchor.publickey.crypto.PublicKeyHash
import com.anchor.publickey.crypto.toGetPostsSignature
import com.anchor.publickey.crypto.toSubmitPostSignature
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.withContext
import org.slf4j.LoggerFactory
import retrofit2.Retrofit
import java.time.Instant

interface PostNetworkClient {

    suspend fun requestUpdate(
        key: AssymetricKeyPair,
        threadIds: List<String>,
        fromTimestamp: Instant
    ): PostUpdateResponse

    suspend fun submitPost(
        key: AssymetricKeyPair,
        threadId: String,
        title: String,
        message: String
    ): PostModel
}

data class PostUpdateResponse(
    val posts: List<PostModel>,
    val accounts: Map<PublicKeyHash, AccountDataModel>,
    val latestTimestamp: Instant,
)

private val logger = LoggerFactory.getLogger(PostNetworkClientImpl::class.java)

internal class PostNetworkClientImpl(
    retrofitFlow: Flow<Retrofit>,
    private val messageSigner: AssymetricKeySigner,
    private val dateProvider: () -> Instant,
) : PostNetworkClient {

    private val serviceBuilder = NetworkServiceBuilder(retrofitFlow) { it.create(PostNetworkService::class.java) }

    override suspend fun requestUpdate(
        key: AssymetricKeyPair,
        threadIds: List<String>,
        fromTimestamp: Instant,
    ): PostUpdateResponse =
        withContext(Dispatchers.IO) {
            serviceBuilder
                .getOrCreateStub()
                .first()
                .getPosts(
                    PostUpdatesRequestDTO(
                        fromTimestamp = fromTimestamp,
                        threadIds = threadIds,
                        signatureBlock = SignatureBlockDTO(
                            publicKey = key.publicKey.key,
                            timestamp = dateProvider(),
                        )
                    ).applySignature(key)
                )
                .let {
                    val accounts = it.body()?.accounts?.map { it.toModel() }?.associateBy { it.publicKeyHash }
                    val posts = it.body()?.posts
                    val timestamp = it.body()?.lastTimestamp
                    if (accounts != null && posts != null && timestamp != null) {
                        PostUpdateResponse(
                            posts = posts.map { it.toModel() },
                            latestTimestamp = timestamp,
                            accounts = accounts,
                        )
                    } else {
                        throw NetworkException(it.errorBody()?.string())
                    }
                }
        }

    override suspend fun submitPost(
        key: AssymetricKeyPair,
        threadId: String,
        title: String,
        message: String
    ): PostModel =
        withContext(Dispatchers.IO) {
            logger.trace("Sending message: $message")
            serviceBuilder
                .getOrCreateStub()
                .first()
                .submitPost(
                    SubmitPostRequestDTO(
                        post = PostDTO(
                            postId = null,
                            message = message,
                            timestamp = null,
                            threadId = threadId,
                            title = title,
                        ),
                        signatureBlock = SignatureBlockDTO(
                            publicKey = key.publicKey.key,
                            timestamp = dateProvider(),
                        )
                    ).applySignature(key)
                )
                .let { it.body()?.post ?: throw NetworkException(it.errorBody()?.string()) }
                .toModel()
        }

    private fun PostUpdatesRequestDTO.applySignature(key: AssymetricKeyPair): PostUpdatesRequestDTO =
        copy(
            signatureBlock = signatureBlock?.copy(
                signature = messageSigner.sign(
                    key.privateKey,
                    signatureBlock?.toGetPostsSignature(threadIds ?: emptyList()) ?: ""
                )
            )
        )

    private fun SubmitPostRequestDTO.applySignature(key: AssymetricKeyPair): SubmitPostRequestDTO =
        copy(
            signatureBlock = signatureBlock?.copy(
                signature = messageSigner.sign(
                    key.privateKey,
                    signatureBlock?.toSubmitPostSignature(
                        post?.threadId ?: "",
                        post?.title,
                        post?.message ?: ""
                    ) ?: ""
                )
            )
        )
}
