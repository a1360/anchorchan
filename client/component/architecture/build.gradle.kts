import org.jetbrains.compose.compose

plugins {
    kotlin("multiplatform")
    id("org.jetbrains.compose") version DepVersions.DESKTOP_COMPOSE
}

kotlin {
    jvm {
        compilations.all {
            kotlinOptions.jvmTarget = "11"
        }
    }
    sourceSets {
        val jvmMain by getting {
            dependencies {
                implementation(compose.foundation)

                implementation(project(":client:component:logger"))

                implementation(Deps.kotlinxSerialization)
            }
        }
        val jvmTest by getting
    }
}
