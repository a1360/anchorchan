package com.anchor.chan.hashcash

import com.anchor.publickey.crypto.toSha1Hex
import com.nettgryppa.security.HashCash
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class NettygryppaChainBuilder(
    private val config: NettygryppaChainBuilderConfig
) : ChainBuilder {

    override suspend fun encode(value: Int, key: String, previousToken: String?): String {
        val scope = CoroutineScope(config.coroutineContext)
        val resultChannel = Channel<HashCash>()
        val extensionMap =
            previousToken
                ?.toSha1Hex()
                ?.let { mapOf(PREVIOUS_HASH_KEY to listOf(it.keyHash)) } ?: emptyMap()

        repeat(config.parallelCount) {
            scope.launch { launchEncodeJob(resultChannel, value, key, extensionMap) }
        }

        val result = resultChannel.receive()
        scope.cancel()
        return result.toEncodingResult()
    }

    private suspend fun launchEncodeJob(
        resultChannel: Channel<HashCash>,
        value: Int,
        key: String,
        extensionMap: Map<String, List<String>>,
    ) {
        HashCash.mintCash(key, extensionMap, value)
            .let { resultChannel.send(it) }
    }

    private fun HashCash.toEncodingResult() =
        myToken!!

    companion object {
        const val PREVIOUS_HASH_KEY = "prev_hash"
    }
}

data class NettygryppaChainBuilderConfig(
    val coroutineContext: CoroutineContext,
    val parallelCount: Int,
)
