package com.anchor.chan.model.imageboard

data class BoardModel(
    val boardId: String,
    val name: String,
    val shortName: String,
    val permissions: BoardPermissions,
)

data class BoardPermissions(
    val readLength: Int,
    val postLength: Int,
    val createLength: Int,
)
