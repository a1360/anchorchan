package com.anchor.publickey.crypto

interface AssymetricKeySigner {

    fun sign(key: AssymetricPrivateKey, message: String): String
}
