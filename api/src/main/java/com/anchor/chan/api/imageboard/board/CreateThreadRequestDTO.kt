package com.anchor.chan.api.imageboard.board

import com.anchor.chan.api.account.SignatureBlockDTO
import com.anchor.chan.api.imageboard.post.PostDTO

data class CreateThreadRequestDTO(
    val boardIds: List<String>?,
    val post: PostDTO?,
    val signatureBlockDTO: SignatureBlockDTO?,
)
