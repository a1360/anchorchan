package com.anchor.publickey.crypto

import java.security.MessageDigest
import kotlin.experimental.and

@JvmInline
value class PublicKeyHash(val keyHash: String)

fun String.toSha1Hex(): PublicKeyHash {
    val md = MessageDigest.getInstance("SHA1")
    md.update(this.toByteArray())
    return md.digest().toHexString()
}

fun AssymetricPublicKey.toSha1Hex(): PublicKeyHash =
    key.toSha1Hex()

private fun ByteArray.toHexString(): PublicKeyHash {
    val result = StringBuilder()
    for (i in indices) {
        val next = ((this[i] and (0xff).toByte()) + 0x100).toString(16).substring(1)
        result.append(next)
    }
    return PublicKeyHash(result.toString())
}
