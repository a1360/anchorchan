package com.anchor.chan.common.threadbrowser

import com.anchor.chan.common.threadbrowser.content.board.SelectAccountUseCase
import com.anchor.chan.common.threadbrowser.watcher.ThreadWatcherStateStore
import com.anchor.chan.common.threadbrowser.watcher.ThreadWatcherUpdateStreamerUseCase
import com.anchor.chan.network.client.PostNetworkClient

class ThreadBrowserBootstrapper(
    private val store: ThreadWatcherStateStore,
    private val selectAccountUseCase: SelectAccountUseCase,
    private val client: PostNetworkClient,
) {

    private val threadStreamer = ThreadWatcherUpdateStreamerUseCase(
        store = store,
        client = client,
        selectAccountUseCase = selectAccountUseCase,
    )

    init {
        threadStreamer.setupStreamThreadUpdates()
    }
}
