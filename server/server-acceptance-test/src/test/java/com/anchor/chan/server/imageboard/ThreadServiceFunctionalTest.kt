package com.anchor.chan.server.imageboard

import com.anchor.chan.model.imageboard.ThreadModel
import com.anchor.publickey.crypto.ecc.EccGenerator
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe

class ThreadServiceFunctionalTest : FunSpec({

    val environment = ImageboardEnvironment()
    val accountKey = EccGenerator().generateKey()
        .also { environment.accountUseCase.setAccountChainLength(it.publicKey, Int.MAX_VALUE) }

    beforeSpec {
        environment.start()
        environment.insertDefaultData()
    }

    afterSpec {
        environment.close()
    }

    suspend fun requestAllThreads(): List<ThreadModel> =
        environment.boardService.requestThreads(accountKey, setOf("0", "1", "2"))

    suspend fun submitPostTo(threadId: String) {
        environment.postService.submitPost(accountKey, threadId, "Test Title", "Test Message")
    }

    test("WHEN requesting threads THEN they are ordered by last post") {
        val boards = requestAllThreads().map { it.threadId }

        boards shouldBe listOf("0", "1")
    }

    test("WHEN making a post THEN the the thread order is updated") {
        submitPostTo("1")

        val boards = requestAllThreads().map { it.threadId }

        boards shouldBe listOf("1", "0")
    }
})
