package com.anchor.publickey.crypto

import com.anchor.chan.api.account.SignatureBlockDTO

fun SignatureBlockDTO.toSignatureString(): String =
    "$publicKey:$timestamp"

fun SignatureBlockDTO.toCreateThreadSignature(boardIds: List<String>, message: String, title: String): String =
    "${boardIds.joinToString(",")}:$message:$title:${toSignatureString()}"

fun SignatureBlockDTO.toGetThreadsSignature(boardIds: List<String>): String =
    "${boardIds.joinToString(",")}:${toSignatureString()}"

fun SignatureBlockDTO.toSubmitPostSignature(threadId: String, title: String?, message: String): String =
    "$threadId:$message:$title:${toSignatureString()}"

fun SignatureBlockDTO.toGetPostsSignature(threadIds: List<String>): String =
    "${threadIds.joinToString(",")}:${toSignatureString()}"
