package com.anchor.chan.desktop.screen.threadbrowser.content.boards

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.text.style.TextAlign
import com.anchor.chan.desktop.LexemeConstants
import com.anchor.chan.desktop.screen.UiConstants
import com.anchor.chan.desktop.screen.viewmodel.ThreadBrowserViewModel
import com.anchor.chan.desktop.ui.CircleLoadingIndicator
import com.anchor.chan.desktop.ui.Fab
import com.anchor.chan.model.imageboard.BoardModel
import compose.icons.FeatherIcons
import compose.icons.feathericons.CheckSquare
import compose.icons.feathericons.CornerDownRight

object SelectBoardRenderer {

    @Composable
    fun render(model: ThreadBrowserViewModel.ContentViewModel.SelectBoardViewModel) {
        if (model.boards.isNotEmpty()) {
            renderBoardList(model)
        } else {
            CircleLoadingIndicator()
        }
    }

    @Composable
    private fun renderBoardList(model: ThreadBrowserViewModel.ContentViewModel.SelectBoardViewModel) {
        Box(
            modifier = Modifier.fillMaxHeight()
        ) {
            val selectedItems = remember { mutableStateOf(emptyList<BoardModel>()) }
            boardList(model, selectedItems)
            submitFab(model, selectedItems)
        }
    }

    @Composable
    private fun boardList(
        model: ThreadBrowserViewModel.ContentViewModel.SelectBoardViewModel,
        selectedItems: MutableState<List<BoardModel>>
    ) {
        val spacerHeight = UiConstants.POST_SPACING
        val halfSpacerHeight = spacerHeight / 2
        LazyColumn {
            items(model.boards.size) { index ->
                val item = model.boards[index]
                val isSelected = selectedItems.value.contains(item.boardModel)
                Spacer(Modifier.height(if (index == 0) spacerHeight else halfSpacerHeight))
                board(
                    isSelected = isSelected,
                    name = item.name,
                    onClick = {
                        if (isSelected) {
                            selectedItems.remove(item.boardModel)
                        } else {
                            selectedItems.add(item.boardModel)
                        }
                    }
                )
                Spacer(Modifier.height(if (index == model.boards.lastIndex) spacerHeight else halfSpacerHeight))
            }
        }
    }

    @Composable
    private fun submitFab(
        model: ThreadBrowserViewModel.ContentViewModel.SelectBoardViewModel,
        selectedItems: MutableState<List<BoardModel>>
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(end = UiConstants.FAB_PADDING, bottom = UiConstants.FAB_PADDING),
            verticalArrangement = Arrangement.Bottom,
            horizontalAlignment = Alignment.End
        ) {
            Fab(
                isEnabled = selectedItems.value.isNotEmpty(),
                painter = rememberVectorPainter(FeatherIcons.CornerDownRight),
                onClick = {
                    model.onBoardsSelected(selectedItems.value)
                }
            )
        }
    }

    private fun MutableState<List<BoardModel>>.add(model: BoardModel) {
        value = value.plus(model)
    }

    private fun MutableState<List<BoardModel>>.remove(model: BoardModel) {
        value = value.minus(model)
    }

    @Composable
    private fun board(isSelected: Boolean, name: String, onClick: () -> Unit) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .clickable(onClick = onClick)
                .padding(end = UiConstants.POST_SPACING)
                .background(MaterialTheme.colors.surface)
                .padding(UiConstants.POST_PADDING),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                modifier = Modifier
                    .defaultMinSize(minHeight = UiConstants.BUTTON_SIZE_NO_PADDING),
                text = name,
                textAlign = TextAlign.Start,
                style = MaterialTheme.typography.h6,
                color = MaterialTheme.colors.onSurface
            )
            if (isSelected) {
                Icon(
                    imageVector = FeatherIcons.CheckSquare,
                    contentDescription = LexemeConstants.ThreadBrowser.SelectBoard.BOARD_IS_SELECTED,
                    tint = MaterialTheme.colors.onSurface,
                    modifier = Modifier
                        .size(UiConstants.BUTTON_SIZE_NO_PADDING),
                )
            }
        }
    }
}
