package com.anchor.chan.server.config.error

import org.slf4j.LoggerFactory
import ratpack.error.ServerErrorHandler
import ratpack.handling.Context
import ratpack.http.Status
import javax.naming.AuthenticationException

private val logger = LoggerFactory.getLogger(ServerExceptionHandler::class.java)

class ServerExceptionHandler : ServerErrorHandler {

    override fun error(context: Context?, throwable: Throwable?) {
        when (throwable) {
            is IllegalArgumentException -> context?.response?.status(Status.BAD_REQUEST)?.send(throwable.message)
            is AuthenticationException -> context?.response?.status(Status.UNAUTHORIZED)?.send(throwable.message)
            else -> context?.response?.status(Status.INTERNAL_SERVER_ERROR)
        }.also { logger.error("Handled Exception", throwable) }
    }
}
