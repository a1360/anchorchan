package com.anchor.chan.common.connection

import com.anchor.chan.common.configuration.ConfigurationStateStore
import com.anchor.chan.common.connection.usecase.ConfigureNetworkProviderUseCase
import com.anchor.chan.network.client.NetworkClientProvider
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class I2PConnectionBootstrapper(
    private val connectionStateStore: I2pConnectionStateStore,
    private val networkClientProvider: NetworkClientProvider,
    private val configurationStateStore: ConfigurationStateStore,
    private val bootstrapScope: CoroutineScope = CoroutineScope(Dispatchers.IO),
) {

    private val configureUseCase = ConfigureNetworkProviderUseCase(
        networkClientProvider = networkClientProvider,
        connectionStateStore = connectionStateStore,
        configurationStateStore = configurationStateStore,
    )

    init {
        configureNetwork()
    }

    private fun configureNetwork() {
        bootstrapScope.launch {
            configureUseCase.setup()
        }
    }
}
