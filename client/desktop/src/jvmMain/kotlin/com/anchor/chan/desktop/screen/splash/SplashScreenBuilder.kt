package com.anchor.chan.desktop.screen.splash

import com.anchor.chan.architecture.ui.ScreenBuilder
import com.anchor.chan.common.configuration.ConfigurationStateStore
import com.anchor.chan.desktop.screen.navigation.NavigateToSettings
import com.anchor.chan.desktop.screen.navigation.NavigateToThreadBrowser
import com.anchor.chan.desktop.screen.viewmodel.SplashViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.onStart

class SplashScreenBuilder(
    private val configurationStore: ConfigurationStateStore,
    private val navigateToThreadBrowser: NavigateToThreadBrowser,
    private val navigateToSettings: NavigateToSettings
) : ScreenBuilder<SplashViewModel> {

    override suspend fun build(): Flow<SplashViewModel> =
        flow {
            emit(SplashViewModel)
        }.onStart { launchLandingScreen() }

    private suspend fun launchLandingScreen() {
        val config = configurationStore.state.filter { !it.isLoading }.first()
        delay(2_000)
        if (config.isInitialSetupComplete) {
            navigateToThreadBrowser.navigateToThreadBrowser()
        } else {
            navigateToSettings.navigateToSettings()
        }
    }
}
