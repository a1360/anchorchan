sh gradlew :server:shadowJar
mv server/build/libs/server-all.jar deployment/ansible/files/server/server.jar
ansible-playbook -i deployment/ansible/hosts deployment/ansible/deploy_server.yaml --ask-become-pass
