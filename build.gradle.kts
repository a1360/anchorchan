buildscript {
    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
        maven("https://plugins.gradle.org/m2/")
    }
    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:${DepVersions.KOTLIN}")
        classpath("com.android.tools.build:gradle:4.2.0")
        classpath("io.ratpack:ratpack-gradle:1.9.0")
        classpath("org.jlleitschuh.gradle:ktlint-gradle:10.1.0")
        classpath("gradle.plugin.com.github.johnrengelman:shadow:7.1.2")
    }
}

repositories {
    mavenCentral()
}

plugins {
    id("org.jlleitschuh.gradle.ktlint") version "10.1.0"
}

group = "com.anchor.chan"
version = "1.0"

subprojects {
    apply(plugin = "org.jlleitschuh.gradle.ktlint")
    repositories {
        mavenCentral()
        maven { url = uri("https://maven.pkg.jetbrains.space/public/p/compose/dev") }
        gradlePluginPortal()
        google()
    }
    configure<org.jlleitschuh.gradle.ktlint.KtlintExtension> {
        debug.set(true)

        filter {
            exclude { element -> element.file.path.contains("generated/") }
        }
    }
    tasks.withType<Test> {
        useJUnitPlatform()
    }
}
