package com.anchor.publickey.crypto

import java.util.Base64

internal fun ByteArray.toBase64String() =
    Base64.getEncoder().encodeToString(this)

internal fun String.toBase64Bytes() =
    Base64.getDecoder().decode(this)
