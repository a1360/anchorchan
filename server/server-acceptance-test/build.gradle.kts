plugins {
    kotlin("jvm")
    id("io.ratpack.ratpack-java")
    id("idea")
}

kotlin {
    jvmToolchain {
        (this as JavaToolchainSpec).languageVersion.set(JavaLanguageVersion.of("11"))
    }
}

dependencies {
    implementation(ratpack.dependency("guice"))

    implementation(project(":server"))
    implementation(project(":api"))
    implementation(project(":common:library:hashcash"))
    implementation(project(":common:library:publickey-crypto"))
    implementation(project(":client:component:network-client"))
    implementation(project(":client:component:client-models"))

    implementation(Deps.jdbi)
    implementation(Deps.kotlinCoroutines)

    testImplementation(Deps.kotest)
    testImplementation(Deps.kotestAssertions)
    testImplementation(Deps.testContainers)
    testImplementation(Deps.testContainerPostgresql)
    testImplementation(Deps.testContainerKotest)
    testImplementation(ratpack.dependency("test"))
}
