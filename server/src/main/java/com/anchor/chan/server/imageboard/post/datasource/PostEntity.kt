package com.anchor.chan.server.imageboard.post.datasource

import com.anchor.publickey.crypto.PublicKeyHash
import org.jdbi.v3.core.mapper.RowMapper
import org.jdbi.v3.core.statement.StatementContext
import java.sql.ResultSet
import java.time.Instant

data class PostEntity(
    val postId: String,
    val threadId: String,
    val message: String,
    val title: String?,
    val publicKeyHash: PublicKeyHash,
    val signature: String,
    val timestamp: Instant,
) {

    companion object {

        val generatedFields: Array<String> = arrayOf("postId", "timestamp")
    }
}

internal class PostEntityMapper : RowMapper<PostEntity> {

    override fun map(rs: ResultSet, ctx: StatementContext): PostEntity =
        map(rs)

    companion object {

        internal fun map(rs: ResultSet): PostEntity =
            PostEntity(
                postId = rs.getString("postId"),
                threadId = rs.getString("threadId"),
                message = rs.getString("message"),
                timestamp = rs.getTimestamp("timestamp").toInstant(),
                title = rs.getString("title"),
                publicKeyHash = PublicKeyHash(rs.getString("publicKeyHash")),
                signature = rs.getString("signature"),
            )
    }
}

data class PostEntityGeneratedField(
    val postId: String,
    val timestamp: Instant,
)

internal class PostEntityGeneratedFieldMapper : RowMapper<PostEntityGeneratedField> {

    override fun map(rs: ResultSet, ctx: StatementContext): PostEntityGeneratedField =
        PostEntityGeneratedField(
            postId = rs.getString("postId"),
            timestamp = rs.getTimestamp("timestamp").toInstant()
        )
}
