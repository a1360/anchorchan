package com.anchor.chan.desktop.screen.settings

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.RadioButton
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.anchor.chan.architecture.ui.Renderer
import com.anchor.chan.common.configuration.ColorConfiguration
import com.anchor.chan.desktop.LexemeConstants
import com.anchor.chan.desktop.screen.UiConstants
import com.anchor.chan.desktop.screen.viewmodel.SettingsViewModel

object SettingsRenderer : Renderer<SettingsViewModel> {

    @Composable
    override fun render(model: SettingsViewModel) {
        Scaffold(
            content = { createOptions(model) },
            floatingActionButton = { confirmButton(model.isFinishButtonEnabled, model.onFinishClickedCallback) }
        )
    }

    @Composable
    private fun createOptions(model: SettingsViewModel) {
        Column(
            modifier = Modifier
                .background(color = MaterialTheme.colors.background)
                .fillMaxSize()
                .padding(PaddingValues(start = UiConstants.CONTENT_PADDING, end = UiConstants.CONTENT_PADDING))
                .verticalScroll(rememberScrollState(0)),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Spacer(Modifier.height(UiConstants.CONTENT_PADDING))
            Text(
                text = LexemeConstants.Config.CONFIG_TITLE,
                style = MaterialTheme.typography.h4,
                fontWeight = FontWeight.Bold,
                color = MaterialTheme.colors.onBackground,
            )
            Spacer(Modifier.height(16.dp))
            routerConfig(model.routerSettingsViewModel)
            AnimatedVisibility(model.routerSettingsViewModel.isRouterEmbedded == true) {
                Column {
                    Spacer(Modifier.height(16.dp))
                    bandwidthSettings(model.bandwidthSettingsViewModel)
                }
            }
            Spacer(Modifier.height(16.dp))
            colorSettings(model.colorSettingsViewModel)
        }
    }

    @Composable
    private fun routerConfig(model: SettingsViewModel.RouterSettingsViewModel) {
        createSectionTitleRow(LexemeConstants.Config.ROUTER_CONFIG_TITLE)
        createRadioButtonRow(model.isRouterEmbedded == true, LexemeConstants.Config.ROUTER_EMBED_OPTION_ENABLE) {
            model.onRouterEmbeddedCallback(true)
        }
        verticalSpacer()
        createRadioButtonRow(model.isRouterEmbedded == false, LexemeConstants.Config.ROUTER_EMBED_OPTION_DISABLE) {
            model.onRouterEmbeddedCallback(false)
        }
    }

    @Composable
    private fun bandwidthSettings(model: SettingsViewModel.BandwidthSettingsViewModel) {
        createSectionTitleRow(LexemeConstants.Config.BANDWIDTH_CONFIG_TITLE)
        createRadioButtonRow(model.isAutoEstimateBandwidth, LexemeConstants.Config.BANDWIDTH_OPTION_AUTO) {
            model.onAutoEstimateChanged(true)
        }
        AnimatedVisibility(model.isAutoEstimateBandwidth) {
            verticalSpacer()
            Row(modifier = Modifier.fillMaxWidth(ROW_FILL)) {
                subItemHorizontalSpacer()
                TextButton(
                    onClick = model.onInvalidateEstimate,
                ) {
                    Text(
                        LexemeConstants.Config.BANDWIDTH_ESTIMATE_INVALIDATE,
                    )
                }
            }
        }
        verticalSpacer()
        createRadioButtonRow(!model.isAutoEstimateBandwidth, LexemeConstants.Config.BANDWITH_OPTION_MANUAL) {
            model.onAutoEstimateChanged(false)
        }
        verticalSpacer()
        AnimatedVisibility(
            visible = !model.isAutoEstimateBandwidth
        ) {
            verticalSpacer()
            Row(modifier = Modifier.fillMaxWidth(ROW_FILL)) {
                subItemHorizontalSpacer()
                intTextField(
                    LexemeConstants.Config.BANDWIDTH_OPTION_MANUAL_IN,
                    model.inBoundKBytesPerSecond.toString(),
                    model.onInBytesChanged
                )
                horizontalSpacer()
                intTextField(
                    LexemeConstants.Config.BANDWIDTH_OPTION_MANUAL_OUT,
                    model.outBoundKBytesPerSecond.toString(),
                    model.onOutBytesChanged
                )
            }
        }
    }

    @Composable
    private fun intTextField(label: String, value: String, callback: (Int) -> Unit) {
        OutlinedTextField(
            label = { Text(label) },
            value = value,
            modifier = Modifier.width(120.dp),
            onValueChange = {
                if (it.isBlank()) {
                    callback(0)
                } else {
                    it.toIntOrNull()?.let { callback(it) }
                }
            }
        )
    }

    @Composable
    private fun colorSettings(model: SettingsViewModel.ColorSettingsViewModel) {
        createSectionTitleRow(LexemeConstants.Config.COLOR_CONFIG_TITLE)
        ColorConfiguration.values().forEachIndexed { index, configuration ->
            if (index > 0) {
                verticalSpacer()
            }
            createRadioButtonRow(
                isSelected = model.selected == configuration,
                title = configuration.toLexeme()
            ) {
                model.colorSelectedCallback(configuration)
            }
        }
    }

    private fun ColorConfiguration.toLexeme(): String =
        when (this) {
            ColorConfiguration.FOREST -> LexemeConstants.Config.COLOR_OPTION_DEFAULT
            ColorConfiguration.DARK_COFFEE_SHOP -> LexemeConstants.Config.COLOR_OPTION_DEFAULT_DARK
            ColorConfiguration.NEON -> LexemeConstants.Config.COLOUR_OPTION_NEON
        }

    @Composable
    private fun createSectionTitleRow(title: String) {
        Row(
            modifier = Modifier.fillMaxWidth(ROW_FILL),
        ) {
            Text(
                title,
                style = MaterialTheme.typography.h5,
                fontWeight = FontWeight.Bold,
                textAlign = TextAlign.Center,
                color = MaterialTheme.colors.onBackground,
            )
        }
        Spacer(Modifier.height(16.dp))
    }

    @Composable
    private fun createRadioButtonRow(isSelected: Boolean, title: String, selectedCallback: () -> Unit) {
        Row(
            modifier = Modifier.fillMaxWidth(ROW_FILL),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Start,
        ) {
            RadioButton(
                selected = isSelected,
                onClick = selectedCallback
            )
            horizontalSpacer()
            Text(
                text = AnnotatedString(title),
                modifier = Modifier
                    .alpha(if (isSelected) 1f else 0.5f)
                    .clickable { selectedCallback.invoke() },
                color = MaterialTheme.colors.onBackground
            )
        }
    }

    @Composable
    private fun confirmButton(isEnabled: Boolean, confirmCallback: () -> Unit) {
        val alpha = if (isEnabled) 1f else 0.5f
        TextButton(
            modifier = Modifier
                .background(
                    color = MaterialTheme.colors.secondary.copy(alpha = alpha),
                    shape = MaterialTheme.shapes.small
                ),
            onClick = confirmCallback,
            enabled = isEnabled
        ) {
            Text(
                LexemeConstants.Generic.COMPLETE_BUTTON,
                color = MaterialTheme.colors.onSecondary.copy(alpha = alpha)
            )
        }
    }

    @Composable
    private fun verticalSpacer() {
        Spacer(Modifier.height(8.dp))
    }

    @Composable
    private fun horizontalSpacer() {
        Spacer(Modifier.width(8.dp))
    }

    @Composable
    fun subItemHorizontalSpacer() {
        Spacer(Modifier.width(24.dp))
    }
}

private const val ROW_FILL = 0.8f
