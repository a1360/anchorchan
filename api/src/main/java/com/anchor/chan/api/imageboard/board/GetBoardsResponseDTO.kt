package com.anchor.chan.api.imageboard.board

import com.fasterxml.jackson.annotation.JsonProperty

class GetBoardsResponseDTO(
    @JsonProperty("boards") val boards: List<BoardDTO>?
)
