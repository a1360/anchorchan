CREATE TABLE Account
(
    accountId     SERIAL        NOT NULL PRIMARY KEY,
    publicKey     VARCHAR(1240) NOT NULL,
    publicKeyHash VARCHAR(50)   NOT NULL,
    timestamp     TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE INDEX accountHashIndex ON Account (publicKeyHash);

CREATE TABLE AccountChainBlock
(
    blockId   SERIAL        NOT NULL,
    accountId SERIAL        NOT NULL,
    token     VARCHAR(1000) NOT NULL,
    timestamp TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (accountId) REFERENCES Account (accountId) ON DELETE CASCADE,
    PRIMARY KEY (blockId, accountId)
);

CREATE TABLE ChainSettings
(
    settingsId   INT NOT NULL PRIMARY KEY,
    leadingZeros INT NOT NULL
);
