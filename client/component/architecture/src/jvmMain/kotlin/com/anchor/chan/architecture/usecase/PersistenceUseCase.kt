package com.anchor.chan.architecture.usecase

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.drop
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import kotlinx.serialization.KSerializer
import kotlinx.serialization.json.Json
import kotlinx.serialization.serializer
import org.slf4j.LoggerFactory
import java.io.File

private val logger = LoggerFactory.getLogger(PersistenceUseCase::class.java)

open class PersistenceUseCase<T>(
    private val serializer: KSerializer<T>,
    private val cacheName: String,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO,
) {

    suspend fun storeStatePeriodically(stateFlow: StateFlow<T>) {
        var previousValue: String? = null
        stateFlow
            .drop(1) // drop first unloaded state
            .map { Json.encodeToString(serializer, it) }
            .flowOn(ioDispatcher)
            .collect {
                if (previousValue != it) {
                    previousValue = it
                    saveChangesToDisk(it)
                }
            }
    }

    private suspend fun saveChangesToDisk(stateString: String) {
        withContext(ioDispatcher) {
            val file = File(cacheName)
            file.writeText(stateString)
        }
    }

    suspend fun loadFromDisk(): T? =
        withContext(ioDispatcher) {
            try {
                File(cacheName)
                    .takeIf { it.exists() }
                    ?.readText()
                    ?.let { Json.decodeFromString(serializer, it) }
            } catch (t: Throwable) {
                logger.error("Failed to load from disk", t)
                null
            }
        }

    companion object {

        inline fun <reified T : Any> create(
            cacheName: String,
            ioDispatcher: CoroutineDispatcher = Dispatchers.IO,
        ): PersistenceUseCase<T> =
            PersistenceUseCase(
                serializer = serializer(),
                cacheName = cacheName,
                ioDispatcher = ioDispatcher
            )
    }
}
