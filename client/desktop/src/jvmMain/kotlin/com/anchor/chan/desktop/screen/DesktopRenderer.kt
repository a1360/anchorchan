package com.anchor.chan.desktop.screen

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.runtime.Composable
import androidx.compose.ui.ExperimentalComposeUiApi
import com.anchor.chan.desktop.screen.account.AccountSettingsRenderer
import com.anchor.chan.desktop.screen.settings.SettingsRenderer
import com.anchor.chan.desktop.screen.splash.SplashRenderer
import com.anchor.chan.desktop.screen.threadbrowser.ThreadBrowserRenderer
import com.anchor.chan.desktop.screen.viewmodel.AccountSettingsViewModel
import com.anchor.chan.desktop.screen.viewmodel.DesktopViewModel
import com.anchor.chan.desktop.screen.viewmodel.SettingsViewModel
import com.anchor.chan.desktop.screen.viewmodel.SplashViewModel
import com.anchor.chan.desktop.screen.viewmodel.ThreadBrowserViewModel

@ExperimentalFoundationApi
@ExperimentalComposeUiApi
@Composable
fun renderDesktopModel(model: DesktopViewModel) {
    when (model) {
        is SplashViewModel -> SplashRenderer.render(model)
        is SettingsViewModel -> SettingsRenderer.render(model)
        is ThreadBrowserViewModel -> ThreadBrowserRenderer.render(model)
        is AccountSettingsViewModel -> AccountSettingsRenderer.render(model)
    }.let { }
}
