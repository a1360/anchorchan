
object Deps {

    private const val kotestVersion = "5.1.0"
    const val kotest = "io.kotest:kotest-runner-junit5:$kotestVersion"
    const val kotestAssertions = "io.kotest:kotest-assertions-core:$kotestVersion"
    const val testContainers = "org.testcontainers:testcontainers:1.16.3"
    const val testContainerPostgresql = "org.testcontainers:postgresql:1.16.3"
    const val testContainerKotest = "io.kotest.extensions:kotest-extensions-testcontainers:1.2.1"

    const val logback = "ch.qos.logback:logback-core:1.2.10"
    const val slf4j = "org.slf4j:slf4j-api:1.7.32"
    const val logbackClassic = "ch.qos.logback:logback-classic:1.2.10"

    const val kotlinCoroutines = "org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.0"

    const val jdbi = "org.jdbi:jdbi3-core:3.21.0"
    const val postgres = "org.postgresql:postgresql:42.1.4"

    const val kotlinxSerialization = "org.jetbrains.kotlinx:kotlinx-serialization-json:1.2.1"

    const val resiliance4jKotlin = "io.github.resilience4j:resilience4j-kotlin:${DepVersions.RESILIANCE4J}"
    const val resiliance4jCache = "io.github.resilience4j:resilience4j-cache:${DepVersions.RESILIANCE4J}"
    const val resiliance4jRetry = "io.github.resilience4j:resilience4j-retry:${DepVersions.RESILIANCE4J}"
    const val caffeineCache = "com.github.ben-manes.caffeine:caffeine:${DepVersions.CAFFEINE}"

    const val arrowCore = "io.arrow-kt:arrow-core:${DepVersions.ARROW}"
}
