package com.anchor.chan.server.imageboard.board.datasource

import com.anchor.chan.server.imageboard.post.datasource.PostEntity
import com.anchor.chan.server.imageboard.post.datasource.PostRepository
import com.anchor.chan.server.imageboard.post.datasource.ThreadEntity
import com.anchor.chan.server.imageboard.post.datasource.ThreadEntityGeneratedFields
import com.anchor.chan.server.imageboard.post.datasource.ThreadEntityGeneratedFieldsMapper
import com.anchor.chan.server.imageboard.post.datasource.ThreadEntityMapper
import com.anchor.publickey.crypto.PublicKeyHash
import com.google.inject.Inject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.jdbi.v3.core.Handle
import org.jdbi.v3.core.Jdbi
import org.slf4j.LoggerFactory
import java.time.Instant

private val logger = LoggerFactory.getLogger(ThreadRepository::class.java)

class ThreadRepository @Inject constructor(
    private val jdbi: Jdbi,
    private val postRepository: PostRepository,
) {

    init {
        jdbi.registerRowMapper(ThreadEntityMapper())
        jdbi.registerRowMapper(ThreadEntityGeneratedFieldsMapper())
    }

    suspend fun insertThread(
        boardIds: List<String>,
        title: String,
        message: String,
        publicKey: PublicKeyHash,
        signature: String,
        timestamp: Instant,
    ): ThreadEntity =
        withContext(Dispatchers.IO) {
            jdbi.inTransaction<ThreadEntity, Exception> {
                val threadGeneratedFields = insertNewThread(it)
                insertThreadBoardIds(it, boardIds, threadGeneratedFields.threadId)
                val postEntity =
                    insertPostOnThreadCreated(
                        it,
                        title,
                        message,
                        threadGeneratedFields.threadId,
                        publicKey,
                        signature,
                        timestamp
                    )

                ThreadEntity(
                    threadId = threadGeneratedFields.threadId,
                    boardIds = boardIds,
                    updateTimestamp = threadGeneratedFields.updateTimestamp,
                    postEntity = postEntity
                )
            }
        }

    private fun insertNewThread(handle: Handle): ThreadEntityGeneratedFields =
        handle.createUpdate("INSERT INTO Thread DEFAULT VALUES RETURNING threadId, updateTimestamp")
            .executeAndReturnGeneratedKeys(*ThreadEntity.generatedFields)
            .mapTo(ThreadEntityGeneratedFields::class.java)
            .one()

    private fun insertThreadBoardIds(handle: Handle, boardIds: List<String>, threadId: String) {
        for (id in boardIds) {
            handle.createUpdate("INSERT INTO ThreadBoardId (threadId, boardId) VALUES ( :threadId, :boardId )")
                .bind("threadId", threadId.toInt())
                .bind("boardId", id.toInt())
                .execute()
        }
    }

    private fun insertPostOnThreadCreated(
        handle: Handle,
        title: String?,
        message: String,
        threadId: String,
        publicKey: PublicKeyHash,
        signature: String,
        timestamp: Instant,
    ): PostEntity =
        runBlocking {
            postRepository.insertPost(handle, title, message, threadId, publicKey, signature, timestamp)
        }

    suspend fun requestThread(threadId: String): ThreadEntity? =
        withContext(Dispatchers.IO) {
            jdbi.withHandle<ThreadEntity?, Exception> {
                it.createQuery(REQUEST_THREAD_SQL)
                    .bind("id", threadId)
                    .mapTo(ThreadEntity::class.java)
                    .findOne()
                    .orElse(null)
            }
        }

    suspend fun requestThreadBoardIds(threadIds: List<String>): List<String> =
        withContext(Dispatchers.IO) {
            jdbi.withHandle<List<String>, Exception> {
                it.createQuery("SELECT boardId FROM ThreadBoardId WHERE threadId IN (<threadIds>)")
                    .bindList("threadIds", threadIds.map { it.toInt() })
                    .mapTo(String::class.java)
                    .list()
            }
        }

    suspend fun requestThreadsForBoard(boardIds: List<String>): List<ThreadEntity> =
        withContext(Dispatchers.IO) {
            jdbi.withHandle<List<ThreadEntity>, Exception> {
                it
                    .createQuery(REQUEST_THREADS_FOR_BOARD_SQL)
                    .bindList("boardIds", boardIds.map { it.toInt() })
                    .mapTo(ThreadEntity::class.java)
                    .list()
            }
        }

    companion object {
        private val REQUEST_THREADS_FOR_BOARD_SQL =
            """
            WITH thread_board_ids AS
                     (
                         SELECT threadId, string_agg(boardId::varchar(255), ',') AS boardIds
                         FROM ThreadBoardId
                         WHERE threadId IN (SELECT threadId FROM ThreadBoardId WHERE boardId IN (<boardIds>))
                         GROUP BY threadId
                     )
            SELECT * FROM thread_board_ids Thread
                     JOIN Post AS p ON p.postId =
                                       (SELECT p2.postId
                                        FROM Post AS p2
                                        WHERE p2.threadId = Thread.threadId
                                        ORDER BY p2.timestamp
                                        LIMIT 1)
                    JOIN Thread as t ON t.threadId = Thread.threadId
                    ORDER BY t.updateTimestamp DESC
            """.trimIndent()

        private val REQUEST_THREAD_SQL =
            """
            WITH thread_board_ids AS
                     (
                         SELECT threadId, string_agg(boardId::varchar(255), ',') AS boardIds
                         FROM ThreadBoardId
                         WHERE threadId = :id
                         GROUP BY threadId
                     )
            SELECT * FROM (thread_board_ids) ThreadBoardIds
                     JOIN Post AS p ON p.postId =
                                       (SELECT p2.postId
                                        FROM Post AS p2
                                        WHERE p2.threadId = ThreadBoardIds.threadId
                                        ORDER BY p2.timestamp
                                        LIMIT 1)
                     JOIN Thread as t ON t.threadId = ThreadBoardIds.threadId
            """.trimIndent()
    }
}
