plugins {
    kotlin("multiplatform")
    kotlin("plugin.serialization") version "1.5.0"
    id("org.jetbrains.compose") version DepVersions.DESKTOP_COMPOSE
}

repositories {
    google()
}

kotlin {
    jvm {
        compilations.all {
            kotlinOptions.jvmTarget = "11"
        }
    }
    sourceSets {
        named("jvmMain") {
            dependencies {
                implementation(compose.foundation)
                implementation(compose.material)
                implementation(project(":client:component:account-management"))
                implementation(project(":client:component:network-client"))
                implementation(project(":client:component:client-models"))
                implementation(project(":client:component:architecture"))
                implementation(project(":client:component:logger"))
                implementation(project(":client:library:bandwidthestimate"))
                implementation(project(":client:library:i2p"))
                implementation(project(":common:library:hashcash"))
                implementation(project(":common:library:publickey-crypto"))

                implementation(Deps.kotlinxSerialization)
                implementation(Deps.resiliance4jKotlin)
                implementation(Deps.resiliance4jRetry)
                implementation(Deps.resiliance4jCache)
                implementation(Deps.caffeineCache)
                implementation(Deps.arrowCore)
            }
        }
    }
}
