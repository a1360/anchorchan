package com.anchor.chan.account

import com.anchor.publickey.crypto.AssymetricKeyPair
import com.anchor.publickey.crypto.AssymetricPublicKey
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
data class AccountState(
    @Transient
    val isLoadedFromCache: Boolean = false,
    val accounts: Map<AssymetricPublicKey, AccountData> = emptyMap(),
    val targetAccountLength: Int? = null,
) {

    @Serializable
    data class AccountData(
        val keys: AssymetricKeyPair,
        val accountEndDate: Long,
        val chain: List<Chain>,
        val associatedThreads: Set<String> = emptySet(),
    ) {

        @Serializable
        data class Chain(
            val token: String,
        )
    }
}
