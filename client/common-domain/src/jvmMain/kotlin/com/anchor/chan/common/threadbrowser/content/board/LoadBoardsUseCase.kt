package com.anchor.chan.common.threadbrowser.content.board

import com.anchor.chan.model.imageboard.BoardModel
import com.anchor.chan.network.client.BoardNetworkClient
import com.github.benmanes.caffeine.cache.Caffeine
import com.github.benmanes.caffeine.cache.LoadingCache
import io.github.resilience4j.kotlin.retry.decorateSuspendFunction
import io.github.resilience4j.retry.RetryConfig
import io.github.resilience4j.retry.RetryRegistry
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.runBlocking
import org.slf4j.LoggerFactory
import java.time.Duration

private val logger = LoggerFactory.getLogger(LoadBoardsUseCase::class.java)

class LoadBoardsUseCase(
    private val selectAccountUseCase: SelectAccountUseCase,
    private val boardNetworkClient: BoardNetworkClient,
    retryRegistry: RetryRegistry,
) {

    private val retryConfig = RetryConfig.Builder<RetryConfig>(retryRegistry.defaultConfig)
        .maxAttempts(Int.MAX_VALUE)
        .build()
    private val retry = retryRegistry
        .retry("LoadBoards", retryConfig)
        .decorateSuspendFunction { performRequestBoards() }
    private val cacheInstance: LoadingCache<Unit, List<BoardModel>> = Caffeine.newBuilder()
        .refreshAfterWrite(Duration.ofMinutes(5))
        .maximumSize(1)
        .build { key -> runBlocking { retry() } }
    private val refreshCacheEvents = MutableSharedFlow<List<BoardModel>>()

    fun loadBoards(): Flow<List<BoardModel>> =
        refreshCacheEvents
            .onStart { emit(cacheInstance.get(Unit)) }
            .flowOn(Dispatchers.IO)

    private suspend fun performRequestBoards(): List<BoardModel> =
        selectAccountUseCase.fetchBestAccount()
            ?.keys
            .let { boardNetworkClient.requestBoards(it) }
            .also { refreshCacheEvents.emit(it) }
}
