package com.anchor.chan.common.threadbrowser.watcher

import com.anchor.chan.common.threadbrowser.content.board.SelectAccountUseCase
import com.anchor.chan.model.account.AccountDataModel
import com.anchor.chan.model.imageboard.PostModel
import com.anchor.chan.network.client.PostNetworkClient
import com.anchor.chan.network.client.PostUpdateResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.flatMapConcat
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import java.time.Instant
import java.util.concurrent.TimeUnit

private val logger = LoggerFactory.getLogger(ThreadWatcherUpdateStreamerUseCase::class.java)

internal class ThreadWatcherUpdateStreamerUseCase(
    private val store: ThreadWatcherStateStore,
    private val selectAccountUseCase: SelectAccountUseCase,
    private val client: PostNetworkClient,
    private val scope: CoroutineScope = CoroutineScope(Dispatchers.IO)
) {

    fun setupStreamThreadUpdates() {
        periodicallyRefresh()
        performInitialRequestForNewThreads()
    }

    private fun periodicallyRefresh() {
        scope.launch {
            var lastTimestamp: Instant = Instant.EPOCH
            var currentDelay = START_REFRESH_TIME
            while (true) {
                when (val result = requestThreadUpdates(lastTimestamp)) {
                    is UpdateResult.Error,
                    is UpdateResult.EmptyResult -> {
                        currentDelay = Math.min(currentDelay * 2, MAX_REFRESH_TIME)
                    }
                    is UpdateResult.NothingToRequest -> {
                        currentDelay = START_REFRESH_TIME
                    }
                    is UpdateResult.Success -> {
                        currentDelay = START_REFRESH_TIME
                        lastTimestamp = result.timestamp
                        store.appendPosts(result.posts, result.accounts)
                    }
                }
                delay(currentDelay)
            }
        }
    }

    private suspend fun requestThreadUpdates(lastTimestamp: Instant): UpdateResult {
        val account = selectAccountUseCase.fetchBestAccount() ?: run {
            logger.error("Unable to request thread updates due to not having an account ready")
            return UpdateResult.Error
        }
        val threads = store.state.value.watchedThreads.keys.takeIf { it.isNotEmpty() } ?: run {
            return UpdateResult.NothingToRequest
        }

        return try {
            val newPostsResult = client.requestUpdate(account.keys, threads.toList(), lastTimestamp)
            if (newPostsResult.posts.isEmpty()) {
                UpdateResult.EmptyResult
            } else {
                val newAccounts = newPostsResult.toAccountsByThread()
                UpdateResult.Success(
                    posts = newPostsResult.posts.groupBy { it.threadId },
                    accounts = newAccounts,
                    timestamp = newPostsResult.latestTimestamp
                )
            }
        } catch (t: Throwable) {
            logger.error("Failed to request new thread updates. Failing safely.", t)
            UpdateResult.Error
        }
    }

    private fun performInitialRequestForNewThreads() {
        scope.launch {
            store
                .state
                .flatMapConcat { it.watchedThreads.entries.asFlow() }
                .filter { it.value.posts.isEmpty() }
                .collect {
                    val account = selectAccountUseCase.fetchAccountForThread(it.value.threadId) ?: run {
                        logger.error("Unable to request initial post data due to missing a valid account")
                        return@collect
                    }
                    val latestUpdates = client.requestUpdate(account.keys, listOf(it.value.threadId), Instant.EPOCH)
                    val newPosts = latestUpdates.posts.groupBy { it.threadId }
                    val newAccountsByThread = latestUpdates.toAccountsByThread()
                    store.appendPosts(newPosts, newAccountsByThread)
                }
        }
    }

    private fun PostUpdateResponse.toAccountsByThread() =
        posts
            .map { it.threadId to it.accountKeyHash }
            .toSet()
            .groupBy { it.first }
            .mapValues { it.value.mapNotNull { accounts[it.second] } }

    private sealed interface UpdateResult {
        data class Success(
            val posts: Map<String, List<PostModel>>,
            val accounts: Map<String, List<AccountDataModel>>,
            val timestamp: Instant
        ) : UpdateResult

        object NothingToRequest : UpdateResult
        object EmptyResult : UpdateResult
        object Error : UpdateResult
    }

    companion object {
        private val START_REFRESH_TIME = TimeUnit.SECONDS.toMillis(30)
        private val MAX_REFRESH_TIME = TimeUnit.MINUTES.toMillis(5)
    }
}
