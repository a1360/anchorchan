package com.anchor.chan.server.imageboard

import com.anchor.chan.server.account.AccountService
import com.anchor.chan.server.imageboard.board.BoardModule
import com.anchor.chan.server.imageboard.board.datasource.BoardRepository
import com.anchor.chan.server.imageboard.board.datasource.ThreadRepository
import com.anchor.chan.server.imageboard.post.PostModule
import com.anchor.chan.server.imageboard.usecase.LocalRequestAccountDataUseCase
import com.anchor.chan.server.imageboard.usecase.RequestAccountChainLengthUseCase
import com.anchor.chan.server.imageboard.usecase.RequestAccountChainLengthUseCaseImpl
import com.anchor.chan.server.imageboard.usecase.RequestAccountDataUseCase
import com.anchor.chan.server.imageboard.usecase.VerifyPermissionUseCase
import com.anchor.chan.server.imageboard.usecase.VerifyPermissionUseCaseImpl
import com.anchor.publickey.crypto.AssymetricKeyVerifier
import com.anchor.publickey.crypto.ecc.EccVerifier
import com.google.inject.AbstractModule
import com.google.inject.Provides
import com.google.inject.Singleton

class ImageboardModule : AbstractModule() {

    override fun configure() {
        install(BoardModule())
        install(PostModule())
        bind(AssymetricKeyVerifier::class.java).toInstance(EccVerifier())
    }

    @Provides
    @Singleton
    fun provideVerifyPermissionUseCase(
        requestAccountDataUseCase: RequestAccountDataUseCase,
        requestAccountChainLengthUseCase: RequestAccountChainLengthUseCase,
        signatureVerifier: AssymetricKeyVerifier,
        boardRepository: BoardRepository,
        threadRepository: ThreadRepository,
    ): VerifyPermissionUseCase =
        VerifyPermissionUseCaseImpl(
            getAccountDataUseCase = requestAccountDataUseCase,
            requestAccountChainLength = requestAccountChainLengthUseCase,
            signatureVerifier = signatureVerifier,
            boardRepository = boardRepository,
            threadRepository = threadRepository,
        )

    @Provides
    @Singleton
    fun provideRequestAccountChainLengthUseCase(
        signatureVerifier: AssymetricKeyVerifier,
        requestAccountDataUseCase: RequestAccountDataUseCase,
    ): RequestAccountChainLengthUseCase =
        RequestAccountChainLengthUseCaseImpl(
            signatureVerifier = signatureVerifier,
            getAccountDataUseCase = requestAccountDataUseCase,
        )

    @Provides
    @Singleton
    fun provideRequestAccountUseCase(
        accountService: AccountService
    ): RequestAccountDataUseCase =
        LocalRequestAccountDataUseCase(accountService)
}
