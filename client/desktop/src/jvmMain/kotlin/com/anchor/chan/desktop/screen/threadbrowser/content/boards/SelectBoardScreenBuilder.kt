package com.anchor.chan.desktop.screen.threadbrowser.content.boards

import com.anchor.chan.architecture.ui.ScreenBuilder
import com.anchor.chan.common.threadbrowser.ThreadBrowserComponent
import com.anchor.chan.common.threadbrowser.watcher.ThreadWatcherContent
import com.anchor.chan.desktop.screen.viewmodel.ThreadBrowserViewModel
import com.anchor.chan.model.imageboard.BoardModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart

class SelectBoardScreenBuilder(
    private val component: ThreadBrowserComponent
) : ScreenBuilder<ThreadBrowserViewModel.ContentViewModel.SelectBoardViewModel> {

    private val loadBoardsUseCase = component.loadBoardsUseCase
    private val watcherStateStore = component.threadWatcherStateStore
    private val onBoardsSelected: (List<BoardModel>) -> Unit = {
        watcherStateStore.setSelected(ThreadWatcherContent.Board(it.sortedBy { it.boardId }))
    }

    override suspend fun build(): Flow<ThreadBrowserViewModel.ContentViewModel.SelectBoardViewModel> =
        loadBoardsUseCase
            .loadBoards()
            .map { it.toViewModel() }
            .onStart { emit(loading()) }

    fun loading() =
        ThreadBrowserViewModel.ContentViewModel.SelectBoardViewModel(
            isLoading = true,
            boards = emptyList(),
            onBoardsSelected = onBoardsSelected
        )

    private fun List<BoardModel>.toViewModel() =
        ThreadBrowserViewModel.ContentViewModel.SelectBoardViewModel(
            isLoading = false,
            boards = map { it.toViewModel() },
            onBoardsSelected = onBoardsSelected
        )

    private fun BoardModel.toViewModel() =
        ThreadBrowserViewModel.ContentViewModel.SelectBoardViewModel.BoardViewModel(
            name = "$shortName - $name",
            boardModel = this
        )
}
