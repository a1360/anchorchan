INSERT INTO ChainSettings (settingsId, leadingZeros)
VALUES (0, 5);

INSERT INTO Board (boardId, boardName, shortName)
VALUES (0, 'Development', '/dev');

INSERT INTO Board (boardId, boardName, shortName)
VALUES (1, 'Testing', '/test');

INSERT INTO Board (boardId, boardName, shortName)
VALUES (2, 'Strict', '/strict');

INSERT INTO BoardPermission (boardId, readChainLength, postChainLength, createThreadChainLength)
VALUES (0, 0, 5, 10);

INSERT INTO BoardPermission (boardId, readChainLength, postChainLength, createThreadChainLength)
VALUES (1, 0, 5, 10);

INSERT INTO BoardPermission (boardId, readChainLength, postChainLength, createThreadChainLength)
VALUES (2, 5, 10, 15);

INSERT INTO Thread (threadId)
VALUES (0);

INSERT INTO ThreadBoardId (threadId, boardId)
VALUES (0, 0);

INSERT INTO Post (threadId, title, message, publicKeyHash, signature, timestamp)
VALUES (0, 'This land belongs to Sparta', 'This is sparta', '', '', CURRENT_TIMESTAMP);

INSERT INTO Thread (threadId)
VALUES (1);

INSERT INTO ThreadBoardId (threadId, boardId)
VALUES (1, 0);

INSERT INTO ThreadBoardId (threadId, boardId)
VALUES (1, 2);

INSERT INTO Post (threadId, title, message, publicKeyHash, signature, timestamp)
VALUES (1, 'This is Athens', 'You are GAY', '', '', CURRENT_TIMESTAMP);

ALTER SEQUENCE thread_threadId_seq RESTART WITH 3;
