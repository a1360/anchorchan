package com.anchor.chan.desktop.screen.threadbrowser.content.boards

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.anchor.chan.desktop.LexemeConstants
import com.anchor.chan.desktop.screen.UiConstants
import com.anchor.chan.desktop.screen.viewmodel.ThreadBrowserViewModel
import com.anchor.chan.desktop.ui.AnimatedInputView
import com.anchor.chan.desktop.ui.CircleLoadingIndicator
import com.anchor.chan.desktop.ui.Fab

object SelectThreadRenderer {

    @ExperimentalFoundationApi
    @ExperimentalComposeUiApi
    @Composable
    fun render(model: ThreadBrowserViewModel.ContentViewModel.SelectThreadViewModel) {
        if (model.threads.isNotEmpty()) {
            renderThreadCatalogue(model)
        } else {
            CircleLoadingIndicator()
        }
    }

    @ExperimentalFoundationApi
    @ExperimentalComposeUiApi
    @Composable
    private fun renderThreadCatalogue(model: ThreadBrowserViewModel.ContentViewModel.SelectThreadViewModel) {
        Box(
            modifier = Modifier.fillMaxSize()
        ) {
            LazyVerticalGrid(
                cells = GridCells.Fixed(UiConstants.SelectThread.GRID_SIZE),
            ) {
                val spacerHeight = UiConstants.POST_SPACING
                val halfSpacerHeight = spacerHeight / 2
                items(model.threads.size) { index ->
                    val item = model.threads[index]
                    Spacer(modifier = Modifier.height(if (index == 0) spacerHeight else halfSpacerHeight))
                    thread(index, item) { model.onThreadSelected(item.model) }
                    Spacer(
                        modifier = Modifier
                            .height(if (index == model.threads.lastIndex) spacerHeight else halfSpacerHeight)
                    )
                }
            }
            val inputIsVisible = remember { mutableStateOf(false) }
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .fillMaxHeight()
                    .padding(end = UiConstants.FAB_PADDING, bottom = UiConstants.FAB_PADDING),
                verticalArrangement = Arrangement.Bottom,
                horizontalAlignment = Alignment.End
            ) {
                Fab(
                    isEnabled = !inputIsVisible.value,
                    painter = painterResource(UiConstants.Icons.POST),
                ) {
                    inputIsVisible.value = true
                }
            }
            Column(
                modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.Bottom
            ) {
                AnimatedInputView(
                    modifier = Modifier
                        .fillMaxWidth()
                        .wrapContentHeight(),
                    enabled = model.allowedToCreatePost,
                    inputIsVisible = inputIsVisible,
                    onTextSubmitted = model.onThreadSubmitted,
                    errorMessage = model.onThreadSubmittedError,
                )
            }
        }
    }

    @Composable
    private fun thread(
        index: Int,
        model: ThreadBrowserViewModel.ContentViewModel.SelectThreadViewModel.ThreadPreviewViewModel,
        onClick: () -> Unit
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .height(200.dp)
                .clickable(onClick = onClick)
                .padding(
                    start = if (index % UiConstants.SelectThread.GRID_SIZE == 0) {
                        0.dp
                    } else {
                        UiConstants.POST_SPACING / 2
                    },
                    end = UiConstants.POST_SPACING / 2,
                    bottom = UiConstants.POST_SPACING / 2,
                    top = if (index < UiConstants.SelectThread.GRID_SIZE) {
                        UiConstants.POST_SPACING
                    } else {
                        UiConstants.POST_SPACING / 2
                    }
                )
                .background(MaterialTheme.colors.surface)
                .padding(UiConstants.POST_PADDING)
        ) {
            postTitleBar(model.initialPost)
            Spacer(modifier = Modifier.height(16.dp))
            message(model.initialPost)
        }
    }

    @Composable
    private fun postTitleBar(
        postViewModel: ThreadBrowserViewModel.ContentViewModel.ThreadViewModel.PostViewModel
    ) {
        Text(
            text = postViewModel.title ?: "",
            style = MaterialTheme.typography.body2,
            color = MaterialTheme.colors.onSurface,
            fontWeight = FontWeight.Bold,
        )
        Spacer(modifier = Modifier.height(4.dp))
        Text(
            text = postViewModel.timestamp,
            style = MaterialTheme.typography.body2,
            color = MaterialTheme.colors.onSurface,
        )
        Spacer(modifier = Modifier.height(4.dp))
        Text(
            text = LexemeConstants.ThreadBrowser.Posts.ID + postViewModel.id,
            style = MaterialTheme.typography.body2,
            color = MaterialTheme.colors.onSurface,
        )
    }

    @Composable
    private fun message(postViewModel: ThreadBrowserViewModel.ContentViewModel.ThreadViewModel.PostViewModel) {
        Text(
            text = postViewModel.message,
            color = MaterialTheme.colors.onSurface,
            textAlign = TextAlign.Start,
            style = MaterialTheme.typography.body1,
            overflow = TextOverflow.Clip
        )
    }
}
