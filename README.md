# Anchor Chan

## Project Goals

AnchorChan is an Imageboard engine built with a focus on anonymity and moderation.

In active development.

<img src="readme/screenshot.png"  width="600">


## Key Features

* User-friendly smart-client.
* Automatically managed I2P connection.
* Automatically managed Proof-of-Work account system requiring no user input.

The client will automatically connect to the I2P anonymous network.

The client generates multiple accounts using HashCash as Proof Of Work to build an account reputation chain.
The Server grants access relative to the account chain length.

When posting the client signs their post. This information is available to other users alongside their PoW account
chain. This allows other clients to verify that other users are authentic. The client will cycle between accounts when
posting in different threads to help maintain user anonymity. Accounts have a limited lifetime to prevent a users post
history from being tracked.

Bans will only ban one of the client multiple accounts, forcing the client to spend energy to generate a new account to maintain
anonymity.

See the project [roadmap](https://gitlab.com/a1360/anchorchan/-/blob/master/roadmap.md) for more.

## Setup for Development

### Building and running locally

Run server with: "./gradlew :server:run"

Run client with: "./gradlew :client:desktop:run"

### Disabling I2P for development

Within the client settings select "Use your own I2P connection".

### Client I2P config

Run the I2P installation within the client/desktop/i2p folder.

## Releases

+ [Pre-Alpha-v1.0.0](https://gitlab.com/a1360/anchorchan/-/releases/pre-alpha-v1.0.0)

## Similar projects

+ [Millchan](https://gitgud.io/millchan/Millchan)

+ [Scuttlebutt Protocol Guide](https://ssbc.github.io/scuttlebutt-protocol-guide/)

+ [MuWire i2p file sharing application](https://github.com/zlatinb/muwire)

+ [Multichan](http://4x13.net/what.html)

+ [NNTPChan](https://github.com/majestrate/nntpchan)

+ [FChannel](https://github.com/FChannel0/FChannel-Server)