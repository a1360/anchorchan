package com.anchor.publickey.crypto.ecc

import com.anchor.publickey.crypto.AssymetricKeyGenerator
import com.anchor.publickey.crypto.AssymetricKeyPair
import com.anchor.publickey.crypto.AssymetricPrivateKey
import com.anchor.publickey.crypto.AssymetricPublicKey
import com.anchor.publickey.crypto.toBase64String
import java.security.KeyPairGenerator
import java.security.SecureRandom
import java.security.spec.ECGenParameterSpec

class EccGenerator : AssymetricKeyGenerator {

    override fun generateKey(): AssymetricKeyPair =
        createGenerator()
            .genKeyPair()
            .let {
                AssymetricKeyPair(
                    publicKey = AssymetricPublicKey(it.public.encoded.toBase64String()),
                    privateKey = AssymetricPrivateKey(it.private.encoded.toBase64String()),
                )
            }

    private fun createGenerator() = KeyPairGenerator.getInstance("EC").apply {
        initialize(ECGenParameterSpec("secp521r1"), SecureRandom())
    }
}
