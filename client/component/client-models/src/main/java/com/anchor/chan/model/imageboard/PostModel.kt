package com.anchor.chan.model.imageboard

import com.anchor.publickey.crypto.PublicKeyHash
import java.time.Instant

data class PostModel(
    val id: String,
    val threadId: String,
    val title: String?,
    val message: String,
    val accountKeyHash: PublicKeyHash,
    val signature: String,
    val timestamp: Instant,
)
