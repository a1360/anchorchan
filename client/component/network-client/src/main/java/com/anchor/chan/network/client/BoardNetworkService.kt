package com.anchor.chan.network.client

import com.anchor.chan.api.imageboard.board.CreateThreadRequestDTO
import com.anchor.chan.api.imageboard.board.CreateThreadResponseDTO
import com.anchor.chan.api.imageboard.board.GetBoardsRequestDTO
import com.anchor.chan.api.imageboard.board.GetBoardsResponseDTO
import com.anchor.chan.api.imageboard.board.GetThreadsRequestDTO
import com.anchor.chan.api.imageboard.board.GetThreadsResponseDTO
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

internal interface BoardNetworkService {

    @POST("/boards/get")
    suspend fun getBoards(@Body getBoardsRequestDTO: GetBoardsRequestDTO): Response<GetBoardsResponseDTO>

    @POST("/boards/threads/get")
    suspend fun getThreads(@Body getThreadsRequestDTO: GetThreadsRequestDTO): Response<GetThreadsResponseDTO>

    @POST("/boards/threads/create")
    suspend fun createThread(@Body createThreadsRequestDTO: CreateThreadRequestDTO): Response<CreateThreadResponseDTO>
}
