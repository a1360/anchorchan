package com.anchor.chan.server.imageboard.post

import com.anchor.chan.api.imageboard.post.PostUpdatesRequestDTO
import com.anchor.chan.api.imageboard.post.SubmitPostRequestDTO
import com.anchor.chan.server.util.await
import com.anchor.chan.server.util.blockingRenderJson
import ratpack.kotlin.handling.KChainAction
import javax.inject.Inject

class PostRouter @Inject constructor(
    private val threadService: PostService
) : KChainAction() {

    override fun execute() {
        post("get") {
            blockingRenderJson {
                val request = it.parse(PostUpdatesRequestDTO::class.java).await()
                val fromTimestamp = request.fromTimestamp
                    ?: throw IllegalArgumentException("Missing fromTimestamp")
                val threadIds = request.threadIds
                    ?: throw IllegalArgumentException("Missing threadIds")
                val signatureBlock = request.signatureBlock
                    ?: throw IllegalArgumentException("Missing signature block")
                threadService.requestPosts(fromTimestamp, threadIds, signatureBlock)
            }
        }
        post("submit") {
            blockingRenderJson {
                val request = it.parse(SubmitPostRequestDTO::class.java).await()
                val post = request.post
                    ?: throw IllegalArgumentException("Missing post")
                val threadId = post.threadId
                    ?: throw IllegalArgumentException("Missing threadId")
                val title = post.title
                val message = post.message
                    ?: throw IllegalArgumentException("Missing message")
                val signatureBlock = request.signatureBlock
                    ?: throw IllegalArgumentException("Missing signature block")
                threadService.performPost(
                    threadId = threadId,
                    title = title,
                    message = message,
                    signatureBlock = signatureBlock
                )
            }
        }
    }
}
