package com.anchor.chan.common.configuration

import com.anchor.chan.architecture.StateStore
import kotlinx.coroutines.launch
import java.util.Calendar

class ConfigurationStateStore() : StateStore<ConfigurationState>(ConfigurationState()) {

    suspend fun updateFromCache(cachedConfig: ConfigurationState?) {
        val isOld = cachedConfig?.isConfigOld() == true
        update {
            val newState = cachedConfig ?: it
            newState.copy(
                isLoading = false,
                isInitialSetupComplete = if (isOld) false else newState.isInitialSetupComplete,
            )
        }
    }

    fun setRouterEmbedded(isEmbedded: Boolean) {
        scope.launch {
            update { it.copy(isRouterEmbedded = isEmbedded) }
        }
    }

    fun setAutoConfigureBandwidth(isAutoEnabled: Boolean) {
        scope.launch {
            update { it.copy(bandwidthSettings = it.bandwidthSettings.copy(isAutoEstimateBandwidth = isAutoEnabled)) }
        }
    }

    fun setBandwidthInBytesPerSecond(inBytes: Int) {
        scope.launch {
            setBandwidthValues(inBytes, state.value.bandwidthSettings.outBoundKBytesPerSecond)
        }
    }

    fun setBandwidthOutBytesPerSecond(outBytes: Int) {
        scope.launch {
            setBandwidthValues(state.value.bandwidthSettings.inBoundKBytesPerSecond, outBytes)
        }
    }

    fun setBandwidthValues(inBytes: Int, outBytes: Int, timeEstimated: Long = -1) {
        scope.launch {
            update {
                it.copy(
                    bandwidthSettings = it.bandwidthSettings.copy(
                        timeEstimated = timeEstimated,
                        inBoundKBytesPerSecond = inBytes,
                        outBoundKBytesPerSecond = outBytes
                    )
                )
            }
        }
    }

    fun invalidateBandwidthEstimate() {
        scope.launch {
            update {
                it.copy(
                    bandwidthSettings = it.bandwidthSettings.copy(
                        timeEstimated = -1,
                        previousEstimateAttempt = 0
                    )
                )
            }
        }
    }

    fun markBandwidthEstimateAsFailed() {
        scope.launch {
            val now = Calendar.getInstance().timeInMillis
            update { it.copy(bandwidthSettings = it.bandwidthSettings.copy(previousEstimateAttempt = now)) }
        }
    }

    fun setColorScheme(colorConfiguration: ColorConfiguration) {
        scope.launch {
            update { it.copy(colorConfiguration = colorConfiguration) }
        }
    }

    fun setInitialConfigCompleted(isCompleted: Boolean) {
        scope.launch {
            update {
                it.copy(
                    isInitialSetupComplete = isCompleted,
                    configVersion = CURRENT_CONFIG_VERSION
                )
            }
        }
    }
}
