package com.anchor.chan.server.account.datasource

import com.anchor.publickey.crypto.PublicKeyHash
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.jdbi.v3.core.Jdbi

class AccountRepository(
    private val jdbi: Jdbi
) {

    init {
        jdbi.registerRowMapper(AccountEntityMapper())
        jdbi.registerRowMapper(AccountChainBlockEntityMapper())
        jdbi.registerRowMapper(ChainSettingsEntityMapper())
        jdbi.registerRowMapper(AccountEntityGeneratedFieldsMapper())
    }

    suspend fun getChainSettings(): ChainSettingsEntity =
        withContext(Dispatchers.IO) {
            jdbi.withHandle<ChainSettingsEntity, Exception> {
                it.createQuery("SELECT * FROM ChainSettings WHERE settingsId = 0")
                    .mapTo(ChainSettingsEntity::class.java)
                    .one()
            }
        }

    suspend fun setChainSettings(leadingZeros: Int): Unit =
        withContext(Dispatchers.IO) {
            jdbi.withHandle<Unit, Exception> {
                it.createUpdate("UPDATE ChainSettings SET leadingZeros = :leadingZeros WHERE settingsId = 0")
                    .bind("leadingZeros", leadingZeros)
                    .execute()
            }
        }

    suspend fun addChainBlock(accountId: String, token: String) {
        withContext(Dispatchers.IO) {
            jdbi.useHandle<Exception> {
                it.createUpdate("INSERT INTO AccountChainBlock (accountId, token) VALUES(:accountId, :token)")
                    .bind("token", token)
                    .bind("accountId", accountId.toInt())
                    .execute()
            }
        }
    }

    suspend fun getLastChainBlock(accountId: String): AccountChainBlockEntity? =
        withContext(Dispatchers.IO) {
            jdbi.withHandle<AccountChainBlockEntity, Exception> {
                it.createQuery("SELECT * FROM AccountChainBlock WHERE accountId = :id ORDER BY timestamp DESC LIMIT 1")
                    .bind("id", accountId.toInt())
                    .mapTo(AccountChainBlockEntity::class.java)
                    .findOne()
                    .orElse(null)
            }
        }

    suspend fun getAccount(publicKeyHash: PublicKeyHash): AccountEntity? =
        withContext(Dispatchers.IO) {
            jdbi.withHandle<AccountEntity, Exception> {
                it.createQuery(
                    """
                      SELECT Account.accountid, publickey, publickeyhash, string_agg(AccountChainBlock.token, ',')  
                      FROM Account
                      INNER JOIN AccountChainBlock ON Account.accountId = AccountChainBlock.accountId
                      WHERE publicKeyHash = :hash
                      GROUP BY Account.accountid, publickey, publickeyhash
                    """.trimIndent()
                )
                    .bind("hash", publicKeyHash.keyHash)
                    .mapTo(AccountEntity::class.java)
                    .findOne()
                    .orElse(null)
            }
        }

    suspend fun getAccounts(publicKeyHashes: List<PublicKeyHash>): List<AccountEntity> =
        withContext(Dispatchers.IO) {
            jdbi.withHandle<List<AccountEntity>, Exception> {
                it.createQuery(
                    """
                      SELECT Account.accountId, publicKey, publicKeyHash, string_agg(AccountChainBlock.token, ',')  
                      FROM Account
                      INNER JOIN AccountChainBlock ON Account.accountId = AccountChainBlock.accountId
                      WHERE publicKeyHash in (<hash>)
                      GROUP BY Account.accountId, publicKey, publicKeyHash
                    """.trimIndent()
                )
                    .bindList("hash", publicKeyHashes.map { it.keyHash })
                    .mapTo(AccountEntity::class.java)
                    .list()
            }
        }

    suspend fun createAccount(publicKeyHash: PublicKeyHash, publicKey: String): AccountEntity =
        withContext(Dispatchers.IO) {
            val accountGenField = jdbi.withHandle<AccountEntityGeneratedFields, Exception> {
                it.createUpdate("INSERT INTO Account (publicKey, publicKeyHash) VALUES (:key, :hash) RETURNING accountId")
                    .bind("key", publicKey)
                    .bind("hash", publicKeyHash.keyHash)
                    .executeAndReturnGeneratedKeys(*AccountEntity.generatedFields)
                    .mapTo(AccountEntityGeneratedFields::class.java)
                    .one()
            }
            AccountEntity(
                accountId = accountGenField.accountId,
                publicKeyHash = publicKeyHash,
                publicKey = publicKey,
                accountChain = emptyList(),
            )
        }

    suspend fun deleteAccountData(accountId: String): Unit =
        withContext(Dispatchers.IO) {
            jdbi.withHandle<Unit, Exception> {
                it.createUpdate("DELETE FROM Account WHERE accountId = :accountId")
                    .bind("accountId", accountId.toInt())
                    .execute()
            }
        }
}
