package com.anchor.chan.server.imageboard

import com.anchor.chan.model.imageboard.PostModel
import com.anchor.chan.network.client.NetworkException
import com.anchor.chan.server.account.AccountEnvironment
import com.anchor.publickey.crypto.AssymetricKeyPair
import com.anchor.publickey.crypto.ecc.EccGenerator
import com.anchor.publickey.crypto.ecc.EccSigner
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.ints.shouldBeGreaterThan
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import io.kotest.matchers.string.shouldContain
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.concurrent.TimeUnit
import kotlin.time.ExperimentalTime

@ExperimentalTime
class ImageBoardAccountActionsFunctionalTest : FunSpec({

    val environment = ImageboardEnvironment()
    val keyGen = EccGenerator()
    val signer = EccSigner()

    beforeSpec {
        environment.start()
        environment.insertDefaultData()
    }

    afterTest {
        environment.clearEnvironmentData()
    }

    afterSpec {
        environment.close()
    }

    context("Thread creation") {

        test("GIVEN a user with no account WHEN creating a thread THEN deny") {
            val key = keyGen.generateKey()

            val result = shouldThrow<NetworkException> {
                environment.boardService.submitThread(key, "Title", "Message", listOf("0"))
            }

            result.message shouldContain AccountEnvironment.AUTH_EXCEPTION
        }

        test("GIVEN a user with a too small chain WHEN creating a thread THEN deny") {
            val key = keyGen.generateKey()
            environment.accountUseCase.setAccountChainLength(key.publicKey, 4)

            val result = shouldThrow<NetworkException> {
                environment.boardService.submitThread(key, "Title", "Message", listOf("0"))
            }

            result.message shouldContain AccountEnvironment.AUTH_EXCEPTION
        }

        test(
            "GIVEN a user with a chain large enough chain for one board " +
                "WHEN creating a multi-thread with a stricter board " +
                "THEN deny"
        ) {
            val key = keyGen.generateKey()
            environment.accountUseCase.setAccountChainLength(key.publicKey, 14)

            val result = shouldThrow<NetworkException> {
                environment.boardService.submitThread(key, "Title", "Message", listOf("0", "2"))
            }

            result.message shouldContain AccountEnvironment.AUTH_EXCEPTION
        }

        test("GIVEN a user that can post but not create WHEN creating a thread THEN deny") {
            val key = keyGen.generateKey()
            environment.accountUseCase.setAccountChainLength(key.publicKey, 9)

            val result = shouldThrow<NetworkException> {
                environment.boardService.submitThread(key, "Title", "Message", listOf("0"))
            }

            result.message shouldContain AccountEnvironment.AUTH_EXCEPTION
        }

        test("GIVEN a user with an old timestamp WHEN creating a thread THEN deny") {
            val key = keyGen.generateKey()
            environment.forcedNetworkProviderDate = Instant.now().minusMillis(TimeUnit.MINUTES.toMillis(5))
            environment.accountUseCase.setAccountChainLength(key.publicKey, 15)

            val result = shouldThrow<NetworkException> {
                environment.boardService.submitThread(key, "Title", "Message", listOf("0"))
            }

            result.message shouldContain AccountEnvironment.AUTH_EXCEPTION
        }

        test("GIVEN a user with a long enough chain WHEN creating a thread THEN allow") {
            val key = keyGen.generateKey()
            environment.accountUseCase.setAccountChainLength(key.publicKey, 15)

            val result = environment.boardService.submitThread(key, "Title", "Message", listOf("0"))

            result shouldNotBe null
        }
    }

    context("Posting to a thread") {

        suspend fun submitPostRequest(key: AssymetricKeyPair, threadId: String): PostModel =
            environment.postService.submitPost(key, threadId, "Title", "Message")

        test("GIVEN a user with no account WHEN posting to a thread THEN deny") {
            val key = keyGen.generateKey()

            val result = shouldThrow<NetworkException> {
                submitPostRequest(key, "0")
            }

            result.message shouldContain AccountEnvironment.AUTH_EXCEPTION
        }

        test("GIVEN a user with a too small chain WHEN posting to a thread THEN deny") {
            val key = keyGen.generateKey()
            environment.accountUseCase.setAccountChainLength(key.publicKey, 4)

            val result = shouldThrow<NetworkException> {
                submitPostRequest(key, "0")
            }

            result.message shouldContain AccountEnvironment.AUTH_EXCEPTION
        }

        test(
            "GIVEN a user with a large enough chain for one board " +
                "WHEN posting to a multi-thread with a stricter board " +
                "THEN deny"
        ) {
            val key = keyGen.generateKey()
            environment.accountUseCase.setAccountChainLength(key.publicKey, 9)

            val result = shouldThrow<NetworkException> {
                submitPostRequest(key, "1")
            }

            result.message shouldContain AccountEnvironment.AUTH_EXCEPTION
        }

        test("GIVEN a user with an old timestamp WHEN posting to a thread THEN deny") {
            val key = keyGen.generateKey()
            environment.forcedNetworkProviderDate = Instant.now().minusMillis(TimeUnit.MINUTES.toMillis(5))
            environment.accountUseCase.setAccountChainLength(key.publicKey, 9)

            val result = shouldThrow<NetworkException> {
                submitPostRequest(key, "0")
            }

            result.message shouldContain AccountEnvironment.AUTH_EXCEPTION
        }

        test("GIVEN a user that can post but not create WHEN posting to a thread THEN allow") {
            val key = keyGen.generateKey()
            environment.accountUseCase.setAccountChainLength(key.publicKey, 9)

            val result = submitPostRequest(key, "0")

            result.id.isNotBlank() shouldBe true
        }

        test("GIVEN a user with a long enough chain WHEN posting to a thread THEN allow") {
            val key = keyGen.generateKey()
            environment.accountUseCase.setAccountChainLength(key.publicKey, 20)

            val result = submitPostRequest(key, "0")

            result.id.isNotBlank() shouldBe true
        }
    }

    context("Requesting available boards") {

        test("GIVEN a user with no acount WHEN requesting boards THEN restricted board list") {
            val key = keyGen.generateKey()

            val result = environment.boardService.requestBoards(key)

            result.size shouldBe 2
        }

        test("GIVEN a user with small read chain WHEN requesting boards THEN restricted board list") {
            val key = keyGen.generateKey()
            environment.accountUseCase.setAccountChainLength(key.publicKey, 4)

            val result = environment.boardService.requestBoards(key)

            result.size shouldBe 2
        }

        test("GIVEN a user with an old timestamp WHEN requesting boards THEN restricted board list") {
            val key = keyGen.generateKey()
            environment.forcedNetworkProviderDate = Instant.now().minusMillis(TimeUnit.MINUTES.toMillis(5))
            environment.accountUseCase.setAccountChainLength(key.publicKey, 20)

            val result = environment.boardService.requestBoards(key)

            result.size shouldBe 2
        }

        test("GIVEN a user with valid read chain WHEN reading a permitted board THEN full access") {
            val key = keyGen.generateKey()
            environment.accountUseCase.setAccountChainLength(key.publicKey, 20)

            val result = environment.boardService.requestBoards(key)

            result.size shouldBe 3
        }
    }

    context("Requesting threads") {

        suspend fun requestSinglePosts(
            key: AssymetricKeyPair,
            threadIds: List<String>,
            timestamp: Instant = Instant.EPOCH
        ): List<PostModel> =
            environment.postService.requestUpdate(key, threadIds, timestamp).posts

        test("GIVEN a user with no account WHEN requesting restricted thread content THEN deny") {
            val key = keyGen.generateKey()

            val result = shouldThrow<NetworkException> {
                requestSinglePosts(key, listOf("1"))
            }

            result.message shouldContain AccountEnvironment.AUTH_EXCEPTION
        }

        test("GIVEN a user with small read chain WHEN requesting restricted thread content THEN deny") {
            val key = keyGen.generateKey()
            environment.accountUseCase.setAccountChainLength(key.publicKey, 4)

            val result = shouldThrow<NetworkException> {
                requestSinglePosts(key, listOf("1"))
            }

            result.message shouldContain AccountEnvironment.AUTH_EXCEPTION
        }

        test("GIVEN a user with an old timestamp WHEN requesting restricted thread content THEN deny") {
            val key = keyGen.generateKey()
            environment.forcedNetworkProviderDate = Instant.now().minusMillis(TimeUnit.MINUTES.toMillis(5))

            val result = shouldThrow<NetworkException> {
                requestSinglePosts(key, listOf("1"))
            }

            result.message shouldContain AccountEnvironment.AUTH_EXCEPTION
        }

        test("GIVEN a user with valid read chain WHEN requesting restricted thread THEN allow") {
            val key = keyGen.generateKey()
            environment.accountUseCase.setAccountChainLength(key.publicKey, 5)

            val result = requestSinglePosts(key, listOf("1"))

            result.size shouldBeGreaterThan 0
        }

        test("GIVEN a user without an account WHEN requesting an unrestricted thread THEN allow") {
            val key = keyGen.generateKey()

            val result = requestSinglePosts(key, listOf("0"))

            result.size shouldBeGreaterThan 0
        }
    }

    context("Requesting the catalogue for the board") {

        test("GIVEN a user with no account WHEN requesting restricted catalogue THEN deny") {
            val key = keyGen.generateKey()

            val result = shouldThrow<NetworkException> {
                environment.boardService.requestThreads(key, setOf("2"))
            }

            result.message shouldContain AccountEnvironment.AUTH_EXCEPTION
        }

        test("GIVEN a user with small read chain WHEN requesting restricted catalogue THEN deny") {
            val key = keyGen.generateKey()
            environment.accountUseCase.setAccountChainLength(key.publicKey, 4)

            val result = shouldThrow<NetworkException> {
                environment.boardService.requestThreads(key, setOf("2"))
            }

            result.message shouldContain AccountEnvironment.AUTH_EXCEPTION
        }

        test(
            "GIVEN a user with an old timestamp and a valid chain " +
                "WHEN requesting unrestricted catalogue " +
                "THEN deny"
        ) {
            val key = keyGen.generateKey()
            environment.accountUseCase.setAccountChainLength(key.publicKey, 5)
            environment.forcedNetworkProviderDate = Instant.now().minus(1, ChronoUnit.DAYS)

            val result = shouldThrow<NetworkException> {
                environment.boardService.requestThreads(key, setOf("2"))
            }

            result.message shouldContain AccountEnvironment.AUTH_EXCEPTION
        }

        test(
            "GIVEN a user with a large enough chain for one board " +
                "WHEN requesting a multi-board catalogue with a stricter board " +
                "THEN deny"
        ) {
            val key = keyGen.generateKey()
            environment.accountUseCase.setAccountChainLength(key.publicKey, 4)

            val result = shouldThrow<NetworkException> {
                environment.boardService.requestThreads(key, setOf("1", "2"))
            }

            result.message shouldContain AccountEnvironment.AUTH_EXCEPTION
        }

        test(
            "GIVEN a user with no account" +
                "WHEN requesting a catalogue for an allowed board " +
                "THEN filter out the threads with a attached to a stricter board"
        ) {
            val key = keyGen.generateKey()

            val result = environment.boardService.requestThreads(key, setOf("0"))

            result.filter { it.boardIds.contains("2") }.size shouldBe 0
        }

        test(
            "GIVEN a user with a full account" +
                "WHEN requesting a catalogue for an allowed board " +
                "THEN allow all threads"
        ) {
            val key = keyGen.generateKey()
            environment.accountUseCase.setAccountChainLength(key.publicKey, 15)

            val result = environment.boardService.requestThreads(key, setOf("0"))

            result.filter { it.boardIds.contains("2") }.size shouldBeGreaterThan 0
        }

        test("GIVEN a user with valid read chain WHEN requesting restricted catalogue THEN allow") {
            val key = keyGen.generateKey()
            environment.accountUseCase.setAccountChainLength(key.publicKey, 5)

            val result = environment.boardService.requestThreads(key, setOf("1", "2"))

            result.size shouldBeGreaterThan 0
        }

        test("GIVEN a user without an account WHEN requesting an unrestricted catalogue THEN allow") {
            val key = keyGen.generateKey()

            val result = environment.boardService.requestThreads(key, setOf("0", "1"))

            result.size shouldBeGreaterThan 0
        }
    }
})
