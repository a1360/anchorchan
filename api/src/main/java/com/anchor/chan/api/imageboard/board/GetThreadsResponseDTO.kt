package com.anchor.chan.api.imageboard.board

import com.fasterxml.jackson.annotation.JsonProperty

data class GetThreadsResponseDTO(
    @JsonProperty("threads") val threads: List<ThreadDTO>?,
)
