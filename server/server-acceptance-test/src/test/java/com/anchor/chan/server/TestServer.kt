package com.anchor.chan.server

import com.anchor.chan.server.config.datasource.DatasourceConfig
import com.anchor.chan.server.enviroment.createTestContainerDatabase
import ratpack.guice.BindingsImposition
import ratpack.guice.BindingsSpec
import ratpack.impose.ImpositionsSpec
import ratpack.server.RatpackServer
import ratpack.server.internal.ServerCapturer
import ratpack.test.ServerBackedApplicationUnderTest
import java.lang.IllegalStateException

open class TestServer(
    private val main: () -> Unit = { com.anchor.chan.server.main() },
    private val addImpositions: BindingsSpec.() -> Unit = { },
) : ServerBackedApplicationUnderTest() {

    private val database = createTestContainerDatabase()
    var server: RatpackServer? = null

    fun start() {
        database.start()
        address
    }

    override fun createServer(): RatpackServer {
        server = ServerCapturer.capture { main() }
        return server ?: throw IllegalStateException("Failed to start server")
    }

    override fun addImpositions(impositions: ImpositionsSpec?) {
        super.addImpositions(impositions)
        impositions?.add(
            BindingsImposition.of {
                val config = DatasourceConfig(
                    jdbcUrl = "jdbc:postgresql://localhost:${database.firstMappedPort}/testContainer",
                    username = "sa",
                    password = "",
                )
                it.bindInstance(config)
                it.addImpositions()
            }
        )
    }

    override fun close() {
        super.close()
        database.close()
    }
}
