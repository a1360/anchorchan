package com.anchor.chan.server.account.datasource

import org.jdbi.v3.core.mapper.RowMapper
import org.jdbi.v3.core.statement.StatementContext
import java.sql.ResultSet

data class ChainSettingsEntity(
    val leadingZeros: Int,
)

internal class ChainSettingsEntityMapper : RowMapper<ChainSettingsEntity> {

    override fun map(rs: ResultSet, ctx: StatementContext): ChainSettingsEntity =
        ChainSettingsEntity(
            leadingZeros = rs.getInt("leadingZeros"),
        )
}
