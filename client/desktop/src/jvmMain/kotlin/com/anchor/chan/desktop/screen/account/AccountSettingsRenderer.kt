package com.anchor.chan.desktop.screen.account

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.anchor.chan.architecture.ui.Renderer
import com.anchor.chan.desktop.LexemeConstants
import com.anchor.chan.desktop.screen.UiConstants
import com.anchor.chan.desktop.screen.roundedClickable
import com.anchor.chan.desktop.screen.viewmodel.AccountSettingsViewModel
import com.anchor.chan.desktop.ui.CircleLoadingIndicator
import compose.icons.FeatherIcons
import compose.icons.feathericons.CheckSquare
import compose.icons.feathericons.Delete
import compose.icons.feathericons.Square
import compose.icons.feathericons.X

object AccountSettingsRenderer : Renderer<AccountSettingsViewModel> {

    @Composable
    override fun render(model: AccountSettingsViewModel) {
        Scaffold(
            content = { createAccountSettings(model) },
            topBar = {
                Icon(
                    rememberVectorPainter(FeatherIcons.X),
                    LexemeConstants.AccountSettings.ACCOUNT_SETTINGS_CLOSE,
                    modifier = Modifier
                        .size(UiConstants.BUTTON_SIZE)
                        .roundedClickable(onClick = model.onCloseClicked)
                        .padding(UiConstants.BUTTON_PADDING)
                )
                Column(modifier = Modifier.fillMaxWidth(), horizontalAlignment = Alignment.CenterHorizontally) {
                    Spacer(Modifier.height(UiConstants.CONTENT_PADDING))
                    Text(
                        text = LexemeConstants.AccountSettings.ACCOUNT_SETTINGS_TITLE,
                        style = MaterialTheme.typography.h4,
                        fontWeight = FontWeight.Bold,
                        color = MaterialTheme.colors.onBackground,
                    )
                }
            }
        )
    }

    @Composable
    private fun createAccountSettings(model: AccountSettingsViewModel) {
        Column(
            modifier = Modifier
                .background(color = MaterialTheme.colors.background)
                .fillMaxSize()
                .padding(PaddingValues(start = UiConstants.CONTENT_PADDING, end = UiConstants.CONTENT_PADDING))
                .verticalScroll(rememberScrollState(0)),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Spacer(Modifier.height(16.dp))
            boardPermissions(model.boards)
            Spacer(Modifier.height(16.dp))
            accountConfig(model)
            Spacer(Modifier.height(16.dp))
            activeAccounts(model)
        }
    }

    @Composable
    private fun boardPermissions(boards: List<AccountSettingsViewModel.BoardPermissionViewModel>) {
        createSectionTitleRow(LexemeConstants.AccountSettings.ACCOUNTS_SETTINGS_SECTION_BOARDS)

        if (boards.isNotEmpty()) {
            Row(modifier = Modifier.fillMaxWidth(ROW_FILL), horizontalArrangement = Arrangement.SpaceBetween) {
                Column(horizontalAlignment = Alignment.Start) {
                    Text(LexemeConstants.AccountSettings.BOARD_NAME, fontWeight = FontWeight.Bold)
                    for (name in boards.map { it.name }) {
                        Spacer(Modifier.height(8.dp))
                        Text(name)
                    }
                }

                Column(horizontalAlignment = Alignment.CenterHorizontally) {
                    Text(LexemeConstants.AccountSettings.CAN_POST, fontWeight = FontWeight.Bold)
                    for (canPost in boards.map { it.canPost }) {
                        Spacer(Modifier.height(8.dp))
                        Icon(
                            rememberVectorPainter(if (canPost) FeatherIcons.CheckSquare else FeatherIcons.Square),
                            contentDescription = LexemeConstants.AccountSettings.CAN_POST,
                            tint = if (canPost) Color.Green else Color.Red,
                            modifier = Modifier.size(18.dp),
                        )
                    }
                }

                Column(horizontalAlignment = Alignment.CenterHorizontally) {
                    Text(LexemeConstants.AccountSettings.CAN_CREATE, fontWeight = FontWeight.Bold)
                    for (canCreate in boards.map { it.canCreateThread }) {
                        Spacer(Modifier.height(8.dp))
                        Icon(
                            rememberVectorPainter(if (canCreate) FeatherIcons.CheckSquare else FeatherIcons.Square),
                            contentDescription = LexemeConstants.AccountSettings.CAN_CREATE,
                            tint = if (canCreate) Color.Green else Color.Red,
                            modifier = Modifier.size(18.dp),
                        )
                    }
                }
            }
        } else {
            Row(modifier = Modifier.height(40.dp)) {
                CircleLoadingIndicator()
            }
        }
    }

    @Composable
    private fun accountConfig(model: AccountSettingsViewModel) {
        createSectionTitleRow(LexemeConstants.AccountSettings.ACCOUNTS_SETTINGS_SECTION_CONFIG)
        Row(modifier = Modifier.fillMaxWidth(ROW_FILL)) {
            Text(LexemeConstants.AccountSettings.CONFIG_TARGET_LENGTH, fontWeight = FontWeight.Bold)
            Text(model.targetLength.toString())
        }
    }

    @Composable
    private fun activeAccounts(model: AccountSettingsViewModel) {
        createSectionTitleRow(LexemeConstants.AccountSettings.ACCOUNT_SETTINGS_SECTION_ACCOUNTS)

        for (account in model.accounts) {
            Row(modifier = Modifier.fillMaxWidth(ROW_FILL), horizontalArrangement = Arrangement.SpaceBetween) {
                Row {
                    Text(LexemeConstants.AccountSettings.ACCOUNT_NAME, fontWeight = FontWeight.Bold)
                    Text(account.publicKeyHash.keyHash)
                }
                Image(
                    painter = rememberVectorPainter(FeatherIcons.Delete),
                    contentDescription = LexemeConstants.AccountSettings.ACCOUNT_DELETE,
                    alignment = Alignment.CenterEnd,
                    modifier = Modifier
                        .roundedClickable { model.onDeleteClicked(account.publicKey) }
                        .size(18.dp),
                    colorFilter = ColorFilter.tint(Color.Red),
                )
            }
            Spacer(Modifier.height(4.dp))
            Row(modifier = Modifier.fillMaxWidth(ROW_FILL)) {
                Text(LexemeConstants.AccountSettings.ACCOUNT_LENGTH, fontWeight = FontWeight.Bold)
                Text(account.chainLength.toString())
            }
            Spacer(Modifier.height(4.dp))
            Row(modifier = Modifier.fillMaxWidth(ROW_FILL)) {
                Text(LexemeConstants.AccountSettings.ACCOUNT_THREADS, fontWeight = FontWeight.Bold)
                Text(account.threadUsageCount.toString())
            }
            Spacer(Modifier.height(16.dp))
        }
    }

    @Composable
    private fun createSectionTitleRow(title: String) {
        Row(
            modifier = Modifier.fillMaxWidth(ROW_FILL),
        ) {
            Text(
                title,
                style = MaterialTheme.typography.h5,
                fontWeight = FontWeight.Bold,
                textAlign = TextAlign.Center,
                color = MaterialTheme.colors.onBackground,
            )
        }
        Spacer(Modifier.height(16.dp))
    }

    private const val ROW_FILL = 0.8f
}
