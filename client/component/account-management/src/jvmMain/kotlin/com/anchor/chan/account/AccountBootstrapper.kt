package com.anchor.chan.account

import com.anchor.chan.account.usecase.BuildChainUseCase
import com.anchor.chan.account.usecase.CalculateTargetLengthUseCase
import com.anchor.chan.account.usecase.GenerateAccountUseCase
import com.anchor.chan.account.usecase.RequestChainSettingsUseCase
import com.anchor.chan.architecture.usecase.PersistenceUseCase
import com.anchor.chan.hashcash.ChainBuilder
import com.anchor.chan.network.client.NetworkClientProvider
import com.anchor.publickey.crypto.AssymetricKeyGenerator
import io.github.resilience4j.retry.RetryRegistry
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AccountBootstrapper(
    private val accountStateStore: AccountStateStore,
    keyGen: AssymetricKeyGenerator,
    chainBuilder: ChainBuilder,
    networkClientProvider: NetworkClientProvider,
    retryRegistry: RetryRegistry,
    private val bootstrapperScope: CoroutineScope = CoroutineScope(Dispatchers.IO)
) {

    private val requestSettings = RequestChainSettingsUseCase(networkClientProvider.accountClient, retryRegistry)
    private val buildUseCase = BuildChainUseCase(
        accountStateStore,
        chainBuilder,
        requestSettings,
        networkClientProvider.accountClient,
    )
    private val generateAccountUseCase = GenerateAccountUseCase(accountStateStore, keyGen)
    private val targetLengthUseCase = CalculateTargetLengthUseCase(
        accountStateStore,
        networkClientProvider.boardClient,
        retryRegistry,
    )
    private val persistence = PersistenceUseCase.create<AccountState>(CACHE_NAME)

    init {
        buildUseCase.buildChains()
        generateAccountUseCase.generateAccounts()
        targetLengthUseCase.calculateTargetLength()
        setupPersistence()
    }

    private fun setupPersistence() {
        bootstrapperScope.launch {
            val cachedState = persistence.loadFromDisk()
            accountStateStore.loadedFromCache(cachedState)
            persistence.storeStatePeriodically(accountStateStore.state)
        }
    }

    companion object {
        private val CACHE_NAME = "${System.getProperty("user.home")}/.i2p/.anchoraccountconfig"
    }
}
