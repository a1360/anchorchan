plugins {
    kotlin("jvm")
}

dependencies {
    implementation("ch.qos.logback:logback-core:1.2.4")
    implementation("ch.qos.logback:logback-classic:1.2.4")
    api("org.slf4j:slf4j-api:1.7.32")
}
