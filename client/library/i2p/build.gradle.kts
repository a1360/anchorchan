plugins {
    kotlin("jvm")
}

dependencies {
    implementation("net.i2p:i2p:${DepVersions.I2P_VERSION}")
    implementation(files("lib/i2ptunnel.jar"))
    implementation(files("lib/jbigi.jar"))
    implementation(files("lib/addressbook.jar"))
    api("net.i2p:i2p:${DepVersions.I2P_VERSION}")
    api("net.i2p:router:${DepVersions.I2P_VERSION}")
    api("net.i2p.client:mstreaming:${DepVersions.I2P_VERSION}")
    api("net.i2p.client:streaming:${DepVersions.I2P_VERSION}")
}
