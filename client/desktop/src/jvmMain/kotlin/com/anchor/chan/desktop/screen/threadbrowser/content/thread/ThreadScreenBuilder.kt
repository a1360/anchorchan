package com.anchor.chan.desktop.screen.threadbrowser.content.thread

import com.anchor.chan.architecture.ui.ScreenBuilder
import com.anchor.chan.common.threadbrowser.ThreadBrowserComponent
import com.anchor.chan.common.threadbrowser.watcher.WatchedThread
import com.anchor.chan.desktop.screen.UiConstants
import com.anchor.chan.desktop.screen.viewmodel.ThreadBrowserViewModel
import com.anchor.chan.model.imageboard.PostModel
import com.anchor.chan.model.imageboard.ThreadModel
import com.anchor.publickey.crypto.PublicKeyHash
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import kotlin.coroutines.coroutineContext

private val logger = LoggerFactory.getLogger(ThreadScreenBuilder::class.java)

class ThreadScreenBuilder(
    private val thread: ThreadModel,
    private val threadBrowserComponent: ThreadBrowserComponent,
) : ScreenBuilder<ThreadBrowserViewModel.ContentViewModel.ThreadViewModel> {

    override suspend fun build(): Flow<ThreadBrowserViewModel.ContentViewModel.ThreadViewModel> =
        ThreadScreen(CoroutineScope(coroutineContext), thread, threadBrowserComponent).build()

    private class ThreadScreen(
        private val scope: CoroutineScope,
        private val thread: ThreadModel,
        private val threadBrowserComponent: ThreadBrowserComponent,
    ) {

        private val onPostSubmittedError = MutableStateFlow<String?>(null)
        private val onPostSubmitted: (title: String, message: String) -> Unit = { title, message ->
            scope.launch {
                val errorMessage = threadBrowserComponent.submitPostUseCase.submitPost(thread.threadId, title, message)
                onPostSubmittedError.update { errorMessage }
            }
        }
        private val onNewPostViewed: (index: Int) -> Unit = { index ->
            threadBrowserComponent.threadWatcherStateStore.notifyPostViewed(thread.threadId, index)
        }

        suspend fun build(): Flow<ThreadBrowserViewModel.ContentViewModel.ThreadViewModel> =
            threadBrowserComponent
                .loadPostsUseCase
                .loadAndListenToPosts(thread.threadId)
                .combine(onPostSubmittedError) { posts, errorMessage -> posts.toViewModel(errorMessage) }
                .flowOn(Dispatchers.Default)

        private fun WatchedThread.toViewModel(
            onPostSubmittedError: String?
        ): ThreadBrowserViewModel.ContentViewModel.ThreadViewModel {
            val postList = posts.map {
                it.toPostViewModel(postsByCurrentUser, accounts[it.accountKeyHash]?.publicKeyHash)
            }
            val postMap = postList.associateBy { it.id }
            return ThreadBrowserViewModel.ContentViewModel.ThreadViewModel(
                posts = postList,
                postMap = postMap,
                onPostSubmitted = onPostSubmitted,
                onPostSubmittedError = onPostSubmittedError,
                onNewPostViewed = onNewPostViewed,
                allowedToPost = true,
                lastViewedPostIndex = lastViewedIndex,
            )
        }

        private fun PostModel.toPostViewModel(
            postsByCurrentUser: Set<String>,
            accountKeyHash: PublicKeyHash?,
        ): ThreadBrowserViewModel.ContentViewModel.ThreadViewModel.PostViewModel =
            ThreadBrowserViewModel.ContentViewModel.ThreadViewModel.PostViewModel(
                id = id,
                title = title,
                message = message,
                timestamp = UiConstants.Time.defaultFormat(timestamp),
                isFromCurrentUser = postsByCurrentUser.contains(id),
                accountId = accountKeyHash
                    ?.keyHash
                    ?.takeLast(10)
                    ?: null.also { logger.error("Post $id has no associated account") }
            )
    }
}
