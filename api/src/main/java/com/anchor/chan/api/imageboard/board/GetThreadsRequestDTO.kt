package com.anchor.chan.api.imageboard.board

import com.anchor.chan.api.account.SignatureBlockDTO

data class GetThreadsRequestDTO(
    val boardIds: List<String>?,
    val signatureBlock: SignatureBlockDTO?,
)
