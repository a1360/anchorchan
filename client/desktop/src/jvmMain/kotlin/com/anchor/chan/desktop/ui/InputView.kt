package com.anchor.chan.desktop.ui

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.input.key.Key
import androidx.compose.ui.input.key.KeyEvent
import androidx.compose.ui.input.key.KeyEventType
import androidx.compose.ui.input.key.isShiftPressed
import androidx.compose.ui.input.key.key
import androidx.compose.ui.input.key.onPreviewKeyEvent
import androidx.compose.ui.input.key.type
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.substring
import androidx.compose.ui.unit.dp
import com.anchor.chan.desktop.LexemeConstants
import com.anchor.chan.desktop.screen.UiConstants
import com.anchor.chan.desktop.screen.isScrolledToTheEnd
import com.anchor.chan.desktop.screen.processMultiLinePostStyle
import com.anchor.chan.desktop.screen.processSingleLinePostStyle
import com.anchor.chan.desktop.screen.roundedClickable
import com.anchor.chan.desktop.screen.scrollToBottom
import compose.icons.FeatherIcons
import compose.icons.feathericons.Send
import kotlinx.coroutines.CoroutineScope

@ExperimentalComposeUiApi
@Composable
fun InputView(
    modifier: Modifier,
    onTextSubmitted: (title: String, message: String) -> Unit,
    focusRequester: FocusRequester?,
    titleFieldValue: MutableState<TextFieldValue> = remember { mutableStateOf(TextFieldValue()) },
    messageFieldValue: MutableState<TextFieldValue> = remember { mutableStateOf(TextFieldValue()) },
    onKeyDown: (Key) -> Boolean = { false },
    errorMessage: String? = null,
) {
    val spacerHeight = UiConstants.Input.PADDING
    val highlightHeight = UiConstants.Input.HIGHLIGHT_HEIGHT
    val roundedShape = RoundedCornerShape(6.dp)
    Column(
        modifier = modifier
            .padding(spacerHeight)
            .background(MaterialTheme.colors.primary, roundedShape)
            .padding(
                top = highlightHeight,
                start = highlightHeight,
                end = highlightHeight,
                bottom = highlightHeight
            )
            .background(MaterialTheme.colors.surface, roundedShape)
    ) {
        titleInput(titleFieldValue, onKeyDown)
        Spacer(
            Modifier
                .padding(top = 2.dp, bottom = 2.dp)
                .height(highlightHeight)
                .fillMaxWidth()
                .background(MaterialTheme.colors.primary)
        )
        messageInput(
            focusRequester,
            onTextSubmitted,
            titleFieldValue,
            messageFieldValue,
            onKeyDown,
            errorMessage
        )
    }
}

@Composable
private fun titleInput(
    titleFieldValue: MutableState<TextFieldValue>,
    onKeyDown: (Key) -> Boolean = { false },
) {
    TextField(
        value = titleFieldValue.value,
        onValueChange = { newValue -> titleFieldValue.value = newValue },
        placeholder = {
            Text(
                text = LexemeConstants.ThreadBrowser.Posts.TITLE_HINT
            )
        },
        modifier = Modifier
            .fillMaxWidth()
            .onPreviewKeyEvent { event ->
                if (event.type != KeyEventType.KeyDown) {
                    return@onPreviewKeyEvent false
                }
                onKeyDown(event.key)
            },
        maxLines = 1,
        colors = TextFieldDefaults.textFieldColors(
            textColor = MaterialTheme.colors.onSurface,
            backgroundColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent,
            focusedIndicatorColor = Color.Transparent,
        ),
        visualTransformation = VisualTransformation.None,
    )
}

@ExperimentalComposeUiApi
@Composable
private fun messageInput(
    focusRequester: FocusRequester?,
    onTextSubmitted: (title: String, message: String) -> Unit,
    titleFieldValue: MutableState<TextFieldValue>,
    messageFieldValue: MutableState<TextFieldValue>,
    onKeyDown: (Key) -> Boolean = { false },
    errorMessage: String? = null,
) {
    val greenTextSpanStyle = UiConstants.Post.greenTextSpanStyle()

    Column {
        TextField(
            value = messageFieldValue.value,
            onValueChange = { newValue ->
                messageFieldValue.value =
                    newValue.copy(annotatedString = newValue.text.processMultiLinePostStyle(greenTextSpanStyle))
            },
            placeholder = {
                Text(
                    text = LexemeConstants.ThreadBrowser.Posts.INPUT_HINT
                )
            },
            modifier = Modifier
                .let { if (focusRequester != null) it.focusRequester(focusRequester) else it }
                .fillMaxWidth()
                .defaultMinSize(minHeight = 150.dp)
                .onPreviewKeyEvent { event ->
                    handleKeyEvents(
                        onTextSubmitted, titleFieldValue, messageFieldValue, onKeyDown, greenTextSpanStyle, event
                    )
                },
            colors = TextFieldDefaults.textFieldColors(
                textColor = MaterialTheme.colors.onSurface,
                backgroundColor = Color.Transparent,
                unfocusedIndicatorColor = Color.Transparent,
                focusedIndicatorColor = Color.Transparent,
            ),
            visualTransformation = VisualTransformation.None,
        )
        Icon(
            painter = rememberVectorPainter(FeatherIcons.Send),
            contentDescription = "Send",
            tint = MaterialTheme.colors.secondary,
            modifier = Modifier
                .align(Alignment.End)
                .size(UiConstants.BUTTON_SIZE)
                .roundedClickable { sendInput(onTextSubmitted, titleFieldValue, messageFieldValue) }
                .padding(UiConstants.SMALLER_BUTTON_PADDING)
        )
    }
    if (errorMessage != null) {
        Text(
            text = errorMessage,
            color = Color.Red,
            modifier = Modifier.padding(start = 2.dp, bottom = 2.dp)
        )
    }
}

@ExperimentalComposeUiApi
private fun handleKeyEvents(
    onTextSubmitted: (title: String, message: String) -> Unit,
    titleFieldValue: MutableState<TextFieldValue>,
    messageFieldValue: MutableState<TextFieldValue>,
    onKeyDown: (Key) -> Boolean,
    greenTextSpanStyle: SpanStyle,
    event: KeyEvent,
): Boolean {
    if (event.type != KeyEventType.KeyDown) {
        return false
    }

    return when (event.key) {
        Key.Enter -> {
            if (event.isShiftPressed) {
                sendInput(onTextSubmitted, titleFieldValue, messageFieldValue)
                true
            } else {
                newLine(messageFieldValue, greenTextSpanStyle)
                true
            }
        }
        else -> {
            onKeyDown(event.key)
        }
    }
}

private fun sendInput(
    onTextSubmitted: (title: String, message: String) -> Unit,
    titleFieldValue: MutableState<TextFieldValue>,
    messageFieldValue: MutableState<TextFieldValue>,
) {
    onTextSubmitted(
        titleFieldValue.value.text,
        messageFieldValue.value.text
    )
    titleFieldValue.value = titleFieldValue.value.copy(text = "")
    messageFieldValue.value = messageFieldValue.value.copy(text = "")
}

private fun newLine(
    messageFieldValue: MutableState<TextFieldValue>,
    greenTextSpanStyle: SpanStyle,
) {
    messageFieldValue.value = messageFieldValue.value.let {
        val minPosition = messageFieldValue.value.selection.min
        val maxPosition = messageFieldValue.value.selection.max
        val newText = it.text.substring(0, minPosition) + System.lineSeparator() + it.text.substring(maxPosition)
        val newCursorPosition = minPosition + System.lineSeparator().length
        it.copy(
            annotatedString = newText.processMultiLinePostStyle(greenTextSpanStyle),
            selection = TextRange(newCursorPosition)
        )
    }
}

fun MutableState<TextFieldValue>.onPostIdClicked(
    id: String,
    greenTextSpanStyle: SpanStyle,
    inputIsVisible: MutableState<Boolean>,
    scrollState: LazyListState,
    scope: CoroutineScope
) {
    val newText =
        value.text + LexemeConstants.ThreadBrowser.Posts.ID_REF + id + System.lineSeparator()
    val newValue = value.copy(
        annotatedString = newText.processSingleLinePostStyle(greenTextSpanStyle),
        selection = TextRange(newText.length)
    )
    this.value = newValue

    if (!inputIsVisible.value && scrollState.isScrolledToTheEnd()) {
        scrollState.scrollToBottom(scope)
    }
    inputIsVisible.value = true
}

@ExperimentalComposeUiApi
@Composable
fun AnimatedInputView(
    enabled: Boolean,
    inputIsVisible: MutableState<Boolean>,
    onTextSubmitted: (title: String, message: String) -> Unit,
    titleFieldValue: MutableState<TextFieldValue> = remember { mutableStateOf(TextFieldValue()) },
    messageFieldValue: MutableState<TextFieldValue> = remember { mutableStateOf(TextFieldValue()) },
    errorMessage: String? = null,
    modifier: Modifier = Modifier
) {
    val focusRequester = remember { FocusRequester() }
    AnimatedVisibility(
        visible = enabled && inputIsVisible.value,
        enter = fadeIn() + slideInVertically(initialOffsetY = { it }),
        exit = fadeOut() + slideOutVertically(targetOffsetY = { it })
    ) {
        InputView(
            modifier = modifier,
            focusRequester = focusRequester,
            onTextSubmitted = onTextSubmitted,
            onKeyDown = {
                when (it) {
                    Key.Escape -> {
                        inputIsVisible.value = false
                        true
                    }
                    else -> false
                }
            },
            messageFieldValue = messageFieldValue,
            titleFieldValue = titleFieldValue,
            errorMessage = errorMessage,
        )
        DisposableEffect(Unit) {
            focusRequester.requestFocus()
            onDispose { }
        }
    }
}
