package com.anchor.chan.desktop.ui

import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import kotlin.math.abs
import kotlin.math.min

@Composable
fun CircleLoadingIndicator() {
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier
            .fillMaxHeight()
            .fillMaxWidth()
    ) {
        Row(modifier = Modifier.width(50.dp).height(24.dp), horizontalArrangement = Arrangement.SpaceBetween) {
            val infiniteTransition = rememberInfiniteTransition()
            val transitionIndex: Float by infiniteTransition.animateFloat(
                initialValue = 0f,
                targetValue = 4f,
                animationSpec = infiniteRepeatable(
                    animation = tween(durationMillis = 1_750)
                )
            )

            for (index in 0 until 3) {
                Box(
                    modifier = Modifier
                        .align(Alignment.CenterVertically)
                        .background(color = Color.LightGray, shape = CircleShape)
                        .size(10.dp + ((1 - min(1f, abs((index + 1f) - transitionIndex))) * 10).dp)
                )
                Spacer(modifier = Modifier.width(4.dp))
            }
        }
    }
}
