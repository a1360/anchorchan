package com.anchor.chan.server.imageboard.board

import com.anchor.chan.api.account.SignatureBlockDTO
import com.anchor.chan.api.imageboard.board.CreateThreadResponseDTO
import com.anchor.chan.api.imageboard.board.GetBoardsResponseDTO
import com.anchor.chan.api.imageboard.board.GetThreadsResponseDTO
import com.anchor.chan.server.imageboard.board.datasource.BoardRepository
import com.anchor.chan.server.imageboard.board.datasource.ThreadRepository
import com.anchor.chan.server.imageboard.usecase.RequestAccountChainLengthUseCase
import com.anchor.chan.server.imageboard.usecase.VerifyPermissionUseCase
import com.anchor.chan.server.toDto
import com.anchor.chan.server.util.extractFields
import com.anchor.chan.server.util.extractFieldsNullable
import com.anchor.publickey.crypto.toCreateThreadSignature
import com.anchor.publickey.crypto.toGetThreadsSignature
import com.anchor.publickey.crypto.toSha1Hex
import com.anchor.publickey.crypto.toSignatureString
import com.google.inject.Inject
import org.slf4j.LoggerFactory

private val logger = LoggerFactory.getLogger(BoardService::class.java)

class BoardService @Inject constructor(
    private val boardRepository: BoardRepository,
    private val threadRepository: ThreadRepository,
    private val verifyPermissionUseCase: VerifyPermissionUseCase,
    private val requestAccountChainLength: RequestAccountChainLengthUseCase,
) {

    suspend fun getBoards(signatureBlock: SignatureBlockDTO): GetBoardsResponseDTO {
        val (publicKey, signature, timestamp) = signatureBlock.extractFieldsNullable()
        val chainLength = requestAccountChainLength.retrieveChainLength(
            publicKey, signatureBlock.toSignatureString(), signature, timestamp
        )

        val permissions = boardRepository.requestBoardPermissions()
            .filter { it.readChainLength <= chainLength }
            .associateBy { it.boardId }
        return boardRepository.requestBoards()
            .mapNotNull { board -> permissions[board.boardId]?.let { board to it } }
            .map { it.toDto() }
            .let { GetBoardsResponseDTO(it) }
    }

    suspend fun getThreads(boardIds: List<String>, signatureBlock: SignatureBlockDTO): GetThreadsResponseDTO {
        val (publicKey, signature, timestamp) = signatureBlock.extractFieldsNullable()
        val expectedSignature = signatureBlock.toGetThreadsSignature(boardIds)

        verifyPermissionUseCase.verifyReadBoardPermission(
            publicKey,
            timestamp,
            expectedSignature,
            signature,
            boardIds,
        )

        val chainLength = requestAccountChainLength.retrieveChainLength(
            publicKey, expectedSignature, signature, timestamp
        )
        val permissions = boardRepository.requestBoardPermissions()
            .filter { it.readChainLength <= chainLength }
            .associateBy { it.boardId }
        return threadRepository.requestThreadsForBoard(boardIds)
            .filter { it.boardIds.map { permissions.containsKey(it) }.reduce { a, b -> a && b } }
            .map { it.toDto() }
            .let { GetThreadsResponseDTO(it) }
    }

    suspend fun createThread(
        boardIds: List<String>,
        message: String,
        title: String,
        signatureBlock: SignatureBlockDTO,
    ): CreateThreadResponseDTO {
        val (publicKey, signature, timestamp) = signatureBlock.extractFields()

        verifyPermissionUseCase.verifyCreateThreadPermission(
            publicKey,
            timestamp,
            signatureBlock.toCreateThreadSignature(boardIds, message, title),
            signature,
            boardIds
        )

        return threadRepository.insertThread(
            boardIds,
            title,
            message,
            publicKey.key.toSha1Hex(),
            signature,
            timestamp,
        )
            .toDto()
            .let { CreateThreadResponseDTO(it) }
    }
}
