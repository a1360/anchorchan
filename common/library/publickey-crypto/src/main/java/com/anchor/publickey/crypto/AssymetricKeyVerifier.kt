package com.anchor.publickey.crypto

interface AssymetricKeyVerifier {

    fun verify(key: AssymetricPublicKey, message: String, signature: String): Boolean
}
