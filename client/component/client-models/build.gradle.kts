plugins {
    kotlin("jvm")
}

kotlin {
    jvmToolchain {
        (this as JavaToolchainSpec).languageVersion.set(JavaLanguageVersion.of("8"))
    }
}

dependencies {
    implementation(project(":api"))
    implementation(project(":common:library:publickey-crypto"))
}
