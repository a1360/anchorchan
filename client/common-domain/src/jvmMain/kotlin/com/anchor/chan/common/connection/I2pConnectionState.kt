package com.anchor.chan.common.connection

data class I2pConnectionState(
    val connectionState: ConnectionState = ConnectionState.SHUTDOWN,
)

enum class ConnectionState {
    MANUAL, SHUTDOWN, SHUTTING_DOWN, CONNECTING, CONNECTED_AND_BUILDING_NETWORK, CONNECTED;
}

fun ConnectionState.canAttemptConnection(): Boolean =
    when (this) {
        ConnectionState.CONNECTED_AND_BUILDING_NETWORK,
        ConnectionState.CONNECTED,
        ConnectionState.MANUAL -> true
        ConnectionState.SHUTDOWN,
        ConnectionState.SHUTTING_DOWN,
        ConnectionState.CONNECTING -> false
    }

fun ConnectionState.isConnectedToLocalI2p(): Boolean =
    when (this) {
        ConnectionState.CONNECTED_AND_BUILDING_NETWORK,
        ConnectionState.CONNECTED -> true
        ConnectionState.SHUTDOWN,
        ConnectionState.SHUTTING_DOWN,
        ConnectionState.MANUAL,
        ConnectionState.CONNECTING -> false
    }
