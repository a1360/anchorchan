package com.anchor.chan.desktop.ui

import androidx.compose.animation.Crossfade
import androidx.compose.animation.core.tween
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import kotlinx.coroutines.flow.StateFlow

@Composable
fun <T> CrossfadeModel(
    stateFlow: StateFlow<T>,
    durationMillis: Int = 500,
    renderModel: @Composable (T) -> Unit
) {
    val state = stateFlow.collectAsState().value
    CrossfadeModel(state, durationMillis, renderModel)
}

@Composable
fun <T> CrossfadeModel(
    model: T,
    durationMillis: Int = 500,
    renderModel: @Composable (T) -> Unit
) {
    val current = remember { mutableStateOf<T?>(null) }
    val old = remember { mutableStateOf<T?>(null) }
    if (current.value?.className() != model?.className()) {
        old.value = current.value
    }
    current.value = model

    Crossfade(model?.className(), animationSpec = tween(durationMillis)) { clazz ->
        val updatingState = current.value.takeIf { clazz == it?.className() } ?: old.value
        updatingState?.let { renderModel(it) }
    }
}

private fun Any.className(): String =
    this::class.java.simpleName
