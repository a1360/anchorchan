object DepVersions {

    const val KOTLIN = "1.6.10"
    const val DESKTOP_COMPOSE = "1.0.1"
    const val I2P_VERSION = "1.6.1"
    const val RESILIANCE4J = "1.7.1"
    const val CAFFEINE = "3.0.5"
    const val ARROW = "1.0.1"
}