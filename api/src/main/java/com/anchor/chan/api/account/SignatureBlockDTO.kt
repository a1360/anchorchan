package com.anchor.chan.api.account

import java.time.Instant

data class SignatureBlockDTO(
    val publicKey: String?,
    val timestamp: Instant?,
    val signature: String? = null,
)
