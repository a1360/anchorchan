package com.anchor.chan.common.connection.usecase

import com.anchor.chan.common.configuration.ConfigurationStateStore
import com.anchor.chan.common.connection.I2pConnectionStateStore
import com.anchor.chan.common.connection.canAttemptConnection
import com.anchor.chan.network.client.NetworkClientProvider
import com.anchor.chan.network.client.NetworkData
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.combine

internal class ConfigureNetworkProviderUseCase(
    private val networkClientProvider: NetworkClientProvider,
    private val connectionStateStore: I2pConnectionStateStore,
    private val configurationStateStore: ConfigurationStateStore
) {

    suspend fun setup() {
        connectionStateStore
            .state
            .combine(configurationStateStore.state) { connection, config -> connection to config }
            .collect {
                val connectionState = it.first.connectionState
                if (connectionState.canAttemptConnection()) {
                    networkClientProvider.updateNetworkData(
                        NetworkData(
                            url = URL,
                            proxyUrl = PROXY_URL,
                            proxyPort = PROXY_PORT,
                        )
                    )
                }
            }
    }

    companion object {
        private const val URL = "http://l2kiechdm6ujqybldxsid2vlmdq2p5tllboud2gwgz2dqukbh6ca.b32.i2p"
        private const val PROXY_URL = "127.0.0.1"
        private const val PROXY_PORT = 4444
    }
}
