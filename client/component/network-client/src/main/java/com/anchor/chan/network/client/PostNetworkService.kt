package com.anchor.chan.network.client

import com.anchor.chan.api.imageboard.post.PostUpdatesRequestDTO
import com.anchor.chan.api.imageboard.post.PostUpdatesResponseDTO
import com.anchor.chan.api.imageboard.post.SubmitPostRequestDTO
import com.anchor.chan.api.imageboard.post.SubmitPostResponseDTO
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

internal interface PostNetworkService {

    @POST("/posts/get")
    suspend fun getPosts(@Body request: PostUpdatesRequestDTO): Response<PostUpdatesResponseDTO>

    @POST("/posts/submit")
    suspend fun submitPost(@Body request: SubmitPostRequestDTO): Response<SubmitPostResponseDTO>
}
