package com.anchor.chan.account.usecase

import com.anchor.chan.model.account.ChainSettingsModel
import com.anchor.chan.network.client.AccountNetworkClient
import com.github.benmanes.caffeine.cache.Caffeine
import io.github.resilience4j.kotlin.retry.decorateSuspendFunction
import io.github.resilience4j.retry.Retry
import io.github.resilience4j.retry.RetryConfig
import io.github.resilience4j.retry.RetryRegistry
import java.time.Duration

internal class RequestChainSettingsUseCase(
    private val accountClient: AccountNetworkClient,
    retryRegistry: RetryRegistry,
) {

    private val retry = RetryConfig.Builder<RetryConfig>(retryRegistry.defaultConfig)
        .maxAttempts(Int.MAX_VALUE)
        .build()
        .let { Retry.of("ChainSettings", it) }
        .decorateSuspendFunction { accountClient.getChainSettings() }
    private val cache = Caffeine.newBuilder()
        .expireAfterWrite(Duration.ofDays(1))
        .build<Unit, ChainSettingsModel>()

    suspend fun fetchSettings(): ChainSettingsModel =
        cache.getIfPresent(Unit) ?: fetchAndCache()

    private suspend fun fetchAndCache(): ChainSettingsModel {
        val settings = retry.invoke()
        cache.put(Unit, settings)
        return settings
    }

    fun invalidateCache() {
        cache.invalidate(Unit)
    }
}
