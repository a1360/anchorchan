package com.anchor.chan.api.imageboard.board

import com.anchor.chan.api.account.SignatureBlockDTO
import com.fasterxml.jackson.annotation.JsonProperty

data class GetBoardsRequestDTO(
    @JsonProperty("signatureBlockDTO") val signatureBlockDTO: SignatureBlockDTO?
)
