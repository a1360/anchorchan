package com.anchor.publickey.crypto.ecc

import com.anchor.publickey.crypto.AssymetricKeySigner
import com.anchor.publickey.crypto.AssymetricPrivateKey
import com.anchor.publickey.crypto.toBase64Bytes
import com.anchor.publickey.crypto.toBase64String
import java.security.KeyFactory
import java.security.Signature
import java.security.spec.PKCS8EncodedKeySpec

class EccSigner : AssymetricKeySigner {

    override fun sign(key: AssymetricPrivateKey, message: String): String {
        val privateKeySpec = PKCS8EncodedKeySpec(key.key.toBase64Bytes())
        val keyFactory = KeyFactory.getInstance("EC")
        val privateKey = keyFactory.generatePrivate(privateKeySpec)

        val signer = Signature.getInstance("SHA256withECDSA")
        signer.initSign(privateKey)
        signer.update(message.toByteArray())
        return signer.sign().toBase64String()
    }
}
