package com.anchor.chan.server.account

import com.anchor.chan.hashcash.ChainVerifier
import com.anchor.chan.hashcash.NettygryppaChainVerifier
import com.anchor.chan.hashcash.NettygryppaChainVerifierConfig
import com.anchor.chan.network.client.AccountNetworkClient
import com.anchor.chan.network.client.NetworkClientProvider
import com.anchor.chan.network.client.NetworkData
import com.anchor.chan.server.TestServer
import com.anchor.chan.server.account.datasource.AccountRepository
import com.anchor.chan.server.enviroment.insertDefaultData
import com.anchor.publickey.crypto.AssymetricPublicKey
import com.anchor.publickey.crypto.ecc.EccSigner
import com.anchor.publickey.crypto.toSha1Hex
import io.kotest.common.runBlocking
import kotlinx.coroutines.Dispatchers
import ratpack.guice.BindingsSpec
import java.io.Closeable
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.TimeZone

class AccountEnvironment : Closeable {

    private val accountRepo: AccountRepository
        get() = server.server!!.registry.get().get(AccountRepository::class.java)
    private val testTimeProvider: () -> Calendar = {
        SimpleDateFormat("yyMMdd")
            .parse("211031")
            .let { Calendar.getInstance(TimeZone.getTimeZone("GMT")).also { cal -> cal.time = it } }
    }
    private val server = TestServer {
        configureTime()
    }
    private val networkProvider by lazy {
        NetworkClientProvider(EccSigner()).apply {
            runBlocking { updateNetworkData(NetworkData("http://127.0.0.1:${server.server!!.bindPort}")) }
        }
    }

    val accountService: AccountService
        get() = server.server!!.registry.get().get(AccountService::class.java)
    val accountNetworkService: AccountNetworkClient by lazy { networkProvider.accountClient }

    val alternativeKey = AssymetricPublicKey("key2")
    val alternativeFirstEncoding = "1:10:211031:key2::ffffffffffffffd9:6fe"
    val defaultKey = AssymetricPublicKey("key")
    val leadingZeros = 10
    val firstExampleEncoding = "1:10:211031:key::ffffffffffffd961:ad5"
    val secondExampleEncoding =
        "1:10:211031:key:prev_hash=0018e6e8006b2f634351663e7412a00c71:fffffffffffff525:b2"
    val invalidSecondExampleEncoding =
        "1:10:211101:key:prev_hash=135a1e01a742457620f41a683f643a43:ffffffffffffff9d:10a"

    private fun BindingsSpec.configureTime() {
        val config = NettygryppaChainVerifierConfig(Dispatchers.IO)
        val verifier = NettygryppaChainVerifier(config, testTimeProvider)
        bindInstance(ChainVerifier::class.java, verifier)
    }

    fun start() {
        server.start()
    }

    fun insertDefaultData() {
        server.insertDefaultData()
    }

    fun configureChainSettingsConfig() {
        runBlocking {
            accountRepo.setChainSettings(leadingZeros)
        }
    }

    fun resetTestAccount() {
        runBlocking {
            accountRepo.getAccount(defaultKey.key.toSha1Hex())
                ?.let { accountRepo.deleteAccountData(it.accountId) }
            accountRepo.getAccount(alternativeKey.key.toSha1Hex())
                ?.let { accountRepo.deleteAccountData(it.accountId) }
        }
    }

    override fun close() {
        server.close()
    }

    companion object {
        const val AUTH_EXCEPTION = "Authentication"
    }
}
