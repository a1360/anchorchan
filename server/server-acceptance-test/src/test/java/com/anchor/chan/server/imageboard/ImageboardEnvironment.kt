package com.anchor.chan.server.imageboard

import com.anchor.chan.network.client.BoardNetworkClient
import com.anchor.chan.network.client.NetworkClientProvider
import com.anchor.chan.network.client.NetworkData
import com.anchor.chan.network.client.PostNetworkClient
import com.anchor.chan.server.TestServer
import com.anchor.chan.server.enviroment.insertDefaultData
import com.anchor.chan.server.imageboard.usecase.RequestAccountDataUseCase
import com.anchor.publickey.crypto.ecc.EccSigner
import com.yahtclub.chan.server.imageboard.usecase.TestRequestAccountDataUseCase
import io.kotest.common.runBlocking
import ratpack.guice.BindingsSpec
import java.io.Closeable
import java.time.Instant

class ImageboardEnvironment : Closeable {

    val accountUseCase: TestRequestAccountDataUseCase = TestRequestAccountDataUseCase()

    private val server = TestServer {
        configureRequestAccountUseCase()
    }

    var forcedNetworkProviderDate: Instant? = null
    private val dateProvider: () -> Instant = { forcedNetworkProviderDate ?: Instant.now() }
    private val networkProvider by lazy {
        NetworkClientProvider(EccSigner(), dateProvider = dateProvider).apply {
            runBlocking { updateNetworkData(NetworkData("http://127.0.0.1:${server.server!!.bindPort}")) }
        }
    }

    val postService: PostNetworkClient by lazy { networkProvider.postClient }
    val boardService: BoardNetworkClient by lazy { networkProvider.boardClient }

    fun start() {
        server.start()
    }

    fun insertDefaultData() {
        server.insertDefaultData()
    }

    fun clearEnvironmentData() {
        accountUseCase.clear()
        forcedNetworkProviderDate = null
    }

    private fun BindingsSpec.configureRequestAccountUseCase() {
        bindInstance(RequestAccountDataUseCase::class.java, accountUseCase)
    }

    override fun close() {
        server.close()
    }
}
