package com.anchor.chan.model

import com.anchor.chan.api.account.AccountDataDTO
import com.anchor.chan.api.account.ProofOfWorkSettingsDTO
import com.anchor.chan.api.imageboard.board.BoardDTO
import com.anchor.chan.api.imageboard.board.BoardPermissionsDTO
import com.anchor.chan.api.imageboard.board.ThreadDTO
import com.anchor.chan.api.imageboard.post.PostDTO
import com.anchor.chan.model.account.AccountDataModel
import com.anchor.chan.model.account.ChainSettingsModel
import com.anchor.chan.model.imageboard.BoardModel
import com.anchor.chan.model.imageboard.BoardPermissions
import com.anchor.chan.model.imageboard.PostModel
import com.anchor.chan.model.imageboard.ThreadModel
import com.anchor.publickey.crypto.AssymetricPublicKey
import com.anchor.publickey.crypto.PublicKeyHash
import java.time.Instant

fun ProofOfWorkSettingsDTO.toModel(): ChainSettingsModel =
    ChainSettingsModel(leadingZeros ?: 0)

fun AccountDataDTO.toModel(): AccountDataModel =
    AccountDataModel(
        publicKeyHash = PublicKeyHash(publicKeyHash),
        publicKey = AssymetricPublicKey(publicKey),
        accountChain = accountChain,
    )

fun ThreadDTO.toModel(): ThreadModel =
    ThreadModel(
        boardIds = boardIds ?: emptyList(),
        threadId = threadId ?: "",
        initialPost = initialPost.toModel(),
        lastUpdated = updatedTimestamp ?: Instant.MIN
    )

fun PostDTO?.toModel(): PostModel =
    PostModel(
        id = this?.postId ?: "",
        threadId = this?.threadId ?: "",
        title = this?.title,
        message = this?.message ?: "",
        timestamp = this?.timestamp ?: Instant.MIN,
        accountKeyHash = this?.publicKeyHash?.let { PublicKeyHash(it) } ?: PublicKeyHash(""),
        signature = this?.signature ?: "",
    )

fun BoardDTO.toModel(): BoardModel =
    BoardModel(
        boardId = boardId ?: "",
        name = name ?: "",
        shortName = shortName ?: "",
        permissions = permissions.toModel()
    )

fun BoardPermissionsDTO?.toModel(): BoardPermissions =
    BoardPermissions(
        readLength = this?.readChainLength ?: 0,
        postLength = this?.postChainLength ?: 0,
        createLength = this?.createThreadChainLength ?: 0,
    )
