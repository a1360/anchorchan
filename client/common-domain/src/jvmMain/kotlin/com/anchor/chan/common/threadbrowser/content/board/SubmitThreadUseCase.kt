package com.anchor.chan.common.threadbrowser.content.board

import arrow.core.Either
import com.anchor.chan.model.imageboard.ThreadModel
import com.anchor.chan.network.client.BoardNetworkClient

class SubmitThreadUseCase(
    private val selectAccountUseCase: SelectAccountUseCase,
    private val loadThreadsUseCase: LoadThreadsUseCase,
    private val boardNetworkClient: BoardNetworkClient,
) {

    suspend fun submitThread(title: String, message: String, boardIds: List<String>): Either<String, ThreadModel> {
        val account = selectAccountUseCase.fetchBestAccount()
            ?.keys
            ?: return Either.Left("No account available")

        return try {
            boardNetworkClient.submitThread(account, title, message, boardIds)
                .let { Either.Right(it) }
                .also { loadThreadsUseCase.invalidate(boardIds) }
        } catch (t: Throwable) {
            Either.Left(t.message ?: "Error submitting thread")
        }
    }
}
