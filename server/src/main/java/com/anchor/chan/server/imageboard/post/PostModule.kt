package com.anchor.chan.server.imageboard.post

import com.anchor.chan.server.imageboard.post.datasource.PostRepository
import com.google.inject.AbstractModule
import com.google.inject.Scopes

class PostModule : AbstractModule() {

    override fun configure() {
        bind(PostService::class.java).`in`(Scopes.SINGLETON)
        bind(PostRouter::class.java).`in`(Scopes.SINGLETON)
        bind(PostRepository::class.java).`in`(Scopes.SINGLETON)
    }
}
