package com.anchor.chan.desktop.screen.threadbrowser.content.thread

import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import com.anchor.chan.desktop.screen.UiConstants
import com.anchor.chan.desktop.screen.isScrolledToTheEnd
import com.anchor.chan.desktop.screen.scrollToBottom
import com.anchor.chan.desktop.screen.viewmodel.ThreadBrowserViewModel.ContentViewModel.ThreadViewModel
import com.anchor.chan.desktop.ui.AnimatedInputView
import com.anchor.chan.desktop.ui.CircleLoadingIndicator
import com.anchor.chan.desktop.ui.Fab
import com.anchor.chan.desktop.ui.onPostIdClicked

internal object ThreadRenderer {

    @ExperimentalComposeUiApi
    @Composable
    fun render(threadViewModel: ThreadViewModel) {
        if (threadViewModel.posts.isNotEmpty()) {
            renderThread(threadViewModel)
        } else {
            CircleLoadingIndicator()
        }
    }

    @ExperimentalComposeUiApi
    @Composable
    private fun renderThread(threadViewModel: ThreadViewModel) {
        Column(
            modifier = Modifier
                .fillMaxSize()
        ) {
            val greenTextStyle = UiConstants.Post.greenTextSpanStyle()
            val messageFieldValue = remember { mutableStateOf(TextFieldValue()) }
            val scrollState = rememberLazyListState()
            val inputIsVisible = remember { mutableStateOf(false) }
            val scope = rememberCoroutineScope()
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f),
            ) {
                createPostList(
                    threadViewModel,
                    scrollState,
                    onIdClicked = {
                        messageFieldValue.onPostIdClicked(
                            id = it,
                            greenTextSpanStyle = greenTextStyle,
                            inputIsVisible = inputIsVisible,
                            scrollState = scrollState,
                            scope = scope
                        )
                    }
                )
                createThreadInteractionButtons(threadViewModel, scrollState, inputIsVisible)
            }
            AnimatedInputView(
                enabled = threadViewModel.allowedToPost,
                inputIsVisible = inputIsVisible,
                onTextSubmitted = threadViewModel.onPostSubmitted,
                messageFieldValue = messageFieldValue,
                errorMessage = threadViewModel.onPostSubmittedError,
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight()
            )
        }
    }

    @Composable
    private fun createPostList(
        model: ThreadViewModel,
        scrollState: LazyListState,
        onIdClicked: (String) -> Unit,
    ) {
        val scope = rememberCoroutineScope()
        val scrollToBottom = remember(model.posts) {
            scrollState.layoutInfo.totalItemsCount < model.posts.size &&
                scrollState.isScrolledToTheEnd()
        }
        val spacerHeight = UiConstants.POST_SPACING
        val halfSpacerHeight = spacerHeight / 2
        val postIdsFromCurrentUser = model.postMap.filter { it.value.isFromCurrentUser }.keys
        LazyColumn(
            modifier = Modifier
                .padding(
                    end = spacerHeight
                )
                .animateContentSize(),
            state = scrollState
        ) {
            items(model.posts.size) { index ->
                if (index > model.lastViewedPostIndex) {
                    LaunchedEffect(index) {
                        model.onNewPostViewed(index)
                    }
                }
                Spacer(modifier = Modifier.height(if (index == 0) spacerHeight else halfSpacerHeight))
                renderPostModel(
                    model.postMap,
                    postIdsFromCurrentUser,
                    model.posts[index],
                    onIdClicked
                )
                Spacer(
                    modifier = Modifier
                        .height(if (index == model.posts.lastIndex) spacerHeight else halfSpacerHeight)
                )
            }
        }
        if (scrollToBottom) {
            scrollState.scrollToBottom(scope)
        }
    }

    @Composable
    private fun createThreadInteractionButtons(
        model: ThreadViewModel,
        scrollState: LazyListState,
        inputIsVisible: MutableState<Boolean>,
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight()
                .padding(end = UiConstants.FAB_PADDING, bottom = UiConstants.FAB_PADDING),
            verticalArrangement = Arrangement.Bottom,
            horizontalAlignment = Alignment.End
        ) {
            val scope = rememberCoroutineScope()
            Fab(
                isEnabled = model.posts.isNotEmpty() && !scrollState.isScrolledToTheEnd(),
                painter = painterResource(UiConstants.Icons.DOWN_ARROW),
                onClick = {
                    scrollState.scrollToBottom(scope)
                }
            )
            Spacer(modifier = Modifier.height(8.dp))
            Fab(
                isEnabled = model.allowedToPost && !inputIsVisible.value,
                painter = painterResource(UiConstants.Icons.POST),
                onClick = {
                    inputIsVisible.value = true
                    if (scrollState.isScrolledToTheEnd()) {
                        scrollState.scrollToBottom(scope)
                    }
                }
            )
        }
    }
}
