package com.yahtclub.chan.server.imageboard.usecase

import com.anchor.chan.api.account.AccountDataDTO
import com.anchor.chan.server.imageboard.usecase.RequestAccountDataUseCase
import com.anchor.publickey.crypto.AssymetricPublicKey
import com.anchor.publickey.crypto.PublicKeyHash
import com.anchor.publickey.crypto.toSha1Hex
import kotlin.math.min

class TestRequestAccountDataUseCase : RequestAccountDataUseCase {

    private val accountMap = HashMap<PublicKeyHash, AccountDataDTO>()

    override suspend fun requestAccount(publicKeyHash: PublicKeyHash): AccountDataDTO? =
        accountMap[publicKeyHash]

    override suspend fun requestAccounts(publicKeyHashes: List<PublicKeyHash>): List<AccountDataDTO> {
        val accounts = ArrayList<AccountDataDTO>(publicKeyHashes.size)

        for (key in publicKeyHashes) {
            accountMap[key]?.let { accounts.add(it) }
        }

        return accounts
    }

    fun setAccountChainLength(key: AssymetricPublicKey, length: Int) {
        val keyHash = key.toSha1Hex()
        val chain = List(min(100, length)) { "" }
        accountMap[keyHash] = AccountDataDTO(keyHash.keyHash, key.key, chain)
    }

    fun clear() {
        accountMap.clear()
    }
}
