package com.anchor.chan.server.imageboard.post.datasource

import org.jdbi.v3.core.mapper.RowMapper
import org.jdbi.v3.core.statement.StatementContext
import java.sql.ResultSet
import java.time.Instant

data class ThreadEntity(
    val threadId: String,
    val boardIds: List<String>,
    val updateTimestamp: Instant,
    val postEntity: PostEntity,
) {

    companion object {

        val generatedFields: Array<String> = arrayOf("threadId", "updateTimestamp")
    }
}

internal class ThreadEntityMapper : RowMapper<ThreadEntity> {

    override fun map(rs: ResultSet, ctx: StatementContext): ThreadEntity =
        ThreadEntity(
            threadId = rs.getString("threadId"),
            boardIds = rs.getString("boardIds").split(","),
            updateTimestamp = rs.getTimestamp("updateTimestamp").toInstant(),
            postEntity = PostEntityMapper.map(rs)
        )
}

data class ThreadEntityGeneratedFields(
    val threadId: String,
    val updateTimestamp: Instant,
)

internal class ThreadEntityGeneratedFieldsMapper : RowMapper<ThreadEntityGeneratedFields> {

    override fun map(rs: ResultSet, ctx: StatementContext): ThreadEntityGeneratedFields =
        ThreadEntityGeneratedFields(
            threadId = rs.getString("threadId"),
            updateTimestamp = rs.getTimestamp("updateTimestamp").toInstant()
        )
}
