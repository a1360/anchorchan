package com.anchor.chan.desktop.screen.splash

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import com.anchor.chan.architecture.ui.Renderer
import com.anchor.chan.desktop.LexemeConstants
import com.anchor.chan.desktop.screen.viewmodel.SplashViewModel

object SplashRenderer : Renderer<SplashViewModel> {

    @Composable
    override fun render(model: SplashViewModel) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .background(color = MaterialTheme.colors.primary),
            contentAlignment = Alignment.Center
        ) {
            Text(
                LexemeConstants.APP_TITLE,
                color = MaterialTheme.colors.onPrimary,
                style = MaterialTheme.typography.h3,
                fontWeight = FontWeight.Bold
            )
        }
    }
}
