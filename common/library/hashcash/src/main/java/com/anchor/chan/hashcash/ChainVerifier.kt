package com.anchor.chan.hashcash

interface ChainVerifier {

    suspend fun verifyToken(value: Int, token: String, key: String, previousToken: String?): Boolean
}
