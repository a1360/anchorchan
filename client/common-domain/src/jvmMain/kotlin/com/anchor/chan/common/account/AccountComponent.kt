package com.anchor.chan.common.account

import com.anchor.chan.account.AccountStateStore
import com.anchor.chan.common.component.CommonApplicationComponent
import com.anchor.chan.common.threadbrowser.content.board.LoadBoardsUseCase

class AccountComponent(
    private val applicationComponent: CommonApplicationComponent
) {

    val accountStateStore: AccountStateStore
        get() = applicationComponent.accountStateStore

    val loadBoardsUseCase: LoadBoardsUseCase
        get() = applicationComponent.loadBoardsUseCase
}
