package com.anchor.chan.model.account

data class ChainSettingsModel(
    val leadingZeros: Int
)
