**Setup the base I2P install:**

1 - Run the installer from https://geti2p.net/en/

2 - Set /usr/local/share/i2p/ as the install location

3 - Copy the created i2p folder to deployment/ansible/files/server

4 - Update i2p/runplain.sh and set I2PTEMP to /usr/local/share/i2p/tmp/

5 - Check that deployment/ansible/hosts contains your target machines

6 - Run the deployment script deploy_i2p_nodes.sh

**Configuring first I2P deployment on the server:**

1 - Run ssh -TNL 4444:127.0.0.1:4444 -L 7657:127.0.0.1:7657 user@ip_address

2 - Open localhost:7657 on your local browser and complete the configuration

**Deploy AnchorChan server**

1 - Run the deployment script build_and_deploy_server.sh

**Configuring first AnchorChan server deployment on the server**

1 - Run ssh -TNL 4444:127.0.0.1:4444 -L 7657:127.0.0.1:7657 user@ip_address

2 - Open localhost:7657 on your local browser and add an entry in the hidden services manager

3 - Also configure the server throttling

4 - Retrieve the b32 address and hardcode it within ConfigureNetworkProviderUseCase