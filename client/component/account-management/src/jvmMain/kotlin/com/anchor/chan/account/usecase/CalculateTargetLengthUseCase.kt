package com.anchor.chan.account.usecase

import com.anchor.chan.account.AccountStateStore
import com.anchor.chan.network.client.BoardNetworkClient
import io.github.resilience4j.kotlin.retry.executeSuspendFunction
import io.github.resilience4j.retry.RetryRegistry
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import java.util.concurrent.TimeUnit
import kotlin.coroutines.CoroutineContext

private val logger = LoggerFactory.getLogger(CalculateTargetLengthUseCase::class.java)

internal class CalculateTargetLengthUseCase(
    private val stateStore: AccountStateStore,
    private val boardClient: BoardNetworkClient,
    retryRegistry: RetryRegistry,
    private val ioDispatcher: CoroutineContext = Dispatchers.IO,
) {

    private val retry = retryRegistry.retry("CalculateTargetChainLength")
    private val scope = CoroutineScope(ioDispatcher)
    private var job: Job? = null

    fun calculateTargetLength() {
        job?.cancel()
        job = scope.launch {
            while (isActive) {
                try {
                    internalCalculateTargetLength()
                } catch (t: Throwable) {
                    logger.error("Error while requesting target chain length", t)
                }
                delay(TimeUnit.HOURS.toMillis(1))
            }
        }
    }

    private suspend fun internalCalculateTargetLength() {
        val key = fetchLongestChainAccount()?.keys
        val boards = retry.executeSuspendFunction { boardClient.requestBoards(key) }
        val maxChainLengthRequired = boards.maxOfOrNull { it.permissions.createLength }

        stateStore.setTargetAccountLength(maxChainLengthRequired ?: 0)
    }

    private suspend fun fetchLongestChainAccount() =
        stateStore
            .state
            .filter { it.isLoadedFromCache }
            .first()
            .accounts
            .values
            .maxByOrNull { it.chain.size }
}
