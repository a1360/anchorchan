package com.anchor.chan.desktop.screen.viewmodel

import androidx.compose.ui.graphics.Color
import com.anchor.chan.common.configuration.ColorConfiguration
import com.anchor.chan.model.imageboard.BoardModel
import com.anchor.chan.model.imageboard.ThreadModel
import com.anchor.publickey.crypto.PublicKeyHash

sealed class DesktopViewModel

object SplashViewModel : DesktopViewModel()

data class SettingsViewModel(
    val routerSettingsViewModel: RouterSettingsViewModel,
    val colorSettingsViewModel: ColorSettingsViewModel,
    val bandwidthSettingsViewModel: BandwidthSettingsViewModel,
    val isFinishButtonEnabled: Boolean,
    val onFinishClickedCallback: () -> Unit,
) : DesktopViewModel() {

    data class RouterSettingsViewModel(
        val isRouterEmbedded: Boolean?,
        val onRouterEmbeddedCallback: (Boolean) -> Unit,
    )

    data class ColorSettingsViewModel(
        val selected: ColorConfiguration,
        val colorSelectedCallback: (ColorConfiguration) -> Unit,
    )

    data class BandwidthSettingsViewModel(
        val isAutoEstimateBandwidth: Boolean,
        val inBoundKBytesPerSecond: Int,
        val outBoundKBytesPerSecond: Int,
        val onInBytesChanged: (Int) -> Unit,
        val onOutBytesChanged: (Int) -> Unit,
        val onAutoEstimateChanged: (Boolean) -> Unit,
        val onInvalidateEstimate: () -> Unit,
    )
}

data class AccountSettingsViewModel(
    val targetLength: Int,
    val accounts: List<AccountViewModel>,
    val boards: List<BoardPermissionViewModel>,
    val onDeleteClicked: (publicKey: String) -> Unit,
    val onCloseClicked: () -> Unit,
) : DesktopViewModel() {

    data class AccountViewModel(
        val publicKey: String,
        val publicKeyHash: PublicKeyHash,
        val chainLength: Int,
        val expiryDate: String,
        val threadUsageCount: Int,
    )

    data class BoardPermissionViewModel(
        val name: String,
        val canPost: Boolean,
        val canCreateThread: Boolean,
    )
}

data class ThreadBrowserViewModel(
    val menuBarViewModel: MenuBarViewModel,
    val connectionStatusViewModel: ConnectionStatusViewModel,
    val contentViewModel: ContentViewModel,
    val threadWatcherViewModel: ThreadWatcherViewModel,
) : DesktopViewModel() {

    data class MenuBarViewModel(
        val settingsClicked: () -> Unit,
        val accountSettingsClicked: () -> Unit,
    )

    data class ConnectionStatusViewModel(
        val displayStatus: Boolean,
        val loadingMessage: String?,
        val color: Color
    )

    data class ThreadWatcherViewModel(
        val onNewTabClicked: () -> Unit,
        val onTabClicked: (Int) -> Unit,
        val onTabClosed: (Int) -> Unit,
        val tabs: List<Tab>
    ) {

        data class Tab(
            val name: String,
            val isSelected: Boolean,
            val unreadCount: Int?,
        )
    }

    sealed interface ContentViewModel {

        data class SelectBoardViewModel(
            val isLoading: Boolean,
            val boards: List<BoardViewModel>,
            val onBoardsSelected: (List<BoardModel>) -> Unit,
        ) : ContentViewModel {

            data class BoardViewModel(
                val name: String,
                val boardModel: BoardModel,
            )
        }

        data class SelectThreadViewModel(
            val isLoading: Boolean,
            val allowedToCreatePost: Boolean,
            val threads: List<ThreadPreviewViewModel>,
            val onThreadSelected: (ThreadModel) -> Unit,
            val onThreadSubmitted: (title: String, message: String) -> Unit,
            val onThreadSubmittedError: String?,
        ) : ContentViewModel {

            data class ThreadPreviewViewModel(
                val initialPost: ThreadViewModel.PostViewModel,
                val model: ThreadModel,
                val lastUpdateTimestamp: String
            )
        }

        data class ThreadViewModel(
            val postMap: Map<String, PostViewModel>,
            val posts: List<PostViewModel>,
            val onPostSubmitted: (title: String, message: String) -> Unit,
            val onPostSubmittedError: String?,
            val allowedToPost: Boolean,
            val onNewPostViewed: (index: Int) -> Unit,
            val lastViewedPostIndex: Int,
        ) : ContentViewModel {

            data class PostViewModel(
                val id: String,
                val title: String?,
                val message: String,
                val timestamp: String,
                val accountId: String?,
                val isFromCurrentUser: Boolean,
            )
        }

        object Empty : ContentViewModel
    }
}
