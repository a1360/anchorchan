# Setting up the client I2P

1 - Create an i2p directory under resources/common

2 - Install the latest version of I2P there

3 - Strip out all personal information including local directory information and your ip address

4 - Create a version file matching the latest version in InstallI2pFiles.kt