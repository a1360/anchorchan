package com.anchor.chan.server.imageboard.post.datasource

import com.anchor.publickey.crypto.PublicKeyHash
import com.google.inject.Inject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.jdbi.v3.core.Handle
import org.jdbi.v3.core.Jdbi
import org.slf4j.LoggerFactory
import java.sql.Timestamp
import java.time.Instant

private val logger = LoggerFactory.getLogger(PostRepository::class.java)

class PostRepository @Inject constructor(
    private val jdbi: Jdbi
) {

    init {
        jdbi.registerRowMapper(PostEntityMapper())
        jdbi.registerRowMapper(PostEntityGeneratedFieldMapper())
    }

    suspend fun insertPost(
        title: String?,
        message: String,
        threadId: String,
        publicKey: PublicKeyHash,
        signature: String,
        timestamp: Instant,
    ): PostEntity =
        withContext(Dispatchers.IO) {
            jdbi.withHandle<PostEntity, Exception> {
                runBlocking {
                    insertPost(it, title, message, threadId, publicKey, signature, timestamp)
                }
            }
        }

    internal suspend fun insertPost(
        handle: Handle,
        title: String?,
        message: String,
        threadId: String,
        publicKey: PublicKeyHash,
        signature: String,
        timestamp: Instant,
    ): PostEntity =
        withContext(Dispatchers.IO) {
            val generatedFields =
                handle
                    .createUpdate(
                        """INSERT INTO Post (title, threadId, message, publicKeyHash, signature, timestamp) 
                        VALUES (:title, :threadId, :message, :publicKeyHash, :signature, :timestamp) 
                        RETURNING postId, timestamp""".trimMargin()
                    )
                    .bind("message", message)
                    .bind("threadId", threadId.toInt())
                    .bind("title", title)
                    .bind("publicKeyHash", publicKey.keyHash)
                    .bind("signature", signature)
                    .bind("timestamp", Timestamp.from(timestamp))
                    .executeAndReturnGeneratedKeys(*PostEntity.generatedFields)
                    .mapTo(PostEntityGeneratedField::class.java)
                    .one()

            PostEntity(
                generatedFields.postId,
                threadId,
                message,
                title,
                publicKey,
                signature,
                timestamp
            )
        }

    suspend fun requestPostsAfterTimestamp(threadIds: List<String>, timestamp: Instant): List<PostEntity> =
        withContext(Dispatchers.IO) {
            jdbi.withHandle<List<PostEntity>, Exception> {
                it.createQuery("SELECT * FROM Post WHERE timestamp > :timestamp AND threadId IN (<threadIds>) ORDER BY timestamp")
                    .bind("timestamp", timestamp)
                    .bindList("threadIds", threadIds.map { it.toInt() })
                    .mapTo(PostEntity::class.java)
                    .list()
            }
        }
}
