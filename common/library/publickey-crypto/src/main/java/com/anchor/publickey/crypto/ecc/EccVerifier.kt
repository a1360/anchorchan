package com.anchor.publickey.crypto.ecc

import com.anchor.publickey.crypto.AssymetricKeyVerifier
import com.anchor.publickey.crypto.AssymetricPublicKey
import com.anchor.publickey.crypto.toBase64Bytes
import java.security.KeyFactory
import java.security.Signature
import java.security.spec.X509EncodedKeySpec

class EccVerifier : AssymetricKeyVerifier {

    override fun verify(key: AssymetricPublicKey, message: String, signature: String): Boolean {
        val publicKeySpec = X509EncodedKeySpec(key.key.toBase64Bytes())
        val keyFactory = KeyFactory.getInstance("EC")
        val publicKey = keyFactory.generatePublic(publicKeySpec)

        val signer = Signature.getInstance("SHA256withECDSA")
        signer.initVerify(publicKey)
        signer.update(message.toByteArray())
        return signer.verify(signature.toBase64Bytes())
    }
}
