package com.anchor.chan.server

import com.anchor.chan.server.account.AccountConfig
import com.anchor.chan.server.account.AccountModule
import com.anchor.chan.server.account.AccountRouter
import com.anchor.chan.server.config.datasource.DatasourceConfig
import com.anchor.chan.server.config.datasource.DatasourceModule
import com.anchor.chan.server.config.error.ServerExceptionHandler
import com.anchor.chan.server.imageboard.ImageboardModule
import com.anchor.chan.server.imageboard.board.BoardRouter
import com.anchor.chan.server.imageboard.post.PostRouter
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import ratpack.error.ServerErrorHandler
import ratpack.kotlin.handling.module
import ratpack.kotlin.handling.ratpack
import ratpack.server.ServerConfigBuilder

fun main() {
    ratpack {
        serverConfig {
            setupEnvironment()
            require("/datasource", DatasourceConfig::class.java)
            require("/accounts", AccountConfig::class.java)
        }
        bindings {
            module<DatasourceModule>()
            module<ImageboardModule>()
            module<AccountModule>()
            add(ObjectMapper().configure())
            bind(ServerErrorHandler::class.java, ServerExceptionHandler::class.java)
        }
        handlers {
            prefix("account", AccountRouter::class.java)
            prefix("boards", BoardRouter::class.java)
            prefix("posts", PostRouter::class.java)
        }
    }
}

private fun ServerConfigBuilder.setupEnvironment(): ServerConfigBuilder {
    port(8080)
    findBaseDir("application.yaml")
    configureObjectMapper {
        it.configure()
    }
    yaml("application.yaml")
    env("")

    development {
        yaml("application-dev.yaml")
        env("")
    }

    sysProps()

    return this
}

private fun ObjectMapper.configure(): ObjectMapper =
    this
        .registerKotlinModule()
        .registerModule(JavaTimeModule())
        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

private fun ServerConfigBuilder.development(block: ServerConfigBuilder.() -> Unit) {
    if (build().isDevelopment) {
        block()
    }
}
