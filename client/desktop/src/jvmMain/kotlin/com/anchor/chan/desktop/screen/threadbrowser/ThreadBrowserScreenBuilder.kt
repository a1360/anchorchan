package com.anchor.chan.desktop.screen.threadbrowser

import androidx.compose.ui.graphics.Color
import com.anchor.chan.architecture.ui.ScreenBuilder
import com.anchor.chan.common.connection.ConnectionState
import com.anchor.chan.common.connection.I2pConnectionState
import com.anchor.chan.common.threadbrowser.ThreadBrowserComponent
import com.anchor.chan.desktop.LexemeConstants
import com.anchor.chan.desktop.screen.navigation.NavigateToSettings
import com.anchor.chan.desktop.screen.threadbrowser.content.ThreadContentScreenBuilder
import com.anchor.chan.desktop.screen.threadbrowser.watcher.ThreadWatcherScreenBuilder
import com.anchor.chan.desktop.screen.viewmodel.ThreadBrowserViewModel
import com.anchor.chan.desktop.screen.viewmodel.ThreadBrowserViewModel.ConnectionStatusViewModel
import com.anchor.chan.desktop.screen.viewmodel.ThreadBrowserViewModel.ContentViewModel
import com.anchor.chan.desktop.screen.viewmodel.ThreadBrowserViewModel.MenuBarViewModel
import com.anchor.chan.desktop.screen.viewmodel.ThreadBrowserViewModel.ThreadWatcherViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import kotlin.coroutines.coroutineContext

class ThreadBrowserScreenBuilder(
    private val threadBrowserComponent: ThreadBrowserComponent,
    private val navigateToSettings: NavigateToSettings,
) : ScreenBuilder<ThreadBrowserViewModel> {

    private lateinit var scope: CoroutineScope
    private val settingsClicked: () -> Unit = {
        navigateToSettings.navigateToSettings()
    }
    private val accountSettingsClicked: () -> Unit = {
        navigateToSettings.navigateToAccountSettings()
    }

    override suspend fun build(): Flow<ThreadBrowserViewModel> {
        scope = CoroutineScope(coroutineContext)
        val connectionStatus = threadBrowserComponent
            .connectionStateStore
            .state
            .map { it.toViewModel() }
        val contentHolder = ThreadContentScreenBuilder(threadBrowserComponent)
            .build()
        val threadWatcher = ThreadWatcherScreenBuilder(threadBrowserComponent)
            .build()
        return combine(
            connectionStatus,
            contentHolder,
            threadWatcher
        ) { connection, content, watcher ->
            createThreadBrowserViewModel(connection, content, watcher)
        }
            .onStart {
                setupConnection()
            }
            .flowOn(Dispatchers.Default)
    }

    private suspend fun setupConnection() {
        scope.launch {
            threadBrowserComponent.setupI2PRouterUseCase.setupConnection()
        }
    }

    private fun createThreadBrowserViewModel(
        connectionStatusViewModel: ConnectionStatusViewModel,
        contentViewModel: ContentViewModel,
        threadWatcherViewModel: ThreadWatcherViewModel
    ) =
        ThreadBrowserViewModel(
            menuBarViewModel = MenuBarViewModel(
                settingsClicked = settingsClicked,
                accountSettingsClicked = accountSettingsClicked,
            ),
            connectionStatusViewModel = connectionStatusViewModel,
            contentViewModel = contentViewModel,
            threadWatcherViewModel = threadWatcherViewModel,
        )

    private fun I2pConnectionState.toViewModel(): ConnectionStatusViewModel =
        when (this.connectionState) {
            ConnectionState.MANUAL,
            ConnectionState.SHUTDOWN,
            ConnectionState.SHUTTING_DOWN -> ConnectionStatusViewModel(
                displayStatus = false,
                loadingMessage = null,
                color = Color.Red
            )
            ConnectionState.CONNECTING -> ConnectionStatusViewModel(
                displayStatus = true,
                loadingMessage = LexemeConstants.ThreadBrowser.ConnectionStatus.CONNECTING,
                color = Color(224, 195, 17)
            )
            ConnectionState.CONNECTED_AND_BUILDING_NETWORK -> ConnectionStatusViewModel(
                displayStatus = true,
                loadingMessage = LexemeConstants.ThreadBrowser.ConnectionStatus.BUILDING_NETWORK,
                color = Color(224, 195, 17),
            )
            ConnectionState.CONNECTED -> ConnectionStatusViewModel(
                displayStatus = false,
                loadingMessage = null,
                color = Color(0, 128, 0)
            )
        }
}
