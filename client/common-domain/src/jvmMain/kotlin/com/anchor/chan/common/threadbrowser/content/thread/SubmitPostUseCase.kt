package com.anchor.chan.common.threadbrowser.content.thread

import com.anchor.chan.account.exception.NoAccountAvailableException
import com.anchor.chan.common.threadbrowser.content.board.SelectAccountUseCase
import com.anchor.chan.common.threadbrowser.watcher.ThreadWatcherStateStore
import com.anchor.chan.network.client.NetworkException
import com.anchor.chan.network.client.PostNetworkClient
import org.slf4j.LoggerFactory

private val logger = LoggerFactory.getLogger(SubmitPostUseCase::class.java)

class SubmitPostUseCase(
    private val selectAccountUseCase: SelectAccountUseCase,
    private val threadNetworkClient: PostNetworkClient,
    private val threadWatcherStateStore: ThreadWatcherStateStore,
) {

    suspend fun submitPost(threadId: String, title: String, message: String): String? =
        try {
            val key = selectAccountUseCase.fetchAccountForThread(threadId)
                ?.keys
                ?: throw NoAccountAvailableException()
            val post = threadNetworkClient.submitPost(key, threadId, title, message)
            threadWatcherStateStore.addPostByUser(key, post)
            null
        } catch (e: NetworkException) {
            logger.error("Network exception when submitting post", e)
            e.message
        } catch (t: Throwable) {
            logger.error("Non-network exception when submitting post", t)
            t.message
        }
}
