package com.anchor.chan.model.imageboard

import java.time.Instant

data class ThreadModel(
    val boardIds: List<String>,
    val threadId: String,
    val initialPost: PostModel,
    val lastUpdated: Instant
)
