package com.anchor.chan.api.account

data class AccountDataDTO(
    val publicKeyHash: String,
    val publicKey: String,
    var accountChain: List<String>,
) {

    val chainLength: Int
        get() = accountChain.size
}
