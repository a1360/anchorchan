package com.anchor.chan.account.usecase

import com.anchor.chan.account.AccountState
import com.anchor.chan.account.AccountStateStore
import com.anchor.publickey.crypto.AssymetricKeyGenerator
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import java.time.Clock
import java.time.Instant
import java.util.concurrent.TimeUnit
import kotlin.coroutines.CoroutineContext
import kotlin.random.Random

internal class GenerateAccountUseCase(
    private val stateStore: AccountStateStore,
    private val keyGen: AssymetricKeyGenerator,
    computeDispatcher: CoroutineContext = Dispatchers.Default,
) {

    private val scope = CoroutineScope(computeDispatcher)
    private var job: Job? = null

    fun generateAccounts() {
        job?.cancel()
        job = scope.launch {
            val waitToLoad = stateStore
                .state
                .filter { it.isLoadedFromCache }
                .first()
            while (isActive) {
                internalGenerateAccounts()
                delay(TimeUnit.DAYS.toMillis(1))
            }
        }
    }

    private suspend fun internalGenerateAccounts() {
        removeOldAccounts()
        createNewAccounts()
    }

    private suspend fun removeOldAccounts() {
        stateStore.updateAccounts {
            val now = Instant.now()
            it.filter { Instant.ofEpochMilli(it.value.accountEndDate).isAfter(now) }
        }
    }

    private suspend fun createNewAccounts() {
        val toAdd = createEstimatedNumberOfAccounts()
        insertNewAccounts(toAdd)
    }

    private suspend fun createEstimatedNumberOfAccounts(): List<AccountState.AccountData> {
        val currentState = stateStore.state.filter { it.isLoadedFromCache }.first()
        val amountNeeded = Math.max(0, TARGET_NUMBER_ACCOUNTS - currentState.accounts.size)
        val now = Instant.now()
        val random = Random(Clock.systemUTC().millis())
        val toAdd = ArrayList<AccountState.AccountData>().apply {
            repeat(amountNeeded) {
                add(createAccount(now, random))
            }
        }
        return toAdd
    }

    private suspend fun insertNewAccounts(accountsToAdd: List<AccountState.AccountData>) {
        stateStore.updateAccounts {
            val amountNeeded = Math.max(0, TARGET_NUMBER_ACCOUNTS - it.size)
            val toAdd = accountsToAdd.take(amountNeeded)
                .associateBy { it.keys.publicKey }
            it
                .takeIf { toAdd.isNotEmpty() }
                ?.plus(toAdd)
                ?: it
        }
    }

    private fun createAccount(now: Instant, random: Random): AccountState.AccountData {
        val keys = keyGen.generateKey()
        val fourMonths = TimeUnit.DAYS.toMillis(121)
        val eightMonths = TimeUnit.DAYS.toMillis(244)
        val expiry = random.nextLong(from = fourMonths, until = eightMonths)
        return AccountState.AccountData(
            keys = keys,
            accountEndDate = now.plusMillis(expiry).toEpochMilli(),
            chain = emptyList(),
        )
    }

    companion object {
        private const val TARGET_NUMBER_ACCOUNTS = 3
    }
}
