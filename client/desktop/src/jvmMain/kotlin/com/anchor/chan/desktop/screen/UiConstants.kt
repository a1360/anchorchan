package com.anchor.chan.desktop.screen

import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.material.MaterialTheme
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import java.time.Instant
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

object UiConstants {

    val POST_PADDING = 16.dp
    val POST_SPACING = 2.dp
    val CONTENT_PADDING = 6.dp
    val FAB_PADDING = 8.dp
    val BUTTON_SIZE = 40.dp
    val BUTTON_PADDING = 12.dp
    val SMALLER_BUTTON_PADDING = 8.dp
    val BUTTON_SIZE_NO_PADDING = BUTTON_SIZE - (BUTTON_PADDING * 2)

    object Input {
        val HIGHLIGHT_HEIGHT = 2.dp
        val PADDING = 4.dp
    }

    object Time {
        val defaultDateFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm z")

        fun defaultFormat(instant: Instant): String =
            instant.atZone(ZoneOffset.systemDefault()).format(defaultDateFormatter)
    }

    object Post {
        val GREEN_TEXT_REGEX: Regex =
            Regex("^>[^>${System.lineSeparator()}]+$")
        val REPLY_REGEX: Regex = Regex("^>>[0-9]+$")

        const val LINK_POST_TAG = "Link_post_tag"

        @Composable
        fun greenTextSpanStyle(): SpanStyle =
            SpanStyle(color = MaterialTheme.colors.primary)
    }

    object Tab {
        val TAB_PADDING = 8.dp
    }

    object SelectThread {
        const val GRID_SIZE = 3
    }

    object Icons {

        private const val FOLDER = "icons/"
        const val DOWN_ARROW = FOLDER + "down_arrow.svg"
        const val POST = FOLDER + "post_icon.svg"
    }
}

@Composable
fun Modifier.roundedClickable(radius: Dp = 16.dp, onClick: () -> Unit): Modifier =
    clickable(
        interactionSource = remember { MutableInteractionSource() },
        indication = rememberRipple(radius = radius),
        onClick = onClick
    )
