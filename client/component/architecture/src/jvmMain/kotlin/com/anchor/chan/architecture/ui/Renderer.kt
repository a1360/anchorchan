package com.anchor.chan.architecture.ui

import androidx.compose.runtime.Composable

interface Renderer<T> {

    @Composable
    fun render(model: T)
}
