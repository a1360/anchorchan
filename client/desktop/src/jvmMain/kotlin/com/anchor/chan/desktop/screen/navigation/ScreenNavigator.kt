package com.anchor.chan.desktop.screen.navigation

import com.anchor.chan.architecture.ui.ScreenBuilder
import com.anchor.chan.architecture.ui.ScreenStateStore
import com.anchor.chan.desktop.screen.ScreenBuilderFactory
import com.anchor.chan.desktop.screen.viewmodel.DesktopViewModel

interface NavigateToThreadBrowser {

    fun navigateToThreadBrowser()
}

interface NavigateToSettings {

    fun navigateToSettings()

    fun navigateToAccountSettings()
}

internal class ScreenNavigator(
    private val screenStateStore: ScreenStateStore<DesktopViewModel>,
    private val screenBuilderFactory: ScreenBuilderFactory
) : NavigateToThreadBrowser,
    NavigateToSettings {

    override fun navigateToThreadBrowser() {
        screenBuilderFactory
            .providerThreadBrowserScreenBuilder()
            .publish()
    }

    override fun navigateToSettings() {
        screenBuilderFactory
            .providerSettingsScreenBuilder()
            .publish()
    }

    override fun navigateToAccountSettings() {
        screenBuilderFactory
            .provideAccountSettingsScreenBuilder()
            .publish()
    }

    private fun <T : DesktopViewModel> ScreenBuilder<T>.publish() {
        screenStateStore.updateScreen(this)
    }
}
