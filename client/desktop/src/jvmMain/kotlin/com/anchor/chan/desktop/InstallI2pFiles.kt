package com.anchor.chan.desktop

import org.slf4j.LoggerFactory
import java.io.File

private val logger = LoggerFactory.getLogger(InstallI2pFiles::class.java)

class InstallI2pFiles {

    fun install() {
        val i2pResource = File("${System.getProperty("compose.application.resources.dir")}/i2p")
        val destinationI2pResource = File("${System.getProperty("user.home")}/.i2p")
        val versionFile = File(destinationI2pResource, LATEST_FILE_VERSION)
        if (!versionFile.exists()) {
            logger.info("Installing the latest I2P version")
            i2pResource.copyRecursively(destinationI2pResource, overwrite = true)
        }
    }

    companion object {
        private const val LATEST_FILE_VERSION = "i2p_1.0"
    }
}
