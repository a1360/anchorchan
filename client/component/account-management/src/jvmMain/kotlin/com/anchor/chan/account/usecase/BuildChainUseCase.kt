package com.anchor.chan.account.usecase

import com.anchor.chan.account.AccountState.AccountData.Chain
import com.anchor.chan.account.AccountStateStore
import com.anchor.chan.hashcash.ChainBuilder
import com.anchor.chan.model.account.ChainSettingsModel
import com.anchor.chan.network.client.AccountNetworkClient
import com.anchor.publickey.crypto.AssymetricPublicKey
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import java.util.concurrent.TimeUnit
import kotlin.coroutines.CoroutineContext

private val logger = LoggerFactory.getLogger(BuildChainUseCase::class.java)

internal class BuildChainUseCase(
    private val stateStore: AccountStateStore,
    private val chainBuilder: ChainBuilder,
    private val requestSettingsUseCase: RequestChainSettingsUseCase,
    private val accountClient: AccountNetworkClient,
    computeDispatcher: CoroutineContext = Dispatchers.Default,
) {

    private val scope = CoroutineScope(computeDispatcher)
    private var job: Job? = null

    fun buildChains() {
        job?.cancel()
        job = scope.launch {
            while (isActive) {
                internalBuildChains()
            }
        }
    }

    private suspend fun internalBuildChains() {
        val accountState = stateStore
            .state
            .filter { it.isLoadedFromCache && it.targetAccountLength != null }
            .first()
        val selectedAccount = accountState
            .accounts
            .values
            .filter { it.chain.size < (accountState.targetAccountLength ?: 0) }
            .minByOrNull { it.accountEndDate - it.chain.size }

        if (selectedAccount != null) {
            logger.info("Building the next chain element for ${selectedAccount.keys.publicKey}")
            buildChainForAccount(selectedAccount.keys.publicKey)
        }

        val delayAmount = if (selectedAccount != null || accountState.accounts.isEmpty()) {
            TimeUnit.SECONDS.toMillis(10)
        } else {
            TimeUnit.HOURS.toMillis(1)
        }
        delay(delayAmount)
    }

    private suspend fun buildChainForAccount(publicKey: AssymetricPublicKey) {
        val settings = requestSettingsUseCase.fetchSettings()

        val newToken = createNewToken(publicKey, settings)

        if (newToken != null) {
            uploadPendingTokens(publicKey, newToken)
        }
    }

    private suspend fun createNewToken(
        publicKey: AssymetricPublicKey,
        settings: ChainSettingsModel
    ): String? {
        val account = stateStore.state.value.accounts[publicKey] ?: return null
        val lastChain = account.chain.lastOrNull()

        val token = chainBuilder.encode(
            settings.leadingZeros,
            account.keys.publicKey.key,
            lastChain?.token
        )

        logger.info("Calculated an additional chain value for ${account.keys.publicKey.key}")
        return token
    }

    private suspend fun uploadPendingTokens(publicKey: AssymetricPublicKey, token: String) {
        val account = stateStore.state.value.accounts[publicKey] ?: return

        try {
            logger.info("Starting chain upload")
            accountClient.uploadChain(account.keys.publicKey, token)
            logger.info("Uploaded new chain")
        } catch (t: Throwable) {
            logger.error("Error uploading chain", t)
            requestSettingsUseCase.invalidateCache()
            return
        }

        stateStore.updateAccount(publicKey) {
            it.copy(chain = it.chain.plus(Chain(token)))
        }
    }
}
