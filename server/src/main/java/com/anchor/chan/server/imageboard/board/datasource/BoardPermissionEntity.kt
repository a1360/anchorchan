package com.anchor.chan.server.imageboard.board.datasource

import org.jdbi.v3.core.mapper.RowMapper
import org.jdbi.v3.core.statement.StatementContext
import java.sql.ResultSet

data class BoardPermissionEntity(
    val boardId: String,
    val postChainLength: Int,
    val createThreadChainLength: Int,
    val readChainLength: Int,
)

internal class BoardPermissionEntityMapper : RowMapper<BoardPermissionEntity> {

    override fun map(rs: ResultSet, ctx: StatementContext): BoardPermissionEntity =
        BoardPermissionEntity(
            boardId = rs.getString("boardId"),
            postChainLength = rs.getInt("postChainLength"),
            createThreadChainLength = rs.getInt("createThreadChainLength"),
            readChainLength = rs.getInt("readChainLength")
        )
}
