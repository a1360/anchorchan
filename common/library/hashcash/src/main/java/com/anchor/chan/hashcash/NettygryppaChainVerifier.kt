package com.anchor.chan.hashcash

import com.anchor.publickey.crypto.toSha1Hex
import com.nettgryppa.security.HashCash
import kotlinx.coroutines.withContext
import org.slf4j.LoggerFactory
import java.util.Calendar
import java.util.TimeZone
import kotlin.coroutines.CoroutineContext

private val logger = LoggerFactory.getLogger(NettygryppaChainVerifier::class.java)

class NettygryppaChainVerifier(
    private val config: NettygryppaChainVerifierConfig,
    private val calendarProvider: () -> Calendar = { Calendar.getInstance(TimeZone.getTimeZone("GMT")) }
) : ChainVerifier {

    override suspend fun verifyToken(
        value: Int,
        token: String,
        key: String,
        previousToken: String?
    ): Boolean =
        withContext(config.coroutineContext) {
            try {
                val tokenHashcash = HashCash(token)
                val tokenPrevHash = tokenHashcash.extensions
                    ?.get(NettygryppaChainBuilder.PREVIOUS_HASH_KEY)
                    ?.firstOrNull()
                require(tokenHashcash.resource == key) { "Key does not match" }
                require(tokenPrevHash == previousToken?.toSha1Hex()?.keyHash) { "Previous token does not match" }
                require(tokenHashcash.date?.isValidDate() == true) { "Date is invalid" }
                require(tokenHashcash.value == value) { "LeadingZeros is not valid" }
                true
            } catch (t: Throwable) {
                logger.error("Throwable thrown while verifing token", t)
                false
            }
        }

    private fun Calendar.isValidDate(): Boolean {
        val lowerMargin = calendarProvider().apply {
            add(Calendar.DAY_OF_YEAR, -2)
        }
        val upperMargin = calendarProvider().apply {
            add(Calendar.DAY_OF_YEAR, 1)
        }
        return lowerMargin.before(this) && upperMargin.after(this)
    }
}

data class NettygryppaChainVerifierConfig(
    val coroutineContext: CoroutineContext
)
