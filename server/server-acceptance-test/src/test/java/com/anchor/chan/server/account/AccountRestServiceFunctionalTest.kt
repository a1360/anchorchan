package com.anchor.chan.server.account

import com.anchor.chan.network.client.NetworkException
import com.anchor.publickey.crypto.AssymetricPublicKey
import com.anchor.publickey.crypto.toSha1Hex
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldContain

class AccountRestServiceFunctionalTest : FunSpec({

    val environment = AccountEnvironment()

    beforeSpec {
        environment.start()
        environment.insertDefaultData()
        environment.configureChainSettingsConfig()
    }

    afterTest {
        environment.resetTestAccount()
    }

    afterSpec {
        environment.close()
    }

    suspend fun insertFullDefaultAccount() {
        environment.accountNetworkService.uploadChain(
            environment.defaultKey,
            environment.firstExampleEncoding,
        )
        environment.accountNetworkService.uploadChain(
            environment.defaultKey,
            environment.secondExampleEncoding,
        )
    }

    suspend fun insertFullAlternativeAccount() {
        environment.accountNetworkService.uploadChain(
            environment.alternativeKey,
            environment.alternativeFirstEncoding
        )
    }

    context("Building a users PoW chain") {

        test("WHEN a valid PoW is submitted THEN accept it") {
            val result = environment.accountNetworkService.uploadChain(
                environment.defaultKey,
                environment.firstExampleEncoding
            )

            result shouldBe Unit
        }

        test("WHEN submit an invalid PoW token THEN receive permission denied") {
            val exception = shouldThrow<NetworkException> {
                environment.accountNetworkService.uploadChain(
                    environment.defaultKey,
                    "afsdafsdaf"
                )
            }

            exception.message shouldContain AccountEnvironment.AUTH_EXCEPTION
        }

        test("GIVEN a user already exists WHEN another user submits their PoW THEN accept it independently") {
            environment.accountNetworkService.uploadChain(
                environment.defaultKey,
                environment.firstExampleEncoding
            )
            val result = environment.accountNetworkService.uploadChain(
                environment.alternativeKey,
                environment.alternativeFirstEncoding
            )

            result shouldBe Unit
        }

        test("GIVEN application has already received a PoW WHEN another is sent with a valid prevToken THEN accept it") {
            environment.accountNetworkService.uploadChain(
                environment.defaultKey,
                environment.firstExampleEncoding,
            )
            val result = environment.accountNetworkService.uploadChain(
                environment.defaultKey,
                environment.secondExampleEncoding,
            )

            result shouldBe Unit
        }

        test("GIVEN application has already received a PoW WHEN a valid PoW is submitted without previous token THEN permission denied") {
            environment.accountNetworkService.uploadChain(
                environment.defaultKey,
                environment.firstExampleEncoding,
            )
            val result = shouldThrow<NetworkException> {
                environment.accountNetworkService.uploadChain(
                    environment.defaultKey,
                    environment.firstExampleEncoding,
                )
            }

            result.message shouldContain AccountEnvironment.AUTH_EXCEPTION
        }

        test("GIVEN application has already received a PoW WHEN a valid PoW is submitted with an invalid previous token THEN permission denied") {
            environment.accountNetworkService.uploadChain(
                environment.defaultKey,
                environment.firstExampleEncoding,
            )
            val result = shouldThrow<NetworkException> {
                environment.accountNetworkService.uploadChain(
                    environment.defaultKey,
                    environment.invalidSecondExampleEncoding,
                )
            }

            result.message shouldContain AccountEnvironment.AUTH_EXCEPTION
        }
    }

    context("Requesting account data") {

        test("GIVEN existing account with a chain WHEN account data is requested THEN accept") {
            environment.accountNetworkService.uploadChain(
                environment.defaultKey,
                environment.firstExampleEncoding,
            )
            environment.accountNetworkService.uploadChain(
                environment.defaultKey,
                environment.secondExampleEncoding,
            )

            val result = environment.accountNetworkService.getAccountData(environment.defaultKey)

            result.chainLength shouldBe 2
        }

        test("WHEN requesting accountData for an account that doesn't exist THEN throw an exception") {
            shouldThrow<NetworkException> {
                environment.accountNetworkService.getAccountData(AssymetricPublicKey("randomKey"))
            }
        }

        test("WHEN internally requesting multiple accounts THEN return all accounts") {
            insertFullDefaultAccount()
            insertFullAlternativeAccount()

            val result = environment.accountService.getAccounts(
                listOf(environment.defaultKey.toSha1Hex(), environment.alternativeKey.toSha1Hex())
            )

            result.size shouldBe 2
        }

        test("WHEN internally requesting zero accounts THEN return an empty list") {
            val result = environment.accountService.getAccounts(emptyList())

            result.size shouldBe 0
        }
    }

    context("Requesting settings") {

        test("WHEN requesting settings THEN return settings") {
            val result = environment.accountNetworkService.getChainSettings()

            result.leadingZeros shouldBe 10
        }
    }
})
