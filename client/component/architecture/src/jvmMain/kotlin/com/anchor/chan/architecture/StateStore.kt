package com.anchor.chan.architecture

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.DisposableHandle
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.concurrent.Executors

abstract class StateStore<T>(
    initialValue: T,
    dispatcher: CoroutineDispatcher = stateStoreContext
) : DisposableHandle {

    protected val scope = CoroutineScope(dispatcher + SupervisorJob())
    private val _state = MutableStateFlow(initialValue)
    val state: StateFlow<T> = _state

    protected suspend fun update(stateUpdate: (oldState: T) -> T) {
        withContext(stateStoreContext) {
            val newState = stateUpdate(state.value)
            _state.emit(newState)
        }
    }

    protected fun updateAsync(stateUpdate: (oldState: T) -> T) {
        scope.launch { update(stateUpdate) }
    }

    override fun dispose() {
        scope.cancel()
    }

    companion object {
        val stateStoreContext = Executors.newSingleThreadExecutor().asCoroutineDispatcher()
    }
}
