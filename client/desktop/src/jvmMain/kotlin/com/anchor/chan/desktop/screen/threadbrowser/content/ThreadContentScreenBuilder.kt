package com.anchor.chan.desktop.screen.threadbrowser.content

import com.anchor.chan.architecture.ui.ScreenBuilder
import com.anchor.chan.common.threadbrowser.ThreadBrowserComponent
import com.anchor.chan.common.threadbrowser.watcher.ThreadWatcherContent
import com.anchor.chan.desktop.screen.threadbrowser.content.boards.SelectBoardScreenBuilder
import com.anchor.chan.desktop.screen.threadbrowser.content.boards.SelectThreadScreenBuilder
import com.anchor.chan.desktop.screen.threadbrowser.content.thread.ThreadScreenBuilder
import com.anchor.chan.desktop.screen.viewmodel.ThreadBrowserViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map

@ExperimentalCoroutinesApi
class ThreadContentScreenBuilder(
    private val component: ThreadBrowserComponent
) : ScreenBuilder<ThreadBrowserViewModel.ContentViewModel> {

    override suspend fun build(): Flow<ThreadBrowserViewModel.ContentViewModel> =
        component
            .threadWatcherStateStore
            .state
            .map { it.selected }
            .distinctUntilChanged()
            .flatMapLatest { it.toContentViewModel() }

    private suspend fun ThreadWatcherContent.toContentViewModel(): Flow<ThreadBrowserViewModel.ContentViewModel> =
        when (this) {
            is ThreadWatcherContent.Thread ->
                ThreadScreenBuilder(model, component).build()
            is ThreadWatcherContent.Board ->
                SelectThreadScreenBuilder(boards, component).build()
            is ThreadWatcherContent.SelectBoard ->
                SelectBoardScreenBuilder(component).build()
            is ThreadWatcherContent.Empty -> flowOf(ThreadBrowserViewModel.ContentViewModel.Empty)
        }
}
