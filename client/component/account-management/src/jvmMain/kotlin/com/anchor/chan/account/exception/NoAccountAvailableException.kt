package com.anchor.chan.account.exception

class NoAccountAvailableException(
    message: String? = null,
) : RuntimeException(message)
