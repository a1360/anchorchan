package com.anchor.chan.server.imageboard.post

import com.anchor.chan.api.account.SignatureBlockDTO
import com.anchor.chan.api.imageboard.post.PostUpdatesResponseDTO
import com.anchor.chan.api.imageboard.post.SubmitPostResponseDTO
import com.anchor.chan.server.imageboard.post.datasource.PostRepository
import com.anchor.chan.server.imageboard.usecase.RequestAccountDataUseCase
import com.anchor.chan.server.imageboard.usecase.VerifyPermissionUseCase
import com.anchor.chan.server.toDto
import com.anchor.chan.server.util.extractFields
import com.anchor.publickey.crypto.toGetPostsSignature
import com.anchor.publickey.crypto.toSha1Hex
import com.anchor.publickey.crypto.toSubmitPostSignature
import com.google.inject.Inject
import java.time.Instant

class PostService @Inject constructor(
    private val postRepository: PostRepository,
    private val verifyPermissionUseCase: VerifyPermissionUseCase,
    private val requestAccountDataUseCase: RequestAccountDataUseCase,
) {

    suspend fun requestPosts(
        fromTimestamp: Instant,
        threadIds: List<String>,
        signatureBlock: SignatureBlockDTO
    ): PostUpdatesResponseDTO {
        val (publicKey, signature, timestamp) = signatureBlock.extractFields()
        val expectedSignature = signatureBlock.toGetPostsSignature(threadIds)

        verifyPermissionUseCase.verifyReadThreadPermission(
            publicKey, timestamp, expectedSignature, signature, threadIds
        )
        val posts = postRepository.requestPostsAfterTimestamp(threadIds, fromTimestamp)
        val accountData = posts
            .takeIf { it.isNotEmpty() }
            ?.map { it.publicKeyHash }
            ?.let { requestAccountDataUseCase.requestAccounts(it) }
            ?: emptyList()

        return PostUpdatesResponseDTO(
            lastTimestamp = posts.lastOrNull()?.timestamp ?: fromTimestamp,
            posts = posts.map { it.toDto() },
            accounts = accountData,
        )
    }

    suspend fun performPost(
        threadId: String,
        title: String?,
        message: String,
        signatureBlock: SignatureBlockDTO
    ): SubmitPostResponseDTO {
        val (publicKey, signature, timestamp) = signatureBlock.extractFields()

        verifyPermissionUseCase.verifyPostPermission(
            publicKey,
            timestamp,
            signatureBlock.toSubmitPostSignature(threadId, title, message),
            signature,
            threadId,
        )
        val post = postRepository.insertPost(title, message, threadId, publicKey.key.toSha1Hex(), signature, timestamp)

        return SubmitPostResponseDTO(post.toDto())
    }
}
