package com.anchor.chan.server.account

import com.anchor.chan.hashcash.ChainVerifier
import com.anchor.chan.hashcash.NettygryppaChainVerifier
import com.anchor.chan.hashcash.NettygryppaChainVerifierConfig
import com.anchor.chan.server.account.datasource.AccountRepository
import com.google.inject.AbstractModule
import com.google.inject.Provides
import com.google.inject.Scopes
import com.google.inject.Singleton
import kotlinx.coroutines.Dispatchers
import org.jdbi.v3.core.Jdbi

class AccountModule : AbstractModule() {

    override fun configure() {
        bind(AccountService::class.java).`in`(Scopes.SINGLETON)
        bind(AccountRouter::class.java).`in`(Scopes.SINGLETON)
    }

    @Provides
    @Singleton
    private fun provideChainVerifier(
        config: NettygryppaChainVerifierConfig
    ): ChainVerifier =
        NettygryppaChainVerifier(config)

    @Provides
    @Singleton
    private fun provideAccountRepository(
        jdbi: Jdbi,
        config: AccountConfig,
    ): AccountRepository =
        AccountRepository(jdbi)

    @Provides
    @Singleton
    private fun provideNettyChainVerifier(
        config: AccountConfig,
    ): NettygryppaChainVerifierConfig =
        NettygryppaChainVerifierConfig(
            coroutineContext = Dispatchers.Default
        )
}

data class AccountConfig(
    val accountCacheSize: Int,
)
