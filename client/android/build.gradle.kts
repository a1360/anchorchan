plugins {
    id("org.jetbrains.compose") version DepVersions.DESKTOP_COMPOSE
    id("com.android.application")
    kotlin("android")
}

group = "com.anchor.chan"
version = "1.0"

repositories {
    google()
}

dependencies {
    implementation(project(":client:common-domain"))
    implementation("androidx.activity:activity-compose:1.3.0-alpha03")
}

android {
    compileSdkVersion(29)
    defaultConfig {
        applicationId = "com.anchor.chan.android"
        minSdkVersion(26)
        targetSdkVersion(29)
        versionCode = 1
        versionName = "1.0"
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
        }
    }
}
