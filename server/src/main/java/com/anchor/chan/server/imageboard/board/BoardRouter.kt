package com.anchor.chan.server.imageboard.board

import com.anchor.chan.api.imageboard.board.CreateThreadRequestDTO
import com.anchor.chan.api.imageboard.board.GetBoardsRequestDTO
import com.anchor.chan.api.imageboard.board.GetThreadsRequestDTO
import com.anchor.chan.server.util.await
import com.anchor.chan.server.util.blockingRenderJson
import ratpack.kotlin.handling.KChainAction
import javax.inject.Inject

class BoardRouter @Inject constructor(
    private val boardService: BoardService
) : KChainAction() {

    override fun execute() {
        post("get") {
            blockingRenderJson {
                val signatureBlock = it.parse(GetBoardsRequestDTO::class.java).await().signatureBlockDTO
                    ?: throw IllegalArgumentException("Missing signature block")
                boardService.getBoards(signatureBlock)
            }
        }

        prefix("threads") {
            post("get") {
                blockingRenderJson {
                    val request = it.parse(GetThreadsRequestDTO::class.java).await()
                    val boardIds = request.boardIds
                        ?.takeIf { it.isNotEmpty() }
                        ?: throw IllegalArgumentException("Missing boardIds")
                    val signatureBlock = request.signatureBlock
                        ?: throw IllegalArgumentException("Missing signature block")
                    boardService.getThreads(boardIds, signatureBlock)
                }
            }
            post("create") {
                blockingRenderJson {
                    val request = it.parse(CreateThreadRequestDTO::class.java).await()
                    val boardIds = request.boardIds.takeIf { it?.isNotEmpty() == true }
                        ?: throw IllegalArgumentException("No Missing boardIds")
                    val message = request?.post?.message?.takeIf { it.isNotBlank() }
                        ?.takeIf { it.length <= 500 }
                        ?: throw IllegalArgumentException("Invalid message")
                    val title = request.post?.title?.takeIf { it.isNotBlank() }
                        ?.takeIf { it.length <= 50 }
                        ?: throw IllegalArgumentException("Valid title required to create a thread")
                    val signatureBlock = request.signatureBlockDTO
                        ?: throw IllegalArgumentException("Missing signature block")
                    boardService.createThread(boardIds, message, title, signatureBlock)
                }
            }
        }
    }
}
