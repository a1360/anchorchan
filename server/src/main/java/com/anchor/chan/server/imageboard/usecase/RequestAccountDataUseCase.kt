package com.anchor.chan.server.imageboard.usecase

import com.anchor.chan.api.account.AccountDataDTO
import com.anchor.chan.server.account.AccountService
import com.anchor.publickey.crypto.PublicKeyHash
import javax.inject.Inject

interface RequestAccountDataUseCase {

    suspend fun requestAccount(publicKeyHash: PublicKeyHash): AccountDataDTO?

    suspend fun requestAccounts(publicKeyHashes: List<PublicKeyHash>): List<AccountDataDTO>
}

class LocalRequestAccountDataUseCase @Inject constructor(
    private val accountService: AccountService
) : RequestAccountDataUseCase {

    override suspend fun requestAccount(publicKeyHash: PublicKeyHash): AccountDataDTO? =
        accountService.getAccount(publicKeyHash)

    override suspend fun requestAccounts(publicKeyHashes: List<PublicKeyHash>): List<AccountDataDTO> =
        accountService.getAccounts(publicKeyHashes)
}
