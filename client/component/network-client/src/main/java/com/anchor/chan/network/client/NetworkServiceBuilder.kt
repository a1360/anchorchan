package com.anchor.chan.network.client

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import okhttp3.HttpUrl
import org.slf4j.LoggerFactory
import retrofit2.Retrofit

private val logger = LoggerFactory.getLogger(NetworkServiceBuilder::class.java)

internal class NetworkServiceBuilder<T>(
    private val retrofitFlow: Flow<Retrofit>,
    private val retrofitToService: (Retrofit) -> T
) {

    private val serviceFlowState = MutableStateFlow<ServiceState<T>?>(null)

    suspend fun getOrCreateStub(): Flow<T> {
        retrofitFlow
            .first()
            .takeIf { serviceFlowState.hasNoValidService(it.baseUrl()) }
            ?.let {
                logger.info("Creating new service")
                val newService = retrofitToService(it)
                serviceFlowState.emit(ServiceState(it.baseUrl(), newService))
            }
        return serviceFlowState
            .filterNotNull()
            .map { it.service }
    }

    private fun MutableStateFlow<ServiceState<T>?>.hasNoValidService(url: HttpUrl) =
        value.let { it == null || it.url != url }

    private data class ServiceState<T>(
        val url: HttpUrl,
        val service: T
    )
}
