package com.anchor.chan.common.threadbrowser.content.thread

import com.anchor.chan.common.threadbrowser.watcher.ThreadWatcherStateStore
import com.anchor.chan.common.threadbrowser.watcher.WatchedThread
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import org.slf4j.LoggerFactory

private val logger = LoggerFactory.getLogger(LoadPostsUseCase::class.java)

class LoadPostsUseCase(
    private val threadBrowserStateStore: ThreadWatcherStateStore,
) {

    suspend fun loadAndListenToPosts(threadId: String): Flow<WatchedThread> =
        flow {
            threadBrowserStateStore
                .state
                .map { it.watchedThreads.get(threadId) }
                .filterNotNull()
                .collect {
                    emit(it)
                }
        }
            .onStart { threadBrowserStateStore.registerWatching(threadId) }
            .onCompletion { threadBrowserStateStore.unregisterWatching(threadId) }
}
