package com.anchor.chan.common.component

import com.anchor.chan.account.AccountBootstrapper
import com.anchor.chan.account.AccountStateStore
import com.anchor.chan.common.account.AccountComponent
import com.anchor.chan.common.configuration.ConfigurationBootstrapper
import com.anchor.chan.common.configuration.ConfigurationStateStore
import com.anchor.chan.common.connection.I2PConnectionBootstrapper
import com.anchor.chan.common.connection.I2pConnectionStateStore
import com.anchor.chan.common.connection.usecase.EstimateBandwidthUseCase
import com.anchor.chan.common.connection.usecase.SetupI2pRouterUseCase
import com.anchor.chan.common.threadbrowser.ThreadBrowserBootstrapper
import com.anchor.chan.common.threadbrowser.ThreadBrowserComponent
import com.anchor.chan.common.threadbrowser.content.board.LoadBoardsUseCase
import com.anchor.chan.common.threadbrowser.content.board.SelectAccountUseCase
import com.anchor.chan.common.threadbrowser.watcher.ThreadWatcherStateStore
import com.anchor.chan.hashcash.NettygryppaChainBuilder
import com.anchor.chan.hashcash.NettygryppaChainBuilderConfig
import com.anchor.chan.model.imageboard.BoardModel
import com.anchor.chan.network.client.NetworkClientProvider
import com.anchor.chan.network.client.NetworkException
import com.anchor.publickey.crypto.ecc.EccGenerator
import com.anchor.publickey.crypto.ecc.EccSigner
import io.github.resilience4j.retry.RetryConfig
import io.github.resilience4j.retry.RetryRegistry
import kotlinx.coroutines.Dispatchers
import java.net.SocketTimeoutException
import java.util.concurrent.TimeUnit

open class CommonApplicationComponent {

    private val messageSigner = EccSigner()

    val retryRegistry = RetryConfig.custom<List<BoardModel>>()
        .maxAttempts(10)
        .retryExceptions(NetworkException::class.java, SocketTimeoutException::class.java)
        .intervalFunction { minOf(TimeUnit.SECONDS.toMillis(30) * it, TimeUnit.MINUTES.toMillis(2)) }
        .failAfterMaxAttempts(false)
        .build()
        .let { RetryRegistry.of(it) }

    val configurationStateStore: ConfigurationStateStore =
        ConfigurationStateStore()

    private val configurationBootstrapper = ConfigurationBootstrapper(
        stateStore = configurationStateStore,
    )

    val connectionStateStore: I2pConnectionStateStore =
        I2pConnectionStateStore()

    val networkProvider: NetworkClientProvider =
        NetworkClientProvider(messageSigner, connectionStateStore::onSuccessfulRequestMade)

    val estimateBandwidthUseCase: EstimateBandwidthUseCase =
        EstimateBandwidthUseCase(configurationStateStore)

    val setupI2PRouterUseCase: SetupI2pRouterUseCase =
        SetupI2pRouterUseCase(
            configurationStateStore,
            connectionStateStore,
            estimateBandwidthUseCase,
        )

    private val i2pConnectionBootstrapper = I2PConnectionBootstrapper(
        connectionStateStore = connectionStateStore,
        networkClientProvider = networkProvider,
        configurationStateStore = configurationStateStore,
    )

    val accountStateStore = AccountStateStore()

    private val chainBuilderConfig = NettygryppaChainBuilderConfig(Dispatchers.Default, 1)
    private val chainBulder = NettygryppaChainBuilder(chainBuilderConfig)

    private val keyGen = EccGenerator()

    private val accountBootstrapper = AccountBootstrapper(
        accountStateStore = accountStateStore,
        keyGen = keyGen,
        chainBuilder = chainBulder,
        networkClientProvider = networkProvider,
        retryRegistry = retryRegistry,
    )

    val threadWatcherStateStore: ThreadWatcherStateStore =
        ThreadWatcherStateStore()

    val selectAccountUseCase: SelectAccountUseCase =
        SelectAccountUseCase(
            accountStateStore,
        )

    private val threadBrowserBootstrapper = ThreadBrowserBootstrapper(
        store = threadWatcherStateStore,
        client = networkProvider.postClient,
        selectAccountUseCase = selectAccountUseCase
    )

    val loadBoardsUseCase: LoadBoardsUseCase =
        LoadBoardsUseCase(
            selectAccountUseCase,
            networkProvider.boardClient,
            retryRegistry,
        )

    fun providerThreadBrowserComponent(): ThreadBrowserComponent =
        ThreadBrowserComponent(this)

    fun provideAccountComponent(): AccountComponent =
        AccountComponent(this)
}
