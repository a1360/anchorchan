package com.anchor.chan.common.threadbrowser.content.board

import com.anchor.chan.model.imageboard.ThreadModel
import com.anchor.chan.network.client.BoardNetworkClient
import com.github.benmanes.caffeine.cache.Caffeine
import com.github.benmanes.caffeine.cache.LoadingCache
import io.github.resilience4j.kotlin.retry.executeSuspendFunction
import io.github.resilience4j.retry.RetryRegistry
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.runBlocking
import java.time.Duration

class LoadThreadsUseCase(
    private val selectAccountUseCase: SelectAccountUseCase,
    private val boardNetworkClient: BoardNetworkClient,
    retryRegistry: RetryRegistry,
) {

    private val retry = retryRegistry.retry("LoadThreads")
    private val cacheInstance: LoadingCache<Set<String>, List<ThreadModel>> = Caffeine.newBuilder()
        .expireAfterWrite(Duration.ofMinutes(20))
        .refreshAfterWrite(Duration.ofMinutes(2))
        .maximumSize(5)
        .build { key -> runBlocking { performLoadThreadsRequest(key) } }
    private val cacheRefreshedEvents = MutableSharedFlow<Pair<Set<String>, List<ThreadModel>>>()

    fun loadThreadsForBoards(boardIds: List<String>): Flow<List<ThreadModel>> =
        cacheRefreshedEvents
            .filter { it.first == boardIds }
            .map { it.second }
            .onStart { emit(cacheInstance.get(boardIds.toSet())) }
            .flowOn(Dispatchers.IO)

    private suspend fun performLoadThreadsRequest(boardIds: Set<String>): List<ThreadModel> =
        retry.executeSuspendFunction {
            selectAccountUseCase.fetchBestAccount()
                ?.keys
                .let { boardNetworkClient.requestThreads(it, boardIds) }
        }.also { cacheRefreshedEvents.emit(boardIds to it) }

    fun invalidate(boardIds: List<String>) {
        cacheInstance.refresh(boardIds.toSet())
    }
}
