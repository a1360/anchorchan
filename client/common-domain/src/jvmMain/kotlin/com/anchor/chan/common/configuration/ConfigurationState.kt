package com.anchor.chan.common.configuration

import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
data class ConfigurationState(
    @Transient
    val isLoading: Boolean = true,
    val configVersion: Int = 0,
    val isInitialSetupComplete: Boolean = false,
    val isRouterEmbedded: Boolean? = null,
    val bandwidthSettings: BandwidthSettings = BandwidthSettings(),
    val colorConfiguration: ColorConfiguration = ColorConfiguration.FOREST,
)

@Serializable
data class BandwidthSettings(
    val isAutoEstimateBandwidth: Boolean = true,
    val inBoundKBytesPerSecond: Int = 300,
    val outBoundKBytesPerSecond: Int = 100,
    val bandwidthSharePercentage: Int = 80,
    val timeEstimated: Long = -1,
    val previousEstimateAttempt: Long = -1,
)

internal fun ConfigurationState.isConfigOld(): Boolean =
    configVersion < CURRENT_CONFIG_VERSION

fun ConfigurationState.areRequiredFieldsFilled(): Boolean =
    isRouterEmbedded != null

internal const val CURRENT_CONFIG_VERSION = 0
