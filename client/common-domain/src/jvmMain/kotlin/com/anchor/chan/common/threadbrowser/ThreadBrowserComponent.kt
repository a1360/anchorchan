package com.anchor.chan.common.threadbrowser

import com.anchor.chan.common.component.CommonApplicationComponent
import com.anchor.chan.common.connection.I2pConnectionStateStore
import com.anchor.chan.common.connection.usecase.SetupI2pRouterUseCase
import com.anchor.chan.common.threadbrowser.content.board.LoadBoardsUseCase
import com.anchor.chan.common.threadbrowser.content.board.LoadThreadsUseCase
import com.anchor.chan.common.threadbrowser.content.board.SubmitThreadUseCase
import com.anchor.chan.common.threadbrowser.content.thread.LoadPostsUseCase
import com.anchor.chan.common.threadbrowser.content.thread.SubmitPostUseCase
import com.anchor.chan.common.threadbrowser.watcher.ThreadWatcherStateStore

class ThreadBrowserComponent(
    private val applicationComponent: CommonApplicationComponent
) {
    val connectionStateStore: I2pConnectionStateStore =
        applicationComponent.connectionStateStore

    val threadWatcherStateStore: ThreadWatcherStateStore =
        applicationComponent.threadWatcherStateStore

    val setupI2PRouterUseCase: SetupI2pRouterUseCase =
        applicationComponent.setupI2PRouterUseCase

    val loadPostsUseCase: LoadPostsUseCase =
        LoadPostsUseCase(applicationComponent.threadWatcherStateStore)

    val loadBoardsUseCase: LoadBoardsUseCase
        get() = applicationComponent.loadBoardsUseCase

    val loadThreadsUseCase: LoadThreadsUseCase =
        LoadThreadsUseCase(
            applicationComponent.selectAccountUseCase,
            applicationComponent.networkProvider.boardClient,
            applicationComponent.retryRegistry,
        )

    val submitThreadUseCase: SubmitThreadUseCase =
        SubmitThreadUseCase(
            applicationComponent.selectAccountUseCase,
            loadThreadsUseCase,
            applicationComponent.networkProvider.boardClient,
        )

    val submitPostUseCase: SubmitPostUseCase =
        SubmitPostUseCase(
            applicationComponent.selectAccountUseCase,
            applicationComponent.networkProvider.postClient,
            applicationComponent.threadWatcherStateStore
        )
}
