package com.anchor.chan.server.imageboard.usecase

import com.anchor.chan.server.imageboard.board.datasource.BoardRepository
import com.anchor.chan.server.imageboard.board.datasource.ThreadRepository
import com.anchor.publickey.crypto.AssymetricKeyVerifier
import com.anchor.publickey.crypto.AssymetricPublicKey
import com.anchor.publickey.crypto.toSha1Hex
import java.time.Instant
import java.util.concurrent.TimeUnit
import javax.naming.AuthenticationException
import kotlin.jvm.Throws

interface VerifyPermissionUseCase {

    @Throws(AuthenticationException::class)
    suspend fun verifyReadBoardPermission(
        publicKey: AssymetricPublicKey?,
        timestamp: Instant?,
        signatureMessage: String?,
        signature: String?,
        boardIds: List<String>,
    )

    @Throws(AuthenticationException::class)
    suspend fun verifyReadThreadPermission(
        publicKey: AssymetricPublicKey?,
        timestamp: Instant?,
        signatureMessage: String?,
        signature: String?,
        threadIds: List<String>,
    )

    @Throws(AuthenticationException::class)
    suspend fun verifyPostPermission(
        publicKey: AssymetricPublicKey,
        timestamp: Instant,
        message: String,
        signature: String,
        threadId: String,
    )

    @Throws(AuthenticationException::class)
    suspend fun verifyCreateThreadPermission(
        publicKey: AssymetricPublicKey,
        timestamp: Instant,
        message: String,
        signature: String,
        boardIds: List<String>,
    )
}

class VerifyPermissionUseCaseImpl(
    private val getAccountDataUseCase: RequestAccountDataUseCase,
    private val requestAccountChainLength: RequestAccountChainLengthUseCase,
    private val signatureVerifier: AssymetricKeyVerifier,
    private val boardRepository: BoardRepository,
    private val threadRepository: ThreadRepository,
) : VerifyPermissionUseCase {

    override suspend fun verifyReadBoardPermission(
        publicKey: AssymetricPublicKey?,
        timestamp: Instant?,
        signatureMessage: String?,
        signature: String?,
        boardIds: List<String>
    ) {
        val chainLength = requestAccountChainLength.retrieveChainLength(
            publicKey, signatureMessage, signature, timestamp
        )

        boardRepository.requestBoardPermissions()
            .filter { boardIds.contains(it.boardId) }
            .map { it.readChainLength }
            .firstOrNull { chainLength < it }
            ?.let { throw AuthenticationException("Authentication Error: Your account isn't old enough") }
    }

    override suspend fun verifyReadThreadPermission(
        publicKey: AssymetricPublicKey?,
        timestamp: Instant?,
        signatureMessage: String?,
        signature: String?,
        threadIds: List<String>,
    ) {
        val chainLength = requestAccountChainLength.retrieveChainLength(
            publicKey, signatureMessage, signature, timestamp
        )

        val boardIds = threadRepository.requestThreadBoardIds(threadIds)
            .takeIf { it.isNotEmpty() }
            ?.toSet()
            ?: throw IllegalArgumentException("Authentication Error: Invalid threadId")
        boardRepository.requestBoardPermissions()
            .filter { boardIds.contains(it.boardId) }
            .map { it.readChainLength }
            .firstOrNull { chainLength < it }
            ?.let { throw AuthenticationException("Authentication Error: Not allowed to read this thread") }
    }

    override suspend fun verifyPostPermission(
        publicKey: AssymetricPublicKey,
        timestamp: Instant,
        message: String,
        signature: String,
        threadId: String,
    ) {
        if (!isValidTimestamp(timestamp)) {
            throw AuthenticationException("Authentication Error: Invalid timestamp")
        }
        if (!signatureVerifier.verify(publicKey, message, signature)) {
            throw AuthenticationException("Authentication Error: Signature is not valid")
        }

        val chainLength = getAccountDataUseCase.requestAccount(publicKey.key.toSha1Hex())
            ?.chainLength
            ?: throw AuthenticationException("Authentication Error: Your account isn't old enough")
        val boardIds = threadRepository.requestThreadBoardIds(listOf(threadId))
            .takeIf { it.isNotEmpty() }
            ?.toSet()
            ?: throw IllegalArgumentException("Authentication Error: Invalid threadId")
        boardRepository.requestBoardPermissions()
            .filter { boardIds.contains(it.boardId) }
            .map { it.postChainLength }
            .firstOrNull { chainLength < it }
            ?.let { throw AuthenticationException("Authentication Error: Your account isn't old enough") }
    }

    override suspend fun verifyCreateThreadPermission(
        publicKey: AssymetricPublicKey,
        timestamp: Instant,
        message: String,
        signature: String,
        boardIds: List<String>,
    ) {
        if (!isValidTimestamp(timestamp)) {
            throw AuthenticationException("Authentication Error: Invalid timestamp")
        }
        if (!signatureVerifier.verify(publicKey, message, signature)) {
            throw AuthenticationException("Authentication Error: Signature is not valid")
        }

        val chainLength = getAccountDataUseCase.requestAccount(publicKey.key.toSha1Hex())
            ?.chainLength
            ?: throw AuthenticationException("Authentication Error: Your account isn't old enough")
        boardRepository.requestBoardPermissions()
            .filter { boardIds.contains(it.boardId) }
            .map { it.createThreadChainLength }
            .firstOrNull { chainLength < it }
            ?.let { throw AuthenticationException("Authentication Error: Your account isn't old enough") }
    }

    companion object {

        internal fun isValidTimestamp(timestamp: Instant): Boolean {
            val now = Instant.now()
            val lowerMargin = now.minusMillis(TimeUnit.MINUTES.toMillis(5))
            val upperMargin = now.plusMillis(TimeUnit.MINUTES.toMillis(5))
            return timestamp.isAfter(lowerMargin) && timestamp.isBefore(upperMargin)
        }
    }
}
