docker run \
	--name dev-postgres \
	-e POSTGRES_PASSWORD="password" \
	-e POSTGRES_DB=testContainer \
	-e POSTGRES_USER=sa \
	-e POSTGRES_HOST_AUTH_METHOD=trust \
  -p 5432:5432 \
  -d postgres