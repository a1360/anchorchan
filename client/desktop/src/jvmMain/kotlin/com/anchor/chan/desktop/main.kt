package com.anchor.chan.desktop

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.WindowState
import androidx.compose.ui.window.application
import com.anchor.chan.architecture.ui.ScreenStateStore
import com.anchor.chan.common.component.CommonApplicationComponent
import com.anchor.chan.common.configuration.ColorConfiguration
import com.anchor.chan.desktop.component.DesktopApplicationComponent
import com.anchor.chan.desktop.screen.ScreenBuilderFactory
import com.anchor.chan.desktop.screen.renderDesktopModel
import com.anchor.chan.desktop.screen.viewmodel.DesktopViewModel
import com.anchor.chan.desktop.screen.viewmodel.SplashViewModel
import com.anchor.chan.desktop.ui.CrossfadeModel
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.runBlocking

@ExperimentalFoundationApi
@ExperimentalComposeUiApi
fun main() {
    InstallI2pFiles().install()
    val applicationComponent = DesktopApplicationComponent()
    val screenStateStore = ScreenStateStore<DesktopViewModel>(SplashViewModel)
    val screenBuilderFactory = ScreenBuilderFactory(applicationComponent, screenStateStore)
    screenStateStore.updateScreen(screenBuilderFactory.providerSplashScreenBuilder())
    application {
        Window(
            state = WindowState(size = DpSize(width = 1200.dp, height = 800.dp)),
            onCloseRequest = {
                runBlocking {
                    applicationComponent.setupI2PRouterUseCase.shutdownConnection()
                }
                exitApplication()
            },
            title = LexemeConstants.APP_TITLE,
        ) {
            val colors = applicationComponent.colorConfigState()
            MaterialTheme(
                colors = colors.value.toColors()
            ) {
                renderApplication(screenStateStore.state)
            }
        }
    }
}

@Composable
private fun CommonApplicationComponent.colorConfigState(): State<ColorConfiguration> =
    configurationStateStore
        .state
        .map { it.colorConfiguration }
        .distinctUntilChanged()
        .collectAsState(configurationStateStore.state.value.colorConfiguration)

@ExperimentalFoundationApi
@ExperimentalComposeUiApi
@Composable
fun renderApplication(stateFlow: StateFlow<DesktopViewModel>) {
    CrossfadeModel<DesktopViewModel>(stateFlow) {
        renderDesktopModel(it)
    }
}
