package com.anchor.publickey.crypto

import kotlinx.serialization.Serializable

interface AssymetricKeyGenerator {

    fun generateKey(): AssymetricKeyPair
}

@Serializable
data class AssymetricKeyPair(
    val privateKey: AssymetricPrivateKey,
    val publicKey: AssymetricPublicKey,
)

@Serializable
@JvmInline
value class AssymetricPublicKey(val key: String)

@Serializable
@JvmInline
value class AssymetricPrivateKey(val key: String)
