package com.anchor.chan.network.client

import com.anchor.chan.api.account.SignatureBlockDTO
import com.anchor.chan.api.imageboard.board.CreateThreadRequestDTO
import com.anchor.chan.api.imageboard.board.GetBoardsRequestDTO
import com.anchor.chan.api.imageboard.board.GetThreadsRequestDTO
import com.anchor.chan.api.imageboard.post.PostDTO
import com.anchor.chan.model.imageboard.BoardModel
import com.anchor.chan.model.imageboard.ThreadModel
import com.anchor.chan.model.toModel
import com.anchor.publickey.crypto.AssymetricKeyPair
import com.anchor.publickey.crypto.AssymetricKeySigner
import com.anchor.publickey.crypto.toCreateThreadSignature
import com.anchor.publickey.crypto.toGetThreadsSignature
import com.anchor.publickey.crypto.toSignatureString
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.withContext
import retrofit2.Retrofit
import java.time.Instant

interface BoardNetworkClient {

    suspend fun requestBoards(key: AssymetricKeyPair?): List<BoardModel>

    suspend fun requestThreads(key: AssymetricKeyPair?, boardIds: Set<String>): List<ThreadModel>

    suspend fun submitThread(
        key: AssymetricKeyPair,
        title: String,
        message: String,
        boardIds: List<String>,
    ): ThreadModel
}

internal class BoardNetworkClientImpl(
    retrofitFlow: Flow<Retrofit>,
    private val messageSigner: AssymetricKeySigner,
    private val dateProvider: () -> Instant,
) : BoardNetworkClient {

    private val serviceBuilder =
        NetworkServiceBuilder(retrofitFlow) { it.create(BoardNetworkService::class.java) }

    override suspend fun requestBoards(key: AssymetricKeyPair?): List<BoardModel> =
        withContext(Dispatchers.IO) {
            serviceBuilder
                .getOrCreateStub()
                .first()
                .getBoards(
                    GetBoardsRequestDTO(SignatureBlockDTO(key?.publicKey?.key, dateProvider()).applySignature(key))
                )
                .let { it.body()?.boards ?: throw NetworkException(it.errorBody()?.string()) }
                .map { it.toModel() }
        }

    private fun SignatureBlockDTO.applySignature(
        key: AssymetricKeyPair?
    ): SignatureBlockDTO =
        copy(
            signature = if (key != null) messageSigner.sign(key.privateKey, toSignatureString()) else null
        )

    override suspend fun requestThreads(
        key: AssymetricKeyPair?,
        boardIds: Set<String>
    ): List<ThreadModel> =
        withContext(Dispatchers.IO) {
            serviceBuilder
                .getOrCreateStub()
                .first()
                .getThreads(
                    GetThreadsRequestDTO(
                        boardIds = boardIds.toList(),
                        signatureBlock = SignatureBlockDTO(key?.publicKey?.key, dateProvider())
                    ).applySignature(key)
                )
                .let { it.body()?.threads ?: throw NetworkException(it.errorBody()?.string()) }
                .map { it.toModel() }
        }

    private fun GetThreadsRequestDTO.applySignature(
        key: AssymetricKeyPair?
    ): GetThreadsRequestDTO =
        if (key != null) {
            copy(
                signatureBlock = signatureBlock?.copy(
                    signature = messageSigner.sign(
                        key.privateKey,
                        signatureBlock?.toGetThreadsSignature(boardIds ?: emptyList()) ?: ""
                    )
                )
            )
        } else {
            this
        }

    override suspend fun submitThread(
        key: AssymetricKeyPair,
        title: String,
        message: String,
        boardIds: List<String>
    ): ThreadModel =
        withContext(Dispatchers.IO) {
            serviceBuilder
                .getOrCreateStub()
                .first()
                .createThread(createThreadRequest(key, title, message, boardIds))
                .let { it.body()?.thread?.toModel() ?: throw NetworkException(it.errorBody()?.string()) }
        }

    private fun createThreadRequest(
        key: AssymetricKeyPair,
        title: String,
        message: String,
        boards: List<String>
    ): CreateThreadRequestDTO =
        CreateThreadRequestDTO(
            boardIds = boards,
            post = PostDTO(
                postId = null,
                threadId = null,
                timestamp = null,
                message = message,
                title = title,
            ),
            signatureBlockDTO = SignatureBlockDTO(key.publicKey.key, dateProvider())
        ).applySignature(key)

    private fun CreateThreadRequestDTO.applySignature(
        key: AssymetricKeyPair
    ): CreateThreadRequestDTO =
        copy(
            signatureBlockDTO = signatureBlockDTO?.copy(
                signature = messageSigner.sign(
                    key = key.privateKey,
                    message = signatureBlockDTO?.toCreateThreadSignature(
                        boardIds ?: emptyList(), post?.message ?: "", post?.title ?: ""
                    ) ?: ""
                )
            )
        )
}
