package com.anchor.chan.api.imageboard.post

import com.anchor.chan.api.account.AccountDataDTO
import java.time.Instant

data class PostUpdatesResponseDTO(
    val lastTimestamp: Instant?,
    val posts: List<PostDTO>?,
    val accounts: List<AccountDataDTO>,
)
