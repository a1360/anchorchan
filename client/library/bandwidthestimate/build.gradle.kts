plugins {
    kotlin("jvm")
}

dependencies {
    val i2pVersion = "0.9.50"
    implementation("net.i2p:i2p:$i2pVersion")
}
