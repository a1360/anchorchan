package com.anchor.chan.api.imageboard.board

import com.fasterxml.jackson.annotation.JsonProperty

data class CreateThreadResponseDTO(
    @JsonProperty("thread") val thread: ThreadDTO,
)
