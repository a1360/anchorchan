plugins {
    kotlin("jvm")
    id("idea")
    kotlin("plugin.serialization") version "1.5.0"
}

kotlin {
    jvmToolchain {
        (this as JavaToolchainSpec).languageVersion.set(JavaLanguageVersion.of("8"))
    }
}

dependencies {
    implementation(project(":api"))

    implementation(Deps.kotlinCoroutines)
    implementation(Deps.slf4j)
    implementation(Deps.logback)
    implementation(Deps.logbackClassic)
    implementation(Deps.kotlinxSerialization)

    testImplementation(Deps.kotest)
    testImplementation(Deps.kotestAssertions)
}
