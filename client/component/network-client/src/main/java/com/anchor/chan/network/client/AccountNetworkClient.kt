package com.anchor.chan.network.client

import com.anchor.chan.api.account.ProofOfWorkDTO
import com.anchor.chan.model.account.AccountDataModel
import com.anchor.chan.model.account.ChainSettingsModel
import com.anchor.chan.model.toModel
import com.anchor.publickey.crypto.AssymetricPublicKey
import com.anchor.publickey.crypto.toSha1Hex
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.withContext
import retrofit2.Retrofit

interface AccountNetworkClient {

    suspend fun getAccountData(publicKey: AssymetricPublicKey): AccountDataModel

    suspend fun uploadChain(publicKey: AssymetricPublicKey, token: String)

    suspend fun getChainSettings(): ChainSettingsModel
}

internal class AccountNetworkClientImpl(
    retrofitFlow: Flow<Retrofit>,
) : AccountNetworkClient {

    private val serviceBuilder = NetworkServiceBuilder(retrofitFlow) { it.create(AccountNetworkService::class.java) }

    override suspend fun uploadChain(publicKey: AssymetricPublicKey, token: String) {
        withContext(Dispatchers.IO) {
            serviceBuilder
                .getOrCreateStub()
                .first()
                .uploadProofOfWorkBlock(
                    publicKey = publicKey.key,
                    proofOfWork = ProofOfWorkDTO(token)
                )
                .errorBody()
                ?.string()
                ?.let { throw NetworkException(it) }
        }
    }

    override suspend fun getChainSettings(): ChainSettingsModel =
        withContext(Dispatchers.IO) {
            serviceBuilder
                .getOrCreateStub()
                .first()
                .getProofOfWorkSettings()
                .let { it.body()?.toModel() ?: throw NetworkException(it.errorBody()?.string()) }
        }

    override suspend fun getAccountData(publicKey: AssymetricPublicKey): AccountDataModel =
        withContext(Dispatchers.IO) {
            serviceBuilder
                .getOrCreateStub()
                .first()
                .getAccountData(publicKey.key.toSha1Hex().keyHash)
                .let { it.body()?.toModel() ?: throw NetworkException(it.errorBody()?.string()) }
        }
}
