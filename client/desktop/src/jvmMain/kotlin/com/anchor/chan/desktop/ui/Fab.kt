package com.anchor.chan.desktop.ui

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.painter.Painter
import com.anchor.chan.desktop.screen.UiConstants
import com.anchor.chan.desktop.screen.roundedClickable

@Composable
fun Fab(
    painter: Painter,
    modifier: Modifier = Modifier,
    isEnabled: Boolean = true,
    onClick: () -> Unit
) {
    AnimatedVisibility(
        visible = isEnabled,
        enter = fadeIn() + slideInVertically(initialOffsetY = { it }),
        exit = fadeOut() + slideOutVertically(targetOffsetY = { it })
    ) {
        Image(
            painter = painter,
            contentDescription = null,
            modifier = modifier
                .size(UiConstants.BUTTON_SIZE)
                .background(MaterialTheme.colors.secondary, CircleShape)
                .roundedClickable(onClick = onClick)
                .padding(UiConstants.BUTTON_PADDING),
            colorFilter = ColorFilter.tint(MaterialTheme.colors.onSecondary)
        )
    }
}
