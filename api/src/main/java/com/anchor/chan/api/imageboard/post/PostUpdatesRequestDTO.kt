package com.anchor.chan.api.imageboard.post

import com.anchor.chan.api.account.SignatureBlockDTO
import java.time.Instant

data class PostUpdatesRequestDTO(
    val fromTimestamp: Instant?,
    val threadIds: List<String>?,
    val signatureBlock: SignatureBlockDTO?,
)
