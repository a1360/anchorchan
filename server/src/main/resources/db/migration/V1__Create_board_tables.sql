CREATE TABLE Board
(
    boardId   SERIAL      NOT NULL PRIMARY KEY,
    boardName VARCHAR(25) NOT NULL,
    shortName VARCHAR(10) NOT NULL
);

CREATE TABLE BoardPermission
(
    boardId                 INT NOT NULL PRIMARY KEY,
    readChainLength         INT NOT NULL,
    postChainLength         INT NOT NULL,
    createThreadChainLength INT NOT NULL
);

CREATE TABLE Thread
(
    threadId        SERIAL    NOT NULL PRIMARY KEY,
    updateTimestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE ThreadBoardId
(
    threadId INT NOT NULL,
    boardId  INT NOT NULL,
    FOREIGN KEY (boardId) REFERENCES Board (boardId) ON DELETE CASCADE,
    FOREIGN KEY (threadId) REFERENCES Thread (threadId) ON DELETE CASCADE,
    PRIMARY KEY (threadId, boardId)
);

CREATE TABLE Post
(
    postId        SERIAL        NOT NULL PRIMARY KEY,
    threadId      INT           NOT NULL,
    message       VARCHAR(500)  NOT NULL,
    title         VARCHAR(50),
    publicKeyHash VARCHAR(50)   NOT NULL,
    signature     VARCHAR(1000) NOT NULL,
    timestamp     TIMESTAMP     NOT NULL,
    FOREIGN KEY (threadId) REFERENCES Thread (threadId) ON DELETE CASCADE
);

CREATE INDEX postsTimestampIndex ON Post (timestamp);

CREATE INDEX postsByAccountIndex ON Post (publicKeyHash);

CREATE or replace FUNCTION update_thread_timestamp() RETURNS trigger AS
$BODY$
BEGIN
    UPDATE Thread
    set updateTimestamp = CURRENT_TIMESTAMP
    WHERE threadId = NEW.threadId;
    RETURN NEW;
END
$BODY$
    LANGUAGE plpgsql;

CREATE TRIGGER update_thread_on_post
    AFTER INSERT
    ON Post
    FOR EACH ROW
EXECUTE PROCEDURE update_thread_timestamp();