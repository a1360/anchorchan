package com.anchor.chan.desktop.screen

import com.anchor.chan.architecture.ui.ScreenStateStore
import com.anchor.chan.desktop.component.DesktopApplicationComponent
import com.anchor.chan.desktop.screen.account.AccountSettingsScreenBuilder
import com.anchor.chan.desktop.screen.navigation.ScreenNavigator
import com.anchor.chan.desktop.screen.settings.SettingsScreenBuilder
import com.anchor.chan.desktop.screen.splash.SplashScreenBuilder
import com.anchor.chan.desktop.screen.threadbrowser.ThreadBrowserScreenBuilder
import com.anchor.chan.desktop.screen.viewmodel.DesktopViewModel

class ScreenBuilderFactory(
    private val applicationComponent: DesktopApplicationComponent,
    screenStateStore: ScreenStateStore<DesktopViewModel>
) {

    private val screenNavigator = ScreenNavigator(screenStateStore, this)

    fun providerSplashScreenBuilder(): SplashScreenBuilder =
        SplashScreenBuilder(
            configurationStore = applicationComponent.configurationStateStore,
            navigateToSettings = screenNavigator,
            navigateToThreadBrowser = screenNavigator
        )

    fun providerSettingsScreenBuilder(): SettingsScreenBuilder =
        SettingsScreenBuilder(screenNavigator, applicationComponent.configurationStateStore)

    fun provideAccountSettingsScreenBuilder(): AccountSettingsScreenBuilder =
        AccountSettingsScreenBuilder(applicationComponent.provideAccountComponent(), screenNavigator)

    fun providerThreadBrowserScreenBuilder(): ThreadBrowserScreenBuilder =
        ThreadBrowserScreenBuilder(
            threadBrowserComponent = applicationComponent.providerThreadBrowserComponent(),
            navigateToSettings = screenNavigator,
        )
}
