package com.anchor.chan.desktop.screen

import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextDecoration
import com.anchor.chan.desktop.LexemeConstants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

private fun AnnotatedString.Builder.addStyle(style: SpanStyle, text: String, regexp: Regex) {
    for (result in regexp.findAll(text)) {
        addStyle(style, result.range.first, result.range.last + 1)
    }
}

internal fun String.processMultiLinePostStyle(
    greenTextSpanStyle: SpanStyle,
    postsFromCurrentUser: Set<String> = emptySet(),
): AnnotatedString =
    lines()
        .map { it.processSingleLinePostStyle(greenTextSpanStyle, postsFromCurrentUser) }
        .fold(AnnotatedString.Builder()) { acc, annotatedString ->
            if (acc.length != 0) {
                acc.append(System.lineSeparator())
            }
            acc.append(annotatedString)
            acc
        }.toAnnotatedString()

internal fun String.processSingleLinePostStyle(
    greenTextSpanStyle: SpanStyle,
    postsFromCurrentUser: Set<String> = emptySet(),
): AnnotatedString =
    buildAnnotatedString {
        append(this@processSingleLinePostStyle)
        addStyle(greenTextSpanStyle, this@processSingleLinePostStyle, UiConstants.Post.GREEN_TEXT_REGEX)
        addStyle(
            SpanStyle(textDecoration = TextDecoration.Underline),
            this@processSingleLinePostStyle,
            UiConstants.Post.REPLY_REGEX
        )
        addReplyAnnotation(this@processSingleLinePostStyle, postsFromCurrentUser)
    }

private fun AnnotatedString.Builder.addReplyAnnotation(
    text: String,
    postsFromCurrentUser: Set<String>,
) {
    val result = UiConstants.Post.REPLY_REGEX.find(text) ?: return
    val key = result.value.substring(2)
    if (postsFromCurrentUser.contains(key)) {
        append(LexemeConstants.ThreadBrowser.Posts.REPLY_TO_YOU)
    }
    addStyle(SpanStyle(textDecoration = TextDecoration.Underline), 0, length)
    addStringAnnotation(
        UiConstants.Post.LINK_POST_TAG,
        key,
        0,
        length
    )
}

private fun AnnotatedString.Builder.addLinkAnnotation(text: String, regexp: Regex) {
    for (result in regexp.findAll(text)) {
        addStringAnnotation(
            UiConstants.Post.LINK_POST_TAG,
            result.value.substring(2),
            result.range.first,
            result.range.last + 1
        )
    }
}

internal fun LazyListState.scrollToBottom(scope: CoroutineScope) {
    scope.launch {
        animateScrollToItem(layoutInfo.totalItemsCount - 1)
    }
}

internal fun LazyListState.isScrolledToTheEnd() =
    layoutInfo.visibleItemsInfo.lastOrNull()?.index == layoutInfo.totalItemsCount - 1
