package com.anchor.chan.server.imageboard.usecase

import com.anchor.publickey.crypto.AssymetricKeyVerifier
import com.anchor.publickey.crypto.AssymetricPublicKey
import com.anchor.publickey.crypto.toSha1Hex
import java.time.Instant

interface RequestAccountChainLengthUseCase {

    suspend fun retrieveChainLength(
        publicKey: AssymetricPublicKey?,
        signatureMessage: String?,
        signature: String?,
        timestamp: Instant?,
    ): Int
}

class RequestAccountChainLengthUseCaseImpl(
    private val signatureVerifier: AssymetricKeyVerifier,
    private val getAccountDataUseCase: RequestAccountDataUseCase,
) : RequestAccountChainLengthUseCase {

    override suspend fun retrieveChainLength(
        publicKey: AssymetricPublicKey?,
        signatureMessage: String?,
        signature: String?,
        timestamp: Instant?,
    ): Int =
        publicKey
            ?.takeIf { timestamp != null && VerifyPermissionUseCaseImpl.isValidTimestamp(timestamp) }
            ?.takeIf { signatureVerifier.verify(it, signatureMessage ?: "", signature ?: "") }
            ?.let { getAccountDataUseCase.requestAccount(it.key.toSha1Hex()) }
            ?.chainLength
            ?: 0
}
