package com.anchor.chan.server.util

import ratpack.http.Status
import ratpack.jackson.Jackson.json
import ratpack.kotlin.handling.KContext

inline fun KContext.blockingRenderJson(crossinline block: suspend () -> Any?) {
    promise {
        block()
    }.then {
        if (it != null) {
            render(json(it))
        } else {
            response.status(Status.NOT_FOUND)
        }
    }
}

inline fun KContext.blockingAccept(crossinline block: suspend () -> Unit) {
    promise {
        block()
    }.then {
        response.status(Status.ACCEPTED).send()
    }
}
