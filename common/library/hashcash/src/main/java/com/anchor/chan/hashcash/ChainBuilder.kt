package com.anchor.chan.hashcash

interface ChainBuilder {

    suspend fun encode(value: Int, key: String, previousToken: String?): String
}
