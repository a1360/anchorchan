package com.anchor.chan.api.imageboard.board

data class BoardDTO(
    val boardId: String?,
    val name: String?,
    val shortName: String?,
    val permissions: BoardPermissionsDTO?,
)
