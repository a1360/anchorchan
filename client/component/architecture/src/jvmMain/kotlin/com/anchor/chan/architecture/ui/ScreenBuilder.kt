package com.anchor.chan.architecture.ui

import kotlinx.coroutines.flow.Flow

interface ScreenBuilder<T> {

    suspend fun build(): Flow<T>
}
