package com.anchor.chan.network.client

import com.anchor.publickey.crypto.AssymetricKeySigner
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import org.slf4j.LoggerFactory
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import java.net.InetSocketAddress
import java.net.Proxy
import java.time.Instant
import kotlin.coroutines.CoroutineContext

private val logger = LoggerFactory.getLogger(NetworkClientProvider::class.java)

class NetworkClientProvider(
    private val messageSigner: AssymetricKeySigner,
    private val onSuccessfulRequest: () -> Unit = { },
    private val ioDispatcher: CoroutineContext = Dispatchers.IO,
    private val dateProvider: () -> Instant = { Instant.now() },
) {

    private val networkStateLock = Any()
    private var networkState: NetworkData? = null
    private val retrofitChannel = MutableStateFlow<Retrofit?>(null)

    val postClient: PostNetworkClient by lazy {
        PostNetworkClientImpl(retrofitChannel.filterNotNull(), messageSigner, dateProvider)
    }
    val boardClient: BoardNetworkClient by lazy {
        BoardNetworkClientImpl(retrofitChannel.filterNotNull(), messageSigner, dateProvider)
    }
    val accountClient: AccountNetworkClient by lazy {
        AccountNetworkClientImpl(retrofitChannel.filterNotNull())
    }

    suspend fun updateNetworkData(networkData: NetworkData) {
        withContext(ioDispatcher) {
            var emit = false
            synchronized(networkStateLock) {
                if (networkState != networkData) {
                    logger.info("Creating new network channel: $networkData")
                    networkState = networkData
                    emit = true
                }
            }
            if (emit) {
                retrofitChannel.emit(createRetrofit(networkData))
            }
        }
    }

    private fun createRetrofit(networkData: NetworkData) =
        Retrofit.Builder()
            .baseUrl(networkData.url)
            .client(networkData.createOkHttpClient())
            .addConverterFactory(JacksonConverterFactory.create(createObjectMapper()))
            .build()

    private fun NetworkData.createOkHttpClient(): OkHttpClient =
        OkHttpClient.Builder()
            .apply {
                if (proxyUrl != null && proxyPort != null) {
                    proxy(Proxy(Proxy.Type.HTTP, InetSocketAddress(proxyUrl, proxyPort)))
                }
                addInterceptor {
                    val response = it.proceed(it.request())
                    if (response.code() / 100 == 2) {
                        onSuccessfulRequest()
                    }
                    response
                }
            }
            .build()

    private fun createObjectMapper(): ObjectMapper =
        ObjectMapper()
            .registerKotlinModule()
            .registerModule(JavaTimeModule())
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
}

data class NetworkData(
    val url: String,
    val proxyUrl: String? = null,
    val proxyPort: Int? = null,
)
