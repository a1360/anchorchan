plugins {
    id("com.google.devtools.ksp").version("1.6.10-1.0.2")
    kotlin("jvm")
}

kotlin {
    jvmToolchain {
        (this as JavaToolchainSpec).languageVersion.set(JavaLanguageVersion.of("8"))
    }
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation(Deps.kotlinCoroutines)
    implementation("com.fasterxml.jackson.core:jackson-databind:2.13.1")
}
