package com.anchor.chan.server.imageboard.board

import com.anchor.chan.server.imageboard.board.datasource.BoardRepository
import com.anchor.chan.server.imageboard.board.datasource.ThreadRepository
import com.google.inject.AbstractModule
import com.google.inject.Scopes

class BoardModule : AbstractModule() {

    override fun configure() {
        bind(BoardRouter::class.java).`in`(Scopes.SINGLETON)
        bind(BoardRepository::class.java).`in`(Scopes.SINGLETON)
        bind(ThreadRepository::class.java).`in`(Scopes.SINGLETON)
    }
}
