package com.nettgryppa.security

import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.isActive
import java.lang.IllegalArgumentException
import java.lang.NullPointerException
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.security.SecureRandom
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Arrays
import java.util.Calendar
import java.util.HashMap
import java.util.TimeZone
import kotlin.experimental.and

// Copyright 2006 Gregory Rubin grrubin@gmail.com
//  Permission is given to use, modify, and or distribute this code so long as this message remains attached
//  Please see the spec at: http://www.hashcash.org/
/**
 * Class for generation and parsing of [HashCash](http://www.hashcash.org/)<br></br>
 * Copyright 2006 Gregory Rubin [grrubin@gmail.com](mailto:grrubin@gmail.com)<br></br>
 * Permission is given to use, modify, and or distribute this code so long as this message remains attached<br></br>
 * Please see the spec at: [http://www.hashcash.org/](http://www.hashcash.org/)
 * @author grrubin@gmail.com
 * @version 1.1
 */
internal class HashCash : Comparable<HashCash?> {
    var myToken: String? = null

    /**
     * The value of the HashCash (e.g. how many leading zero bits it has)
     */
    var value = 0
        private set

    /**
     * The minting date
     */
    var date: Calendar? = null
        private set

    /**
     * Extra data encoded in the HashCash
     */
    var extensions: Map<String, List<String>?>? = null
        private set

    /**
     * Which version of HashCash is used here
     */
    var version = 0
        private set

    /**
     * The primary resource being protected
     */
    var resource: String? = null
        private set
    // Constructors
    /**
     * Parses and validates a HashCash.
     * @throws NoSuchAlgorithmException If SHA1 is not a supported Message Digest
     */
    constructor(cash: String) {
        myToken = cash
        val parts = cash.split(":").toTypedArray()
        version = parts[0].toInt()
        require(!(version < 0 || version > 1)) { "Only supported versions are 0 and 1" }
        require(
            !(
                version == 0 && parts.size != 6 ||
                    version == 1 && parts.size != 7
                )
        ) { "Improperly formed HashCash" }
        try {
            var index = 1
            if (version == 1) value = parts[index++].toInt() else value = 0
            val dateFormat = SimpleDateFormat(dateFormatString)
            val tempCal = Calendar.getInstance(TimeZone.getTimeZone("GMT"))
            tempCal.time = dateFormat.parse(parts[index++])
            date = tempCal
            resource = parts[index++]
            extensions = deserializeExtensions(parts[index++])
            val md = MessageDigest.getInstance("SHA1")
            md.update(cash.toByteArray())
            val tempBytes = md.digest()
            val tempValue = numberOfLeadingZeros(tempBytes)
            if (version == 0) value = tempValue else if (version == 1) value =
                if (tempValue > value) value else tempValue
        } catch (ex: ParseException) {
            throw IllegalArgumentException("Improperly formed HashCash", ex)
        }
    }

    private constructor() {}
    // Accessors
    /**
     * Two objects are considered equal if they are both of type HashCash and have an identical string representation
     */
    override fun equals(other: Any?): Boolean {
        return if (other is HashCash) toString() == other.toString() else super.equals(other)
    }

    /**
     * Returns the canonical string representation of the HashCash
     */
    override fun toString(): String {
        return myToken!!
    }

    /**
     * Compares the value of two HashCashes
     * @param other
     * @see java.lang.Comparable.compareTo
     */
    override fun compareTo(other: HashCash?): Int {
        if (null == other) throw NullPointerException()
        return Integer.valueOf(value).compareTo(Integer.valueOf(other.value))
    }

    companion object {
        const val DefaultVersion = 1
        private const val hashLength = 160
        private const val dateFormatString = "yyMMdd"
        private var milliFor16: Long = -1

        /**
         * Mints a version 1 HashCash using now as the date
         * @param resource the string to be encoded in the HashCash
         * @throws NoSuchAlgorithmException If SHA1 is not a supported Message Digest
         */
        @Throws(NoSuchAlgorithmException::class)
        suspend fun mintCash(resource: String, value: Int): HashCash {
            val now = Calendar.getInstance(TimeZone.getTimeZone("GMT"))
            return mintCash(resource, null, now, value, DefaultVersion)
        }

        /**
         * Mints a  HashCash  using now as the date
         * @param resource the string to be encoded in the HashCash
         * @param version Which version to mint.  Only valid values are 0 and 1
         * @throws NoSuchAlgorithmException If SHA1 is not a supported Message Digest
         */
        @Throws(NoSuchAlgorithmException::class)
        suspend fun mintCash(resource: String, value: Int, version: Int): HashCash {
            val now = Calendar.getInstance(TimeZone.getTimeZone("GMT"))
            return mintCash(resource, null, now, value, version)
        }

        /**
         * Mints a version 1 HashCash
         * @param resource the string to be encoded in the HashCash
         * @throws NoSuchAlgorithmException If SHA1 is not a supported Message Digest
         */
        @Throws(NoSuchAlgorithmException::class)
        suspend fun mintCash(resource: String, date: Calendar, value: Int): HashCash {
            return mintCash(resource, null, date, value, DefaultVersion)
        }

        /**
         * Mints a  HashCash
         * @param resource the string to be encoded in the HashCash
         * @param version Which version to mint.  Only valid values are 0 and 1
         * @throws NoSuchAlgorithmException If SHA1 is not a supported Message Digest
         */
        @Throws(NoSuchAlgorithmException::class)
        suspend fun mintCash(resource: String, date: Calendar, value: Int, version: Int): HashCash {
            return mintCash(resource, null, date, value, version)
        }

        /**
         * Mints a version 1 HashCash using now as the date
         * @param resource the string to be encoded in the HashCash
         * @param extensions Extra data to be encoded in the HashCash
         * @throws NoSuchAlgorithmException If SHA1 is not a supported Message Digest
         */
        @Throws(NoSuchAlgorithmException::class)
        suspend fun mintCash(
            resource: String,
            extensions: Map<String, List<String>?>?,
            value: Int
        ): HashCash {
            val now = Calendar.getInstance(TimeZone.getTimeZone("GMT"))
            return mintCash(resource, extensions, now, value, DefaultVersion)
        }

        /**
         * Mints a  HashCash using now as the date
         * @param resource the string to be encoded in the HashCash
         * @param extensions Extra data to be encoded in the HashCash
         * @param version Which version to mint.  Only valid values are 0 and 1
         * @throws NoSuchAlgorithmException If SHA1 is not a supported Message Digest
         */
        @Throws(NoSuchAlgorithmException::class)
        suspend fun mintCash(
            resource: String,
            extensions: Map<String, List<String>?>?,
            value: Int,
            version: Int
        ): HashCash {
            val now = Calendar.getInstance(TimeZone.getTimeZone("GMT"))
            return mintCash(resource, extensions, now, value, version)
        }
        /**
         * Mints a  HashCash
         * @param resource the string to be encoded in the HashCash
         * @param extensions Extra data to be encoded in the HashCash
         * @param version Which version to mint.  Only valid values are 0 and 1
         * @throws NoSuchAlgorithmException If SHA1 is not a supported Message Digest
         */
        /**
         * Mints a version 1 HashCash
         * @param resource the string to be encoded in the HashCash
         * @param extensions Extra data to be encoded in the HashCash
         * @throws NoSuchAlgorithmException If SHA1 is not a supported Message Digest
         */
        @JvmOverloads
        @Throws(NoSuchAlgorithmException::class)
        suspend fun mintCash(
            resource: String,
            extensions: Map<String, List<String>?>?,
            date: Calendar,
            value: Int,
            version: Int = DefaultVersion
        ): HashCash {
            require(!(version < 0 || version > 1)) { "Only supported versions are 0 and 1" }
            require(!(value < 0 || value > hashLength)) { "Value must be between 0 and $hashLength" }
            require(!resource.contains(":")) { "Resource may not contain a colon." }
            val result = HashCash()
            val md = MessageDigest.getInstance("SHA1")
            result.resource = resource
            result.extensions = extensions ?: HashMap()
            result.date = date
            result.version = version
            val prefix: String
            val dateFormat = SimpleDateFormat(dateFormatString)
            when (version) {
                0 -> {
                    prefix =
                        version.toString() + ":" + dateFormat.format(date.time) + ":" + resource + ":" +
                        serializeExtensions(extensions) + ":"
                    result.myToken = generateCash(prefix, value, md)
                    md.reset()
                    md.update(result.myToken!!.toByteArray())
                    result.value = numberOfLeadingZeros(md.digest())
                }
                1 -> {
                    result.value = value
                    prefix =
                        version.toString() + ":" + value + ":" + dateFormat.format(date.time) + ":" + resource + ":" +
                        serializeExtensions(extensions) + ":"
                    result.myToken = generateCash(prefix, value, md)
                }
                else -> throw IllegalArgumentException("Only supported versions are 0 and 1")
            }
            return result
        }
        // Private utility functions
        /**
         * Actually tries various combinations to find a valid hash.  Form is of prefix + random_hex + ":" + random_hex
         * @throws NoSuchAlgorithmException If SHA1 is not a supported Message Digest
         */
        @Throws(NoSuchAlgorithmException::class)
        private suspend fun generateCash(prefix: String, value: Int, md: MessageDigest): String =
            coroutineScope {
                var prefix = prefix
                val rnd = SecureRandom.getInstance("SHA1PRNG")
                val tmpBytes = ByteArray(4)
                rnd.nextBytes(tmpBytes)
                val random = unsignedIntToLong(tmpBytes)
                rnd.nextBytes(tmpBytes)
                var counter = unsignedIntToLong(tmpBytes)
                prefix = prefix + java.lang.Long.toHexString(random) + ":"
                var temp: String
                var tempValue: Int
                var bArray: ByteArray
                do {
                    counter++
                    temp = prefix + java.lang.Long.toHexString(counter)
                    md.reset()
                    md.update(temp.toByteArray())
                    bArray = md.digest()
                    tempValue = numberOfLeadingZeros(bArray)
                } while (tempValue < value && isActive)
                temp
            }

        /**
         * Converts a 4 byte array of unsigned bytes to an long
         * @param b an array of 4 unsigned bytes
         * @return a long representing the unsigned int
         */
        private fun unsignedIntToLong(b: ByteArray): Long {
            val byte = (0xFF).toByte()
            var l: Long = 0
            l = l or (b[0] and byte).toLong()
            l = l shl 8
            l = l or (b[1] and byte).toLong()
            l = l shl 8
            l = l or (b[2] and byte).toLong()
            l = l shl 8
            l = l or (b[3] and byte).toLong()
            return l
        }

        /**
         * Serializes the extensions with (key, value) seperated by semi-colons and values seperated by commas
         */
        private fun serializeExtensions(extensions: Map<String, List<String>?>?): String {
            if (null == extensions || extensions.isEmpty()) return ""
            val result = StringBuffer()
            var tempList: List<String>?
            var first = true
            for (key in extensions.keys) {
                require(!(key.contains(":") || key.contains(";") || key.contains("="))) { "Extension key contains an illegal character. $key" }
                if (!first) result.append(";")
                first = false
                result.append(key)
                tempList = extensions[key]
                if (null != tempList) {
                    result.append("=")
                    for (i in tempList.indices) {
                        require(
                            !(
                                tempList[i].contains(":") || tempList[i].contains(";") || tempList[i].contains(
                                    ","
                                )
                                )
                        ) { "Extension value contains an illegal character. " + tempList[i] }
                        if (i > 0) result.append(",")
                        result.append(tempList[i])
                    }
                }
            }
            return result.toString()
        }

        /**
         * Inverse of [.serializeExtensions]
         */
        private fun deserializeExtensions(extensions: String?): Map<String, List<String>?> {
            val result: MutableMap<String, List<String>?> = HashMap()
            if (null == extensions || extensions.length == 0) return result
            val items = extensions.split(";").toTypedArray()
            for (i in items.indices) {
                val parts = items[i].split("=", limit = 2).toTypedArray()
                if (parts.size == 1) result[parts[0]] = null else result[parts[0]] =
                    Arrays.asList(*parts[1].split(",").toTypedArray())
            }
            return result
        }

        /**
         * Counts the number of leading zeros in a byte array.
         */
        private fun numberOfLeadingZeros(values: ByteArray): Int {
            var result = 0
            var temp = 0
            for (i in values.indices) {
                temp = numberOfLeadingZeros(values[i])
                result += temp
                if (temp != 8) break
            }
            return result
        }

        /**
         * Returns the number of leading zeros in a bytes binary represenation
         */
        private fun numberOfLeadingZeros(value: Byte): Int {
            if (value < 0) return 0
            return if (value < 1) 8 else if (value < 2) 7 else if (value < 4) 6 else if (value < 8) 5 else if (value < 16) 4 else if (value < 32) 3 else if (value < 64) 2 else if (value < 128) 1 else 0
        }

        /**
         * Estimates how many milliseconds it would take to mint a cash of the specified value.
         *
         *  * NOTE1: Minting time can vary greatly in fact, half of the time it will take half as long)
         *  * NOTE2: The first time that an estimation function is called it is expensive (on the order of seconds).  After that, it is very quick.
         *
         * @throws NoSuchAlgorithmException If SHA1 is not a supported Message Digest
         */
        @Throws(NoSuchAlgorithmException::class)
        suspend fun estimateTime(value: Int): Long {
            initEstimates()
            return (milliFor16 * Math.pow(2.0, (value - 16).toDouble())).toLong()
        }

        /**
         * Estimates what value (e.g. how many bits of collision) are required for the specified  length of time.
         *
         *  * NOTE1: Minting time can vary greatly in fact, half of the time it will take half as long)
         *  * NOTE2: The first time that an estimation function is called it is expensive (on the order of seconds).  After that, it is very quick.
         *
         * @throws NoSuchAlgorithmException If SHA1 is not a supported Message Digest
         */
        @Throws(NoSuchAlgorithmException::class)
        suspend fun estimateValue(secs: Int): Int {
            initEstimates()
            var result = 0
            var millis = (secs * 1000 * 65536).toLong()
            millis /= milliFor16
            while (millis > 1) {
                result++
                millis /= 2
            }
            return result
        }

        /**
         * Seeds the estimates by determining how long it takes to calculate a 16bit collision on average.
         * @throws NoSuchAlgorithmException If SHA1 is not a supported Message Digest
         */
        @Throws(NoSuchAlgorithmException::class)
        private suspend fun initEstimates() {
            if (milliFor16 == -1L) {
                var duration: Long
                duration = Calendar.getInstance().timeInMillis
                for (i in 0..10) {
                    mintCash("estimation", 16)
                }
                duration = Calendar.getInstance().timeInMillis - duration
                milliFor16 = duration / 10
            }
        }
    }
}
