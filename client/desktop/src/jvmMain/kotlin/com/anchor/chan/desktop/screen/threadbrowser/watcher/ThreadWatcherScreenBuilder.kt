package com.anchor.chan.desktop.screen.threadbrowser.watcher

import com.anchor.chan.architecture.ui.ScreenBuilder
import com.anchor.chan.common.threadbrowser.ThreadBrowserComponent
import com.anchor.chan.common.threadbrowser.watcher.ThreadWatcherContent
import com.anchor.chan.common.threadbrowser.watcher.ThreadWatcherState
import com.anchor.chan.common.threadbrowser.watcher.WatchedThread
import com.anchor.chan.desktop.LexemeConstants
import com.anchor.chan.desktop.screen.viewmodel.ThreadBrowserViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import java.lang.IllegalStateException

class ThreadWatcherScreenBuilder(
    private val component: ThreadBrowserComponent,
) : ScreenBuilder<ThreadBrowserViewModel.ThreadWatcherViewModel> {

    private val watcherStore = component.threadWatcherStateStore

    private val onNewTabClicked: () -> Unit = {
        watcherStore.setSelected(ThreadWatcherContent.SelectBoard)
    }
    private val onTabClicked: (Int) -> Unit = {
        watcherStore.state.value
            .watcherTabs.getOrNull(it)
            ?.let { newSelected -> watcherStore.setSelected(newSelected) }
    }
    private val onTabClosed: (Int) -> Unit = {
        watcherStore.state.value
            .watcherTabs.getOrNull(it)
            ?.let { toRemove -> watcherStore.removeTab(toRemove) }
    }

    override suspend fun build(): Flow<ThreadBrowserViewModel.ThreadWatcherViewModel> =
        watcherStore
            .state
            .map { it.toViewModel() }

    private fun ThreadWatcherState.toViewModel(): ThreadBrowserViewModel.ThreadWatcherViewModel =
        ThreadBrowserViewModel.ThreadWatcherViewModel(
            onNewTabClicked = onNewTabClicked,
            onTabClicked = onTabClicked,
            tabs = watcherTabs.map { it.toViewModel(watchedThreads, it == this.selected) },
            onTabClosed = onTabClosed
        )

    private fun ThreadWatcherContent.toViewModel(
        watchedThreads: Map<String, WatchedThread>,
        isSelected: Boolean,
    ): ThreadBrowserViewModel.ThreadWatcherViewModel.Tab =
        ThreadBrowserViewModel.ThreadWatcherViewModel.Tab(
            name = when (this) {
                is ThreadWatcherContent.Thread -> model.initialPost.title ?: ""
                is ThreadWatcherContent.Board -> boards.map { it.shortName }.joinToString(", ")
                is ThreadWatcherContent.SelectBoard -> LexemeConstants.ThreadBrowser.ThreadWatcher.SELECT_BOARDS
                is ThreadWatcherContent.Empty ->
                    throw IllegalStateException("Empty should not be possible to select")
                else ->
                    throw IllegalStateException("Unexpected branch - odd compiler warning here")
            },
            isSelected = isSelected,
            unreadCount = when (this) {
                is ThreadWatcherContent.Thread -> {
                    val thread = watchedThreads[model.threadId]
                    thread
                        ?.let { it.posts.lastIndex - it.lastViewedIndex }
                        ?.takeIf { it > 0 }
                }
                else -> null
            }
        )
}
