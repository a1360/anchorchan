package com.anchor.chan.desktop.screen.threadbrowser.watcher

import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.unit.dp
import com.anchor.chan.desktop.screen.UiConstants
import com.anchor.chan.desktop.screen.roundedClickable
import com.anchor.chan.desktop.screen.viewmodel.ThreadBrowserViewModel
import compose.icons.FeatherIcons
import compose.icons.feathericons.X

@Composable
fun RowScope.renderThreadWatcher(model: ThreadBrowserViewModel.ThreadWatcherViewModel) {
    val spacerHeight = UiConstants.POST_SPACING
    val halfSpacerHeight = spacerHeight / 2
    LazyColumn(
        modifier = Modifier
            .fillMaxHeight()
            .weight(0.3f)
            .padding(
                start = spacerHeight,
                end = spacerHeight
            )
            .animateContentSize(),
        horizontalAlignment = Alignment.Start,
    ) {
        items(model.tabs.size) { index ->
            val item = model.tabs[index]
            Spacer(Modifier.height(if (index == 0) spacerHeight else halfSpacerHeight))
            tab(
                item.name,
                item.unreadCount,
                item.isSelected,
                onClick = { model.onTabClicked(index) },
                onClosed = model.onTabClosed.takeIf { index != 0 }?.let { { it.invoke(index) } }
            )
            Spacer(
                Modifier.height(if (index == model.tabs.lastIndex) spacerHeight else halfSpacerHeight)
            )
        }
    }
}

@Composable
private fun tab(
    name: String,
    unreadCount: Int?,
    isSelected: Boolean,
    onClick: () -> Unit,
    onClosed: (() -> Unit)?
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .background(if (isSelected) MaterialTheme.colors.secondaryVariant else MaterialTheme.colors.secondary)
            .clickable(onClick = onClick)
            .padding(UiConstants.Tab.TAB_PADDING)
            .defaultMinSize(minHeight = UiConstants.BUTTON_SIZE),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceEvenly
    ) {
        Text(
            modifier = Modifier.weight(0.7f),
            text = name,
            color = MaterialTheme.colors.onSecondary,
            style = MaterialTheme.typography.button,
        )
        if (unreadCount != null) {
            Text(
                modifier = Modifier
                    .background(MaterialTheme.colors.primaryVariant, CircleShape)
                    .padding(4.dp),
                text = unreadCount.toString(),
                color = MaterialTheme.colors.onSecondary,
                style = MaterialTheme.typography.button,
            )
        }
        if (onClosed != null) {
            Image(
                painter = rememberVectorPainter(FeatherIcons.X),
                contentDescription = null,
                modifier = Modifier
                    .size(UiConstants.BUTTON_SIZE)
                    .roundedClickable(onClick = onClosed)
                    .padding(UiConstants.BUTTON_PADDING)
                    .weight(0.3f),
                colorFilter = ColorFilter.tint(MaterialTheme.colors.onSecondary)
            )
        }
    }
}
