plugins {
    kotlin("jvm")
    id("io.ratpack.ratpack-java")
    id("idea")
    id("com.github.johnrengelman.shadow")
}

kotlin {
    jvmToolchain {
        (this as JavaToolchainSpec).languageVersion.set(JavaLanguageVersion.of("11"))
    }
}

dependencies {
    implementation(ratpack.dependency("hikari"))
    implementation(ratpack.dependency("guice"))

    implementation(project(":api"))
    implementation(project(":common:library:hashcash"))
    implementation(project(":common:library:publickey-crypto"))

    implementation(Deps.jdbi)
    implementation("org.flywaydb:flyway-core:7.11.2")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.13.1")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.12.3")
    implementation(Deps.kotlinCoroutines)
    implementation("me.drmaas:ratpack-kotlin-dsl:1.10.0")
    implementation(Deps.postgres)
    implementation(Deps.slf4j)
    implementation(Deps.logback)

    testImplementation(Deps.kotest)
    testImplementation(Deps.kotestAssertions)
}

application {
    mainClass.set("com.anchor.chan.server.MainKt")
}
