package com.anchor.chan.api.imageboard.board

import com.anchor.chan.api.imageboard.post.PostDTO
import java.time.Instant

data class ThreadDTO(
    val boardIds: List<String>?,
    val threadId: String?,
    val initialPost: PostDTO?,
    val updatedTimestamp: Instant?,
)
