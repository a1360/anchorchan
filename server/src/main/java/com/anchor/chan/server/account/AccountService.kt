package com.anchor.chan.server.account

import com.anchor.chan.api.account.AccountDataDTO
import com.anchor.chan.api.account.ProofOfWorkSettingsDTO
import com.anchor.chan.hashcash.ChainVerifier
import com.anchor.chan.server.account.datasource.AccountEntity
import com.anchor.chan.server.account.datasource.AccountRepository
import com.anchor.chan.server.toDto
import com.anchor.publickey.crypto.PublicKeyHash
import com.anchor.publickey.crypto.toSha1Hex
import javax.inject.Inject
import javax.naming.AuthenticationException

class AccountService @Inject constructor(
    private val accountRepo: AccountRepository,
    private val chainVerifier: ChainVerifier,
) {

    suspend fun getAccount(publicKeyHash: PublicKeyHash): AccountDataDTO? {
        return accountRepo.getAccount(publicKeyHash)?.toDto()
    }

    suspend fun getAccounts(publicKeyHashes: List<PublicKeyHash>) =
        publicKeyHashes
            .takeIf { it.isNotEmpty() }
            ?.let { accountRepo.getAccounts(it) }
            ?.map { it.toDto() }
            ?: emptyList()

    suspend fun getProofOfWorkSettings(): ProofOfWorkSettingsDTO =
        ProofOfWorkSettingsDTO(accountRepo.getChainSettings().leadingZeros)

    suspend fun uploadProofOfWork(
        publicKey: String,
        token: String,
    ) {
        val publicKeyHash = publicKey.toSha1Hex()
        val account = accountRepo.getAccount(publicKeyHash)
            ?: accountRepo.createAccount(publicKeyHash, publicKey)
        verifyTokenInChain(account, token, publicKey)
        insertNewBlock(account, token)
    }

    private suspend fun verifyTokenInChain(
        account: AccountEntity,
        token: String,
        publicKey: String,
    ) {
        val lastBlock = accountRepo.getLastChainBlock(account.accountId)
        val leadingZeros = accountRepo.getChainSettings().leadingZeros
        if (!chainVerifier.verifyToken(leadingZeros, token, publicKey, lastBlock?.token)) {
            throw AuthenticationException("Authentication Error: Unable to verify the ProofOfWork block")
        }
    }

    private suspend fun insertNewBlock(
        account: AccountEntity,
        token: String,
    ) {
        accountRepo.addChainBlock(account.accountId, token)
    }
}
