package com.anchor.chan.server

import com.anchor.chan.api.account.AccountDataDTO
import com.anchor.chan.api.imageboard.board.BoardDTO
import com.anchor.chan.api.imageboard.board.BoardPermissionsDTO
import com.anchor.chan.api.imageboard.board.ThreadDTO
import com.anchor.chan.api.imageboard.post.PostDTO
import com.anchor.chan.server.account.datasource.AccountEntity
import com.anchor.chan.server.imageboard.board.datasource.BoardEntity
import com.anchor.chan.server.imageboard.board.datasource.BoardPermissionEntity
import com.anchor.chan.server.imageboard.post.datasource.PostEntity
import com.anchor.chan.server.imageboard.post.datasource.ThreadEntity

internal fun ThreadEntity.toDto(): ThreadDTO =
    ThreadDTO(
        boardIds = boardIds,
        threadId = threadId,
        initialPost = postEntity.toDto(),
        updatedTimestamp = updateTimestamp
    )

internal fun PostEntity.toDto(): PostDTO =
    PostDTO(
        postId = postId,
        message = message,
        timestamp = timestamp,
        threadId = threadId,
        title = title,
        publicKeyHash = publicKeyHash.keyHash,
        signature = signature,
    )

internal fun AccountEntity.toDto(): AccountDataDTO =
    AccountDataDTO(
        publicKey = publicKey,
        publicKeyHash = publicKeyHash.keyHash,
        accountChain = accountChain,
    )

internal fun Pair<BoardEntity, BoardPermissionEntity>.toDto(): BoardDTO =
    BoardDTO(
        boardId = first.boardId,
        name = first.boardName,
        shortName = first.shortName,
        permissions = second.toDto()
    )

internal fun BoardPermissionEntity.toDto(): BoardPermissionsDTO =
    BoardPermissionsDTO(
        readChainLength = readChainLength,
        postChainLength = postChainLength,
        createThreadChainLength = createThreadChainLength,
    )
