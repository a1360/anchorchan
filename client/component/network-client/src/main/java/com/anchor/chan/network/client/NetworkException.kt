package com.anchor.chan.network.client

class NetworkException(message: String?, throwable: Throwable? = null) : RuntimeException(message, throwable)
