package com.anchor.chan.api.account

import com.fasterxml.jackson.annotation.JsonProperty

data class ProofOfWorkDTO(
    @JsonProperty("token") var token: String?
)
