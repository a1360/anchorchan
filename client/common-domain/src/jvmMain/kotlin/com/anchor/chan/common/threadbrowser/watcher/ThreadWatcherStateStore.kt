package com.anchor.chan.common.threadbrowser.watcher

import com.anchor.chan.architecture.StateStore
import com.anchor.chan.model.account.AccountDataModel
import com.anchor.chan.model.imageboard.BoardModel
import com.anchor.chan.model.imageboard.PostModel
import com.anchor.chan.model.imageboard.ThreadModel
import com.anchor.publickey.crypto.AssymetricKeyPair
import com.anchor.publickey.crypto.PublicKeyHash
import com.anchor.publickey.crypto.toSha1Hex
import org.slf4j.LoggerFactory

private val logger = LoggerFactory.getLogger(ThreadWatcherStateStore::class.java)

class ThreadWatcherStateStore : StateStore<ThreadWatcherState>(ThreadWatcherState()) {

    fun registerWatching(threadId: String) {
        updateAsync { it.registerWatching(threadId) }
    }

    private fun ThreadWatcherState.registerWatching(threadId: String): ThreadWatcherState {
        val newValue = watchedThreads.getOrDefault(threadId, WatchedThread(threadId))
            .let { it.copy(watchers = it.watchers + 1) }
        val newWatched = watchedThreads.plus(threadId to newValue)
        return copy(watchedThreads = newWatched)
    }

    fun unregisterWatching(threadId: String) {
        updateAsync { oldState -> oldState.unregisterWatching(threadId) }
    }

    private fun ThreadWatcherState.unregisterWatching(threadId: String): ThreadWatcherState {
        val newWatchedThreads = watchedThreads.get(threadId)
            ?.let { it.copy(watchers = it.watchers - 1) }
            ?.takeIf { it.watchers > 0 }
            ?.let { watchedThreads.plus(threadId to it) }
            ?: watchedThreads.minus(threadId)
        return copy(watchedThreads = newWatchedThreads)
    }

    suspend fun appendPosts(
        newPostMap: Map<String, List<PostModel>>,
        newAccounts: Map<String, List<AccountDataModel>>
    ) {
        update {
            val newThreadValues = mutableMapOf<String, WatchedThread>()

            for ((threadId, newPosts) in newPostMap) {
                logger.info("Updating posts in $threadId")
                val watchedThread = it.watchedThreads[threadId] ?: continue

                val currentPosts = watchedThread.posts.toMutableList()
                val currentPostIds = currentPosts.map { it.id }.toSet()
                currentPosts.addAll(newPosts.filter { !currentPostIds.contains(it.id) })
                currentPosts.sortBy { it.timestamp }

                val accountsByPublicKey = newAccounts[threadId]?.associateBy { it.publicKeyHash } ?: emptyMap()
                val currentAccounts = watchedThread.accounts.plus(accountsByPublicKey)
                newThreadValues.put(threadId, watchedThread.copy(posts = currentPosts, accounts = currentAccounts))
            }

            it.copy(watchedThreads = it.watchedThreads.plus(newThreadValues))
        }
    }

    fun setSelected(selected: ThreadWatcherContent) {
        updateAsync { oldState ->
            val isNewTab = !oldState.watcherTabs.contains(selected)
            val watcherTabs =
                if (isNewTab) {
                    oldState.watcherTabs.plus(selected)
                } else {
                    oldState.watcherTabs
                }
            val intermediateState =
                when {
                    isNewTab && selected is ThreadWatcherContent.Thread ->
                        oldState.registerWatching(selected.model.threadId)
                    else -> oldState
                }

            intermediateState.copy(selected = selected, watcherTabs = watcherTabs)
        }
    }

    fun removeTab(toRemove: ThreadWatcherContent) {
        updateAsync { oldState ->
            val watcherTabs = oldState.watcherTabs.minus(toRemove)
            val selected = oldState
                .selected
                .takeIf { it != toRemove }
                ?: oldState.watcherTabs[oldState.watcherTabs.indexOf(toRemove) - 1]
            val intermediateState =
                when (toRemove) {
                    is ThreadWatcherContent.Thread ->
                        oldState.unregisterWatching(toRemove.model.threadId)
                    else -> oldState
                }
            intermediateState.copy(selected = selected, watcherTabs = watcherTabs)
        }
    }

    fun addPostByUser(account: AssymetricKeyPair, model: PostModel) {
        updateAsync { oldState ->
            val keyHash = account.publicKey.toSha1Hex()
            val watchedThread =
                oldState.watchedThreads.get(model.threadId) ?: return@updateAsync oldState
            val accounts = watchedThread.accounts
            val newAccounts = if (!accounts.contains(keyHash)) {
                accounts.plus(keyHash to AccountDataModel(keyHash, account.publicKey, emptyList()))
            } else {
                accounts
            }

            val newWatchedThread =
                watchedThread.copy(
                    postsByCurrentUser = watchedThread.postsByCurrentUser.plus(model.id),
                    posts = watchedThread.posts.toMutableList().apply {
                        add(model)
                        sortBy { it.timestamp }
                    },
                    accounts = newAccounts,
                )
            val newWatchedThreads = oldState.watchedThreads.plus(model.threadId to newWatchedThread)

            oldState.copy(watchedThreads = newWatchedThreads)
        }
    }

    fun notifyPostViewed(threadId: String, index: Int) {
        updateAsync { oldState ->
            val watchedThread = oldState.watchedThreads.get(threadId) ?: return@updateAsync oldState
            val newWatchedThread =
                watchedThread.copy(lastViewedIndex = Math.max(index, watchedThread.lastViewedIndex))
            val newWatchedThreads = oldState.watchedThreads.plus(threadId to newWatchedThread)
            oldState.copy(watchedThreads = newWatchedThreads)
        }
    }
}

data class ThreadWatcherState(
    val watchedThreads: Map<String, WatchedThread> = emptyMap(),
    val watcherTabs: List<ThreadWatcherContent> = listOf(ThreadWatcherContent.SelectBoard),
    val selected: ThreadWatcherContent = ThreadWatcherContent.SelectBoard
)

data class WatchedThread(
    val threadId: String,
    val posts: List<PostModel> = emptyList(),
    val accounts: Map<PublicKeyHash, AccountDataModel> = emptyMap(),
    val watchers: Int = 0,
    val postsByCurrentUser: Set<String> = emptySet(),
    val lastViewedIndex: Int = 0,
)

sealed interface ThreadWatcherContent {

    data class Thread(val model: ThreadModel) : ThreadWatcherContent

    data class Board(val boards: List<BoardModel>) : ThreadWatcherContent

    object SelectBoard : ThreadWatcherContent

    object Empty : ThreadWatcherContent
}
