package com.anchor.chan.api.imageboard.post

import com.anchor.chan.api.account.SignatureBlockDTO

data class SubmitPostRequestDTO(
    val post: PostDTO?,
    val signatureBlock: SignatureBlockDTO?,
)
