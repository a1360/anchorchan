package com.anchor.chan.api.imageboard.board

data class BoardPermissionsDTO(
    val readChainLength: Int?,
    val postChainLength: Int?,
    val createThreadChainLength: Int?,
)
