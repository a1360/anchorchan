package com.anchor.chan.common.configuration

import com.anchor.chan.architecture.usecase.PersistenceUseCase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ConfigurationBootstrapper(
    private val stateStore: ConfigurationStateStore,
    private val bootstrapScope: CoroutineScope = CoroutineScope(Dispatchers.IO),
) {

    private val persistence = PersistenceUseCase.create<ConfigurationState>(CONFIG_CACHE_PATH)

    init {
        persistence()
    }

    private fun persistence() {
        bootstrapScope.launch {
            val cachedState = persistence.loadFromDisk()
            stateStore.updateFromCache(cachedState)
            persistence.storeStatePeriodically(stateStore.state)
        }
    }

    companion object {
        private val CONFIG_CACHE_PATH = "${System.getProperty("user.home")}/.i2p/.anchorconfig"
    }
}
