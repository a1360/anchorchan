package com.anchor.chan.server.util

import com.anchor.chan.api.account.SignatureBlockDTO
import com.anchor.publickey.crypto.AssymetricPublicKey
import java.time.Instant

internal fun SignatureBlockDTO.extractFields(): Triple<AssymetricPublicKey, String, Instant> {
    val publicKey = publicKey?.let { AssymetricPublicKey(it) }
        ?: throw IllegalArgumentException("Missing valid publicKey")
    val timestamp = timestamp
        ?: throw IllegalArgumentException("Missing valid timestamp")
    val signature = signature
        ?: throw IllegalArgumentException("Missing valid signature")
    return Triple(publicKey, signature, timestamp)
}

internal fun SignatureBlockDTO.extractFieldsNullable(): Triple<AssymetricPublicKey?, String?, Instant?> {
    val publicKey = publicKey?.let { AssymetricPublicKey(it) }
        ?: return Triple(null, null, null)
    val timestamp = timestamp
        ?: return Triple(null, null, null)
    val signature = signature
        ?: return Triple(null, null, null)
    return Triple(publicKey, signature, timestamp)
}
