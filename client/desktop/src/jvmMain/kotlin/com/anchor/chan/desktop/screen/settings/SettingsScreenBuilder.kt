package com.anchor.chan.desktop.screen.settings

import com.anchor.chan.architecture.ui.ScreenBuilder
import com.anchor.chan.common.configuration.ColorConfiguration
import com.anchor.chan.common.configuration.ConfigurationState
import com.anchor.chan.common.configuration.ConfigurationStateStore
import com.anchor.chan.common.configuration.areRequiredFieldsFilled
import com.anchor.chan.desktop.screen.navigation.NavigateToThreadBrowser
import com.anchor.chan.desktop.screen.viewmodel.SettingsViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class SettingsScreenBuilder(
    private val navigateToThreadBrowser: NavigateToThreadBrowser,
    private val configurationStateStore: ConfigurationStateStore
) : ScreenBuilder<SettingsViewModel> {

    private val isRouterEmbeddedCallback: (Boolean) -> Unit = {
        configurationStateStore.setRouterEmbedded(it)
    }
    private val onAutoSetupBandwidthChanged: (Boolean) -> Unit = {
        configurationStateStore.setAutoConfigureBandwidth(it)
    }
    private val onInBytesChanged: (Int) -> Unit = {
        configurationStateStore.setBandwidthInBytesPerSecond(it)
    }
    private val onOutBytesChanged: (Int) -> Unit = {
        configurationStateStore.setBandwidthOutBytesPerSecond(it)
    }
    private val onInvalidateBandwidthEstimate: () -> Unit = {
        configurationStateStore.invalidateBandwidthEstimate()
    }
    private val colorSelectedCallback: (ColorConfiguration) -> Unit = {
        configurationStateStore.setColorScheme(it)
    }
    private val finishClickedCallback: () -> Unit = {
        configurationStateStore.setInitialConfigCompleted(true)
        navigateToThreadBrowser.navigateToThreadBrowser()
    }

    override suspend fun build(): Flow<SettingsViewModel> {
        return configurationStateStore
            .state
            .map { it.toInitialStateViewModel() }
    }

    private fun ConfigurationState.toInitialStateViewModel(): SettingsViewModel =
        SettingsViewModel(
            routerSettingsViewModel = SettingsViewModel.RouterSettingsViewModel(
                isRouterEmbedded = isRouterEmbedded,
                onRouterEmbeddedCallback = isRouterEmbeddedCallback,
            ),
            colorSettingsViewModel = SettingsViewModel.ColorSettingsViewModel(
                selected = colorConfiguration,
                colorSelectedCallback = colorSelectedCallback
            ),
            bandwidthSettingsViewModel = SettingsViewModel.BandwidthSettingsViewModel(
                isAutoEstimateBandwidth = bandwidthSettings.isAutoEstimateBandwidth,
                inBoundKBytesPerSecond = bandwidthSettings.inBoundKBytesPerSecond,
                outBoundKBytesPerSecond = bandwidthSettings.outBoundKBytesPerSecond,
                onAutoEstimateChanged = onAutoSetupBandwidthChanged,
                onInBytesChanged = onInBytesChanged,
                onOutBytesChanged = onOutBytesChanged,
                onInvalidateEstimate = onInvalidateBandwidthEstimate,
            ),
            isFinishButtonEnabled = areRequiredFieldsFilled(),
            onFinishClickedCallback = finishClickedCallback
        )
}
